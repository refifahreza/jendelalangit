<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// if (env('APP_ENV') === 'production') {
//     URL::forceScheme('https');
// }

// Auth::routes();
Route::get('/hidden/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/hidden/login', 'Auth\LoginController@login');
Route::post('/hidden/logout', 'Auth\LoginController@logout')->name('logout');

// test
Route::get('/notifemaile', 'NotifController@test')->name('notifemaile');

// real
Route::get('/notifemail', 'NotifController@index')->name('notifemail');

Route::get('/', 'FrontendController@index')->name('main');
Route::get('/file/{id}/{slug}', 'FrontendController@download')->name('download');
Route::post('/pesan', 'FrontendController@store_pesan')->name('pesanstore');
Route::get('/berita', 'FrontendController@berita')->name('berita');
Route::get('/album', 'FrontendController@album')->name('album');
Route::get('/faq', 'FrontendController@faq')->name('faq');
Route::get('/kontak', 'FrontendController@kontak')->name('kontak');
Route::get('/rekening', 'FrontendController@rekening')->name('rekening');
Route::get('/detail_berita/{slug_title}', 'FrontendController@detailberita')->name('detail_berita');
Route::get('/kategori_berita/{katber_slug}', 'FrontendController@kategori')->name('kategori_berita');
Route::get('/detail_album/{album_slug}', 'FrontendController@detailalbum')->name('detail_album');
Route::get('/tentang-kami', 'FrontendController@tentangkami')->name('tentangkami');
Route::get('/detail_tentang/{katbout_slug}', 'FrontendController@detailtentang')->name('detail_tentang');
Route::post('/detail-donasi/confirm', 'FrontendController@detaildonasi')->name('detail-donasi');
Route::get('/status-donasi/{kode_donasi}', 'FrontendController@statusdonasi')->name('status-donasi');
Route::get('/cek-donasi', 'FrontendController@cekdonasi')->name('cek-donasi');
Route::put('/konfirmasi-donasi/{kode_donasi}', 'FrontendController@konfirmasidonasi')->name('konfirmasi-donasi');
Route::get('/list-donatur', 'FrontendController@listdonatur')->name('listdonatur');
Route::get('/manual-payment/{rekening_slug}', 'FrontendController@manualpembayaran')->name('manual-payment');

// Zakat
Route::get('/zakat', 'FrontendController@zakat')->name('zakat');

// Midtrans
Route::post('/finish', function () {
    return redirect()->route('/');
})->name('donasi.finish');

Route::post('/donasi/store', 'FrontendController@submit')->name('donasi.store');
Route::post('/zakat/store', 'FrontendController@submitzakat')->name('zakat.store');
Route::post('/notification/handler', 'FrontendController@notificationHandler')->name('notification.handler');

// AJAX
Route::post('/jenis_change', 'FrontendController@jenis')->name('front.jenis');

// Filemanager
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

// My Routes
Route::group(['namespace' => 'Admin',  'prefix' => 'hidden', 'middleware' => 'auth'], function () {
    Route::name('admin.')->group(function () {
        Route::get('/', 'DashboardController@index')->name('home');

        // Account
        Route::get('/profil', 'AkunController@index')->name('profil.index');
        Route::put('/profil', 'AkunController@update')->name('profil.update');
        Route::resource('/manajemen_akun', 'ManukController');

        // Setting
        Route::get('/setting_zakat', 'SettingzakatController@index')->name('settingzakat.index');
        Route::put('/setting_zakat', 'SettingzakatController@update')->name('settingzakat.update');

        // Setting
        Route::get('/settings', 'SettingController@index')->name('settings.index');
        Route::put('/settings', 'SettingController@update')->name('settings.update');

        // Donasi Online
        Route::get('/donasi_online', 'DonlineController@index')->name('donasi_online.index');
        Route::get('/export_donasi', 'DonlineController@export')->name('export_donasi');

        // Donasi Manual
        Route::get('/donasi_manual', 'DonmanController@index')->name('donasi_manual.index');
        Route::get('/donasi_manual/{kode_donasi}', 'DonmanController@show')->name('donasi_manual.show');
        Route::delete('/donasi_manual/{kode_donasi}/delete', 'DonmanController@destroy')->name('donasi_manual.destroy');
        Route::put('/update_donasi/{kode_donasi}', 'DonmanController@change')->name('update_donasi.change');
        Route::get('/export_donasi2', 'DonmanController@export')->name('export_donasi2');

        // List Zakat
        Route::get('/list_zakat', 'ListzakatController@index')->name('list_zakat.index');
        Route::get('/list_zakat/{kode_zakat}', 'ListzakatController@show')->name('list_zakat.show');
        Route::delete('/list_zakat/{kode_zakat}/delete', 'ListzakatController@destroy')->name('list_zakat.destroy');
        Route::put('/update_zakat/{kode_zakat}', 'ListzakatController@change')->name('update_zakat.change');
        Route::get('/listzakat_export', 'ListzakatController@export')->name('export_listzakat');

        // Pesan
        Route::resource('/pesan', 'PesanController');

        // Kategori Berita
        Route::resource('/kategori_berita', 'KatberController');
        Route::post('/kategori_berita/aktif', 'KatberController@aktif')->name('katber.aktif');


        // Video
        Route::resource('/video', 'VideoController');
        Route::post('/video/aktif', 'VideoController@aktif')->name('video.aktif');

        // Tag
        Route::resource('/tag', 'TagController');
        Route::post('/tag/aktif', 'TagController@aktif')->name('tag.aktif');


        // Kategori Video
        Route::resource('/kategori_video', 'KatvidController');
        Route::post('/kategori_video/aktif', 'KatvidController@aktif')->name('katvid.aktif');

        // Kategori About
        Route::resource('/katbout', 'KatboutController');
        Route::post('/katbout/aktif', 'KatboutController@aktif')->name('katbout.aktif');

        // Mitra
        Route::resource('/mitra', 'MitraController');
        Route::post('/mitra/aktif', 'MitraController@aktif')->name('mitra.aktif');

        // Faqs
        Route::resource('/faqs', 'FaqsController');
        Route::post('/faqs/aktif', 'FaqsController@aktif')->name('faqs.aktif');

        // Agenda
        Route::resource('/agenda', 'AgendaController');
        Route::post('/agenda/aktif', 'AgendaController@aktif')->name('agenda.aktif');

        // Zakat
        Route::resource('/zakat', 'ZakatController');
        Route::post('/zakat/aktif', 'ZakatController@aktif')->name('zakat.aktif');

        // Zakat
        Route::resource('/sub-zakat', 'SubzakatController');
        Route::post('/sub-zakat/aktif', 'SubzakatController@aktif')->name('sub-zakat.aktif');

        // Slider
        Route::get('/slider/change', 'SliderController@change')->name('slider.change');
        Route::resource('/slider', 'SliderController');
        Route::post('/slider/aktif', 'SliderController@aktif')->name('slider.aktif');

        // Berita
        Route::post('/berita/upload', 'BeritaController@upload')->name('berita.upload');
        Route::resource('/berita', 'BeritaController');
        Route::post('/berita/aktif', 'BeritaController@aktif')->name('berita.aktif');

        // Jenis Donasi
        Route::resource('/jenis_donasi', 'JendonController');
        Route::post('/jenis_donasi/aktif', 'JendonController@aktif')->name('jenis_donasi.aktif');

        // Sub Donasi
        Route::resource('/sub_donasi', 'SubdonController');
        Route::post('/sub_donasi/aktif', 'SubdonController@aktif')->name('sub_donasi.aktif');

        // Album
        Route::get('/album/change', 'AlbumController@change')->name('album.change');
        Route::resource('/album', 'AlbumController');
        Route::post('/album/aktif', 'AlbumController@aktif')->name('album.aktif');

        // File
        Route::resource('/file', 'FileController');
        Route::post('/file/aktif', 'FileController@aktif')->name('file.aktif');

        // Foto
        Route::get('/gallery/{album_id}', 'GalleryController@index')->name('gallery.index');
        Route::get('/gallery/{album_id}/create', 'GalleryController@create')->name('gallery.create');
        Route::post('/gallery/{album_id}/create', 'GalleryController@store')->name('gallery.store');
        Route::get('/gallery/{album_id}/show', 'GalleryController@show')->name('gallery.show');
        Route::get('/gallery/{album_id}/edit/', 'GalleryController@edit')->name('gallery.edit');
        Route::put('/gallery/{id}', 'GalleryController@update')->name('gallery.update');
        Route::delete('/gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');
        Route::post('/gallery/aktif', 'GalleryController@aktif')->name('gallery.aktif');

        // Galeri
        // Route::resource('/gallery', 'GalleryController');
        // Route::post('/gallery/aktif', 'GalleryController@aktif')->name('gallery.aktif');

        // Kategori Produk
        Route::resource('/kategori_produk', 'KatproController');
        Route::resource('/produk', 'ProdukController');

        // Rekening
        Route::resource('/rekening', 'RekeningController');
        Route::post('/rekening/aktif', 'RekeningController@aktif')->name('rekening.aktif');
    });
});




// End

// My Test
Route::livewire('/task', 'task.index')->name('task.index');
Route::livewire('/task/create', 'task.create')->name('task.create');
Route::livewire('/task/edit/{id}', 'task.edit')->name('task.edit');
