<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                @if($users->roles == 'superadmin')
                <li>
                    <a href="{{ route('admin.home') }}" class="waves-effect">
                        <i class="fas fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!--  <li>
                    <a href="{{ route('admin.donasi_online.index') }}" class="waves-effect">
                        <i class="fas fa-money-bill-wave"></i>
                        <span>Donasi Online</span>
                    </a>
                </li> -->

                <li>
                    <a href="{{ route('admin.donasi_manual.index') }}" class="waves-effect">
                        <i class="fas fa-money-bill-wave"></i>
                        <span>Data Donasi</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                        <i class="fas fa-hand-holding-usd"></i>
                        <span>Jenis Donasi</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.rekening.index') }}" class="waves-effect">
                        <i class="fas fa-money-check"></i>
                        <span>Rekening</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.katbout.index') }}" class="waves-effect">
                        <i class="fas fa-address-card"></i>
                        <span>Tentang Kami</span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-newspaper"></i>
                        <span>Konten</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.berita.index') }}">Post</a></li>
                        <li><a href="{{ route('admin.kategori_berita.index') }}">Kategori</a></li>
                        <li><a href="{{ route('admin.tag.index') }}">Tag</a></li>
                        {{-- <li><a href="{{ route('admin.manajemen_akun.index') }}">Komentar</a>
                </li> --}}
            </ul>
            </li>

            {{-- <li>
                    <a href="{{ route('admin.tag.index') }}" class="waves-effect">
            <i class="fas fa-tags"></i>
            <span>Tag</span>
            </a>
            </li> --}}

            <li>
                <a href="{{route('admin.slider.index')}}" class="waves-effect">
                    <i class="fas fa-image"></i>
                    <span>Slider</span>
                </a>
            </li>


            <li>
                <a href="{{ route('admin.album.index') }}" class="waves-effect">
                    <i class="fas fa-images"></i>
                    <span>Galeri</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.agenda.index') }}" class="waves-effect">
                    <i class="fas fa-calendar-alt"></i>
                    <span>Agenda</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.video.index') }}" class="waves-effect">
                    <i class="fab fa-youtube"></i>
                    <span>Video</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.file.index') }}" class="waves-effect">
                    <i class="fas fa-file-alt"></i>
                    <span>File</span>
                </a>
            </li>

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-hand-holding-usd"></i>
                        <span>Zakat</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.zakat.index') }}">Jenis Zakat</a></li>
            <li><a href="{{ route('admin.list_zakat.index') }}">Data Zakat</a></li>
            <li><a href="{{ route('admin.settingzakat.index')}}">Setting Zakat</a></li>
            </ul>
            </li> --}}

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fab fa-youtube"></i>
                        <span>Video</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.video.index') }}">Video</a></li>
            <li><a href="{{ route('admin.kategori_video.index') }}">Kategori Video</a></li>
            </ul>
            </li> --}}


            <!-- <li>
                    <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                        <i class="fas fa-list-alt"></i>
                        <span>Jenis Donasi</span>
                    </a>
                </li> -->

            <li>
                <a href="{{ route('admin.mitra.index') }}" class="waves-effect">
                    <i class="fas fa-handshake"></i>
                    <span>Mitra</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.pesan.index') }}" class="waves-effect">
                    <i class="fas fa-envelope"></i>
                    <span>Pesan</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.faqs.index') }}" class="waves-effect">
                    <i class="fas fa-question"></i>
                    <span>FAQ'S</span>
                </a>
            </li>

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="fas fa-cogs"></i>
                    <span>Pengaturan</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('admin.settings.index') }}">Pengaturan</a></li>
                    <li><a href="{{ route('admin.manajemen_akun.index') }}">Akun</a></li>
                </ul>
            </li>

            @elseif($users->roles == 'admin')
            <li>
                <a href="{{ route('admin.home') }}" class="waves-effect">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!--  <li>
                    <a href="{{ route('admin.donasi_online.index') }}" class="waves-effect">
                        <i class="fas fa-money-bill-wave"></i>
                        <span>Donasi Online</span>
                    </a>
                </li> -->

            <li>
                <a href="{{ route('admin.donasi_manual.index') }}" class="waves-effect">
                    <i class="fas fa-money-bill-wave"></i>
                    <span>Data Donasi</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                    <i class="fas fa-hand-holding-usd"></i>
                    <span>Jenis Donasi</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.katbout.index') }}" class="waves-effect">
                    <i class="fas fa-address-card"></i>
                    <span>Tentang Kami</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.berita.index') }}" class="waves-effect">
                    <i class="fas fa-newspaper"></i>
                    <span>Berita</span>
                </a>
            </li>

            {{-- <li>
                    <a href="{{ route('admin.tag.index') }}" class="waves-effect">
            <i class="fas fa-tags"></i>
            <span>Tag</span>
            </a>
            </li> --}}

            <li>
                <a href="{{ route('admin.album.index') }}" class="waves-effect">
                    <i class="fas fa-images"></i>
                    <span>Album</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.video.index') }}" class="waves-effect">
                    <i class="fab fa-youtube"></i>
                    <span>Video</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.file.index') }}" class="waves-effect">
                    <i class="fas fa-file-alt"></i>
                    <span>File</span>
                </a>
            </li>

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-hand-holding-usd"></i>
                        <span>Zakat</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.zakat.index') }}">Jenis Zakat</a></li>
            <li><a href="{{ route('admin.list_zakat.index') }}">Data Zakat</a></li>
            <li><a href="{{ route('admin.settingzakat.index')}}">Setting Zakat</a></li>
            </ul>
            </li> --}}

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fab fa-youtube"></i>
                        <span>Video</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.video.index') }}">Video</a></li>
            <li><a href="{{ route('admin.kategori_video.index') }}">Kategori Video</a></li>
            </ul>
            </li> --}}


            <!-- <li>
                    <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                        <i class="fas fa-list-alt"></i>
                        <span>Jenis Donasi</span>
                    </a>
                </li> -->

            {{-- <li>
                    <a href="{{route('admin.slider.index')}}" class="waves-effect">
            <i class="fas fa-image"></i>
            <span>Slider</span>
            </a>
            </li> --}}

            <li>
                <a href="{{ route('admin.mitra.index') }}" class="waves-effect">
                    <i class="fas fa-handshake"></i>
                    <span>Mitra</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.pesan.index') }}" class="waves-effect">
                    <i class="fas fa-envelope"></i>
                    <span>Pesan</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.faqs.index') }}" class="waves-effect">
                    <i class="fas fa-question"></i>
                    <span>FAQ'S</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.settings.index') }}" class="waves-effect">
                    <i class="fas fa-cogs"></i>
                    <span>Pengaturan</span>
                </a>
            </li>

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-cogs"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.settings.index') }}">Pengaturan</a></li>
            <li><a href="{{ route('admin.manajemen_akun.index') }}">Akun</a></li>
            </ul>
            </li> --}}

            @elseif($users->roles == 'petugas')
            <li>
                <a href="{{ route('admin.home') }}" class="waves-effect">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!--  <li>
                    <a href="{{ route('admin.donasi_online.index') }}" class="waves-effect">
                        <i class="fas fa-money-bill-wave"></i>
                        <span>Donasi Online</span>
                    </a>
                </li> -->

            <li>
                <a href="{{ route('admin.donasi_manual.index') }}" class="waves-effect">
                    <i class="fas fa-money-bill-wave"></i>
                    <span>Data Donasi</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                    <i class="fas fa-hand-holding-usd"></i>
                    <span>Jenis Donasi</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.katbout.index') }}" class="waves-effect">
                    <i class="fas fa-address-card"></i>
                    <span>Tentang Kami</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.berita.index') }}" class="waves-effect">
                    <i class="fas fa-newspaper"></i>
                    <span>Berita</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.album.index') }}" class="waves-effect">
                    <i class="fas fa-images"></i>
                    <span>Album</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.video.index') }}" class="waves-effect">
                    <i class="fab fa-youtube"></i>
                    <span>Video</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.file.index') }}" class="waves-effect">
                    <i class="fas fa-file-alt"></i>
                    <span>File</span>
                </a>
            </li>

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-hand-holding-usd"></i>
                        <span>Zakat</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.zakat.index') }}">Jenis Zakat</a></li>
            <li><a href="{{ route('admin.list_zakat.index') }}">Data Zakat</a></li>
            <li><a href="{{ route('admin.settingzakat.index')}}">Setting Zakat</a></li>
            </ul>
            </li> --}}

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fab fa-youtube"></i>
                        <span>Video</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.video.index') }}">Video</a></li>
            <li><a href="{{ route('admin.kategori_video.index') }}">Kategori Video</a></li>
            </ul>
            </li> --}}


            <!-- <li>
                    <a href="{{ route('admin.jenis_donasi.index') }}" class="waves-effect">
                        <i class="fas fa-list-alt"></i>
                        <span>Jenis Donasi</span>
                    </a>
                </li> -->

            {{-- <li>
                    <a href="{{route('admin.slider.index')}}" class="waves-effect">
            <i class="fas fa-image"></i>
            <span>Slider</span>
            </a>
            </li> --}}

            <li>
                <a href="{{ route('admin.mitra.index') }}" class="waves-effect">
                    <i class="fas fa-handshake"></i>
                    <span>Mitra</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.pesan.index') }}" class="waves-effect">
                    <i class="fas fa-envelope"></i>
                    <span>Pesan</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.faqs.index') }}" class="waves-effect">
                    <i class="fas fa-question"></i>
                    <span>FAQ'S</span>
                </a>
            </li>

            {{-- <li>
                    <a href="{{ route('admin.settings.index') }}" class="waves-effect">
            <i class="fas fa-cogs"></i>
            <span>Pengaturan</span>
            </a>
            </li> --}}

            {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-cogs"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.settings.index') }}">Pengaturan</a></li>
            <li><a href="{{ route('admin.manajemen_akun.index') }}">Akun</a></li>
            </ul>
            </li> --}}
            @endif
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
