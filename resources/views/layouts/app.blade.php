<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <title>@yield('title') | {{config('web_config')['WEB_TITLE']}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Portal Donasi : Jendela Langit" name="description">
    <meta content="Refi Fahreza" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ Storage::url('images/logo/'.config('web_config')['WEB_FAVICON'])}}"
        sizes="32x32">

    <!-- Bootstrap Css -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">

    <!-- Select2 -->
    <link href="{{asset('template/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Date Picker -->
    <link href="{{asset('template/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <!-- DataTables -->
    <link href="{{asset('template/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
        type="text/css">

    <!-- Icons Css -->
    <link href="{{asset('template/css/icons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/fonts/font-awesome/css/all.css')}}" rel="stylesheet" type="text/css">

    <!-- Plugin css -->
    <link href="{{asset('template/libs/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/libs/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">

    <!-- App Css-->
    <link href="{{asset('template/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css">

    <!-- Style Css -->
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css">

    @stack('style')

</head>

<body data-sidebar="light">

    <!-- Begin page -->
    <div id="layout-wrapper">
        <!-- Header -->
        @include('layouts.header')
        <!-- End of Header -->

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">
                    <!-- Content -->
                    <!-- @if (session('message'))
<div class="alert alert-{{session('Class')}} alert-dismissible fade show" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<strong>Well done!</strong> {{session('message')}}
</div>

@endif  -->
                    @yield('content')
                    <!-- End of Content -->
                </div>
            </div>
        </div>
        <!-- Sidebar -->
        @include('layouts.footer')
        <!-- End of Sidebar -->
    </div>

    <form action="{{ route('logout') }}" method="POST">
        @csrf
        <div id="ModalLogout" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Keluar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin Ingin Keluar ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">Keluar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- JAVASCRIPT -->

    <script src="{{asset('template/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('template/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('template/libs/metismenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('template/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('template/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{asset('template/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('template/libs/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('frontend/vendor/venobox/venobox.js')}}"></script>

    <!-- Required datatable js -->
    <script src="{{asset('template/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('template/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('template/js/pages/datatables.init.js')}}"></script>

    <!-- plugin js -->
    <script src="{{asset('template/libs/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('template/libs/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('template/libs/fullcalendar/fullcalendar.min.js')}}"></script>

    {{-- <script src="{{ asset('template/libs/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script> --}}
    <script src="https://cdn.tiny.cloud/1/mim6gcf27qdak2zsqarjkdk9338007cr9tb5niisjf32usay/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>

    <!-- Sweet alert init js-->
    <!-- <script src="{{asset('template/js/pages/sweet-alerts.init.js')}}"></script>
  <script src="{{asset('template/libs/sweetalert2/sweetalert2.min.js')}}"></script> -->

    <!-- Calendar init -->
    <script src="{{asset('template/js/pages/calendar.init.js')}}"></script>

    <!-- App js -->
    <script src="{{asset('template/js/app.js')}}"></script>
    <script src="{{ asset('template/js/main.js') }}"></script>

    @if (session('message'))
    <script>
        $(document).ready(function(){
    function ops() {
      Swal.fire(
        '{{session('header')}}',
        '{{session('message')}}',
        '{{session('Class')}}'
        )
      }
      ops();
      });

    </script>
    @endif

    @stack('scripts')
</body>

</html>
