<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="{{route('admin.home')}}" class="logo">
                    <span class="logo-sm">
                        <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}" alt="Logo"
                            height="35" style="padding: 5px">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}" alt="Logo"
                            height="60" style="padding: 5px">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                <i class="mdi mdi-menu"></i>
            </button>
        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth()->user()->icon != NULL)
                    {{-- <img src="{{  Storage::url('images/user_icon/'.$akun->icon) }}" width="200"> --}}
                    <img class="rounded-circle header-profile-user"
                        src="{{ Storage::url('images/user_icon/'. Auth()->user()->icon)}}" alt="Header Avatar"
                        style="height: 45px !important; width: 48px !important; background-color: #fff;">
                    @else
                    {{-- <img src="{{  Storage::url('images/user_icon/default-icon.png') }}" width="200"> --}}
                    <img class="rounded-circle header-profile-user"
                        src="{{ Storage::url('images/user_icon/default-icon.png')}}" alt="Header Avatar" style="height: 45px !important; width: 48px !important; background-color: #fff;>
                    @endif
                </button>
                <div class=" dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a class="dropdown-item" href="{{ route('main') }}" target="_blank"><i
                            class="fas fa-desktop font-size-17 text-muted align-middle mr-1"></i> Lihat Website</a>
                    <a class="dropdown-item" href="{{ route('admin.profil.index') }}"><i
                            class="mdi mdi-account-circle font-size-17 text-muted align-middle mr-1"></i> Profil</a>
                    <div class="dropdown-divider"></div>
                    <button type="button" class="dropdown-item text-danger" data-toggle="modal"
                        data-target="#ModalLogout"><i
                            class="mdi mdi-power font-size-17 text-muted align-middle mr-1 text-danger"></i>
                        Keluar</button>
            </div>
        </div>
    </div>
    </div>
</header>
