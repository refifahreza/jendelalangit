<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <span>&copy; Copyright© {{date('Y')}} <a href="https://refifahreza.com">Refi Fahreza</a>- All
                    rights
                    reserved</span>
            </div>
        </div>
    </div>
</footer>
