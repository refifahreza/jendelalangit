@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio3 Section ======= -->
<section id="portfolio3" class="section-bg">
  <div class="container">

    <header class="section-header judul"> 
      <h3 class="section-title">Galeri Photo - {{ $albums->album_name }}</h3>
    </header>

    <div class="row portfolio3-container" style="margin-top: 30px;">
      @foreach($galleryd as $gallery)
      <div class="col-lg-4 col-md-6 portfolio3-item filter-web wow fadeInUp" data-wow-delay="0.1s">
        <div class="portfolio3-wrap">
          <figure>
            <img src="{{ Storage::url('images/gallery/'.$gallery->foto) }}" class="img-fluid" alt="">
            <a href="{{ Storage::url('images/gallery/'.$gallery->foto) }}" class="link-preview venobox" data-gall="portfolioGallery" title="{{ $gallery->nama_foto . ' ' . $loop->iteration }}"><i class="ion ion-eye"></i></a>
          </figure>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
<!-- End Portfolio3 Section -->

@endsection

