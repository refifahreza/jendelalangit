@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="contact" class="section-bg">
    <div class="container" align="center">
        <header class="section-header judul">
            <h3 class="section-title">Kontak</h3>
        </header>
        <div class="row" style="margin-top: 30px;">
            <div class="col-lg-8" style="margin-top: 30px;">
                <header class="">
                    <h4 style="text-align: left !important; color: #706e6e; font-weight: 600;">Hubungi Kami</h4>
                    <hr style="width: 50%; margin-left: 0px;">
                </header>
                <div class="aos-init aos-animate" data-aos="fade-up">
                    {{-- <div class="row contact-info">

                        <div class="col-md-6">
                            <div class="contact-address">
                                <i class="fas fa-map-marker-alt mb-3"></i>
                                <h5>Alamat</h5>
                                <address>Jl. Danau Towuti No.8, RT.26 (depan TK Ruhui Rahayu), Sungai Pinang Luar, Kec.
                                    Samarinda Kota, Kota Samarinda,
                                    Kalimantan Timur 75117</address>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="contact-phone">
                                <i class="fas fa-phone mb-3"></i>
                                <h5>Nomor Handphone</h5>
                                <p><a href="tel:+155895548855">+6282156333356</a></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="contact-wa">
                                <i class="fab fa-whatsapp mb-3"></i>
                                <h5>Nomor Handphone</h5>
                                <p><a href="tel:+155895548855">+6282156333356</a></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="contact-email">
                                <i class="fas fa-envelope mb-3"></i>
                                <h5>Email</h5>
                                <p><a href="mailto:info@example.com">jendelalangit98@gmail.com</a></p>
                            </div>
                        </div>

                    </div> --}}
                    <p class="mb-4 text-left">Silahkan menghubungi kami melalui private message melalui form yang berada
                        pada
                        bagian kanan
                        halaman ini. Kritik dan
                        saran Anda sangat penting bagi kami untuk terus meningkatkan kualitas produk dan layanan yang
                        kami berikan kepada Anda.</p>
                    <div class="form">
                        <form action="{{route('pesanstore')}}" method="post" role="form" class="php-email-form">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" name="nama" class="form-control" id="nama"
                                        placeholder="Nama Lengkap" required="">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                        required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subjek" id="subjek" placeholder="Subjek"
                                    required="">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="pesan" rows="5" cols="5" placeholder="Pesan"
                                    required=""></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div class="text-center"><button type="submit">Kirim Pesan</button></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" style="margin-top: 30px;">
                <header class="">
                    <h4 style="text-align: left !important; color: #706e6e; font-weight: 600;">Kunjungi Juga Instagram
                        Kami
                    </h4>
                    <hr style="width: 50%; margin-left: 0px;">
                </header>
                <blockquote class="instagram-media"
                    data-instgrm-permalink="https://www.instagram.com/p/COpJQ_2J8lG/?utm_source=ig_embed&amp;utm_campaign=loading"
                    data-instgrm-version="13"
                    style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; width: 100%;">
                </blockquote>
                <script async src="//www.instagram.com/embed.js"></script>
            </div>
        </div>
    </div>
</section>
<!-- End Portfolio Section -->

@endsection
