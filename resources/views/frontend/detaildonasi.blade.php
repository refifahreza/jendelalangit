@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
    <div class="container">

        <header class="section-header judul">
            <h3 class="section-title">Detail Donasi</h3>
        </header>

        <div class="card card-donasi mt-4">
            <!-- <h5 class="card-header" align="center" style="">Jendela Langit</h5> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card-body form-donasi">
                        <h4><strong>Pilihan Donasi</strong></h4>
                        <hr>
                        <div class="input-group mb-3 mt-3">
                            <input type="text" class="form-control" aria-label="Default"
                                aria-describedby="inputGroup-sizing-default" value="{{ $donasis->jenis->jendon_name }}"
                                disabled="disabled">
                        </div>
                        <div class="input-group mb-3 mt-3">
                            <input type="text" id="rupiah" class="form-control" aria-label="Default"
                                aria-describedby="inputGroup-sizing-default"
                                value="Rp. {{ number_format($donasis->jumlah) }}" disabled="disabled">
                        </div>

                        <hr style="margin: 20px 0; ">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-label="Default"
                                aria-describedby="inputGroup-sizing-default" value="{{ $donasis->nama_donatur }}"
                                disabled="disabled">
                        </div>
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" aria-label="Default"
                                aria-describedby="inputGroup-sizing-default" value="{{ $donasis->email }}"
                                disabled="disabled">
                        </div>
                        {{-- <div class="input-group mb-3">
              <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="{{ $donasis->notelp }}"
                        disabled="disabled">
                    </div> --}}

                    <!-- <div class="row">
              <div class="input-group mb-3">
                <div class="col-10">
                  <span>Doa atau Pesan (Opsional)</span>
                </div>
              </div>
            </div>
            <div id="dvPassport">
              <textarea type="text" readonly class="form-control textarea mb-3" id="txtPassportNumber" maxlength="150" rows="2" style=" resize: none; width: 100%;min-height: 70px;height: 100%; font-family: inherit;padding: 0.75em 0.625em;border-width: 1px;border-style: solid;border-color: rgb(204, 208, 211);border-image: initial;background: rgb(233 236 239);border-radius: 5px;">{{ $donasis->pesan }}</textarea>
            </div> -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <h4><strong>Konfirmasi Pembayaran</strong></h4>
                    <hr>
                    <div class="alert alert-success" style="margin-top: 10px; margin-bottom: 0;">
                        Kode Pembayaran Anda : {{ $donasis->kode_donasi }}
                    </div>
                    <small>*Kode Pembayaran telah terkirim ke Email anda</small>
                    <p class="pt-3">Metode Pembayaran Menggunakan :</p>
                    <img class="icon mt-3 mb-3" src="{{ Storage::url('images/rekening/'.$donasis->rekening->logo) }}"
                        width="200">
                    <p>Silahkan klik tombol dibawah ini untuk Konfirmasi Pembayaran :</p>
                    <div class="row">
                        <div class="col-md-12 mt-2">
                            <center><a href="{{route('status-donasi', $donasis->kode_donasi)}}" class="btn-donasi">Bayar
                                    Sekarang</a></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</section>
<!-- End Portfolio Section -->

@endsection
