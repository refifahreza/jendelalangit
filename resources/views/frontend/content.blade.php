@extends('frontend.layouts.app2')
@section('content')
<!-- ======= Intro Section ======= -->
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators"></ol>
            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $key => $slider)
                <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                    <img class="trian-gambar-utama" src="{{Storage::url('images/slider/'.$slider->picture)}}"
                        alt="{{{ $slider->header_text}}}">
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<!-- ======= End Section ======= -->

<main id="main">

    <!-- ======= Donation Section ======= -->
    <section class="wow fadeInUp" style="padding: 75px 0px 0px 0px !important;">
        <div class="container">
            <header class="section-header wow fadeInUp">
                <h3>Donasi Aman, Mudah, dan Terpercaya</h3>
            </header>

            <div class="row mt-5">
                <div class="col-md-6 col-12">
                    <div class="card card-donasi">

                        <div class="card-body form-donasi">
                            <h5 class="mb-3" align="center" style="color: #142766 !important;">Bank Transfer</h5>
                            <hr class="mb-3">
                            <form action="{{ route('detail-donasi') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="input-group">
                                    <select type="text" class="form-control select2" required id="jenis" name="jenis"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default"
                                        style="width: 100%">
                                        <option value="" selected disabled>Jenis Donasi</option>
                                        @foreach($jendons as $jendon)
                                        <option value="{{ $jendon->id }}">{{ $jendon->jendon_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group mt-3">
                                    <select type="text" class="form-control select2" required id="rekening"
                                        name="rekening" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default" style="width: 100%">
                                        <option value="" selected disabled>Metode Pembayaran</option>
                                        @foreach($rekenings as $rekening)
                                        <option value="{{ $rekening->id }}">{{ $rekening->rekening_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group mb-3 mt-3">
                                    <input type="text" inputmode="numeric" id="jumlah" required name="jumlah"
                                        autocomplete="off" class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default" placeholder="Masukan Nominal" />
                                </div>
                                <div class="row" id="myDIV">
                                    <div class="col-6 col-md-3">
                                        <button class="form-control btn-nominal" data-uang="Rp. 10.000">
                                            <span style="font-weight: 600; font-size: 13px;">Rp. 10.000</span>
                                        </button>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <button class="form-control btn-nominal" data-uang="Rp. 20.000">
                                            <span style="font-weight: 600; font-size: 13px;">Rp. 20.000</span>
                                        </button>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <button class="form-control btn-nominal2" data-uang="Rp. 50.000">
                                            <span style="font-weight: 600; font-size: 13px;">Rp. 50.000</span>
                                        </button>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <button class="form-control btn-nominal2" data-uang="Rp. 100.000">
                                            <span style="font-weight: 600; font-size: 13px;">Rp. 100.000</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="input-group mb-3 mt-3">
                                    <input type="text" inputmode="text" class="form-control" required id="names"
                                        name="names" autocomplete="off" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default" placeholder="Nama Lengkap">
                                </div>
                                <div class="row">
                                    <div class="input-group mb-3">
                                        <div class="col-12">
                                            <span class="mr-3">Sembunyikan nama saya</span>
                                            <input type="checkbox" id="anony" name="anony" value="ya" width="10"
                                                style="transform: scale(1.5) !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="email" inputmode="email" class="form-control" required id="emails"
                                        name="emails" autocomplete="off" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default" placeholder="Email">
                                </div>
                                {{-- <div class="input-group mb-3">
                                    <span class="mt-2 mr-2">+62</span><input type="text" inputmode="tel" minlength="10"
                                        maxlength="12" class="form-control" required id="telp" name="telp"
                                        autocomplete="off" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default" placeholder="Nomor Ponsel">
                                </div> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <center><button type="submit" class="btn btn-donasi"
                                                style="border: none;">Proses Pembayaran</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 infaq">
                    <div class="clear"></div>
                    <span>Total Infaq :</span>
                    <div class="row mt-2">
                        <div class="col-md-6 margin">
                            <div class="total">
                                <h3>Rp. {{ number_format($count,0,',','.') }}</h3>
                            </div>
                        </div>
                        <div class="col-md-6 margin">
                            <a type="button" data-toggle="modal" data-target=".modal-konfirmasi"
                                class="btn-konfir">KONFIRMASI PEMBAYARAN</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row" align="center">
                        <div class="col-12 col-md-12">
                            <div class="form-donatur mt-2 mb-3">
                                <div class="row">
                                    <div class="col-12">
                                        <img src="{{ asset('frontend/images/cara-donasi.png')}}" alt="" srcset=""
                                            width="93%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-donatur mt-2 mb-3">
                        <table class="rwd-table">
                            <thead>
                                <tr>
                                    <th scope="col" class="align-middle text-center">Donatur</th>
                                    <th scope="col" class="align-middle text-center">Via Bayar</th>
                                    <th scope="col" class="align-middle text-center">Nominal</th>
                                    <th scope="col" class="align-middle text-center">Waktu Donasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($donmans as $donman)
                                <tr>
                                    <td scope="col" class="align-middle text-center">{{ $donman->nama_donatur }}</td>
                    <td scope="col" class="align-middle text-center">{{ $donman->bank_donatur }}</td>
                    <td scope="col" class="align-middle text-center">Rp.
                        {{ number_format($donman->jumlah,0,',','.') }}</td>
                    <td scope="col" class="align-middle text-center">
                        {{ \Carbon\Carbon::parse($donman->created_at)->diffForHumans() }}</td>
                    </tr>
                    @endforeach
                    @foreach($donlines as $donline)
                    <tr>
                        <td scope="col" class="align-middle text-center">{{ $donline->nama_donatur }}</td>
                        <td scope="col" class="align-middle text-center">Midtrans Payment</td>
                        <td scope="col" class="align-middle text-center">Rp.
                            {{ number_format($donline->jumlah,0,',','.') }}</td>
                        <td scope="col" class="align-middle text-center">
                            {{ \Carbon\Carbon::parse($donline->created_at)->diffForHumans() }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
                <center><a href="{{route('listdonatur')}}" class="btn-donatur">List Donatur</a></center> --}}
            </div>
    </section>
    <!-- ======= End Section ======= -->

    <!-- ======= News Section ======= -->
    <section id="portfolio" class="{{ (count($beritas) == 0) ? "d-none" : "" }}"
        style="padding: 75px 0px 0px 0px !important;">
        {{-- <section id="portfolio" class="{{ (count($beritas) == 0) ? "d-none" : "" }}"
        style="padding: 48px 0px 45px 0px !important;"> --}}
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <header class="section-header wow fadeInUp">
                        <h3 class="section-title">Berita</h3>
                    </header>
                    <div class="row mt-5">
                        {{-- Start --}}
                        @foreach($beritas as $key => $berita)
                        <div class="col-md-6 col-sm-6" style="padding: 0;">
                            <div class="news-card">
                                <a href="{{ route('detail_berita', $berita->title_slug) }}"
                                    class="news-card__card-link"></a>
                                <img src="{{ Storage::url('images/berita/'.$berita->thumnail) }}"
                                    alt="{{ $berita->title }}" class="news-card__image">
                                <div class="news-card__text-wrapper">
                                    <h2 class="news-card__title">{{ $berita->title }}</h2>
                                    <div class="news-card__post-date">
                                        {{ \Carbon\Carbon::parse($berita->created_at)->format('d, M Y H:i') }}
                                    </div>
                                    <div class="news-card__details-wrapper">
                                        <p class="news-card__excerpt">{{ $berita->kutipan }}</p>
                                        <a href="{{ route('detail_berita', $berita->title_slug) }}"
                                            class="news-card__read-more">Baca
                                            Selengkapnya <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{-- End --}}
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{route('berita')}}" class="btn-berita wow fadeIn"
                            style="margin-top: 20px; margin-bottom: 0px !important;">Baca Semua Berita</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= End Section ======= -->

    <!-- ======= Album Section ======= -->
    <section id="portfolio" class="{{ (count($albums) == 0) ? "d-none" : "" }}"
        style="padding: 75px 0px 0px 0px !important;">
        <div class="container">
            <header class="section-header wow fadeInUp">
                <h3 class="section-title">Album</h3>
            </header>

            <div class="row mt-5">
                @foreach($albums as $key => $album)
                <div
                    class="col-lg-4 col-md-6 portfolio-item filter-{{ strtolower(str_replace(' ','',$album->album_name)) }} wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="{{ Storage::url('images/album/'.$album->thumbnail) }}" class="img-fluid"
                                alt="{{{ $album->album_name}}}">
                            <a href="{{ Storage::url('images/album/'.$album->thumbnail) }}" class="link-preview venobox"
                                data-gall="portfolioGallery" title="{{{ $album->album_name}}}"><i
                                    class="ion ion-eye"></i></a>
                            <a href="{{route('detail_album', $album->album_slug)}}" class="link-details"
                                title="Lebih lengkap"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="{{ route('detail_album', $album->album_slug) }}"
                                    style="font-size: 12px !important;">{{ $album->album_name}}</a>
                            </h4>
                        </div>
                    </div>
                </div>
                @endforeach
                {{-- End --}}
            </div>
            <div class="d-flex justify-content-end">
                <a href="{{route('album')}}" class="btn-berita wow fadeInUp"
                    style="margin-top: 20px; margin-bottom: 0px !important;">Lihat Semua
                    Album
                </a>
            </div>
        </div>
    </section>
    <!-- ======= End Section ======= -->

    <!-- ======= Video Section ======= -->
    <section id="portfolio" class="{{ (count($videos) == 0) ? "d-none" : "" }}"
        style="padding: 75px 0px 0px 0px !important;">
        <div class="container">
            <header class="section-header wow fadeInUp">
                <h3 class="section-title">Video Terbaru</h3>
            </header>

            <div class="row mt-5">
                @foreach ($videos as $video)
                <div class="col-12 col-md-6 col-xl-6">
                    {{-- <div class="owl-carousel owl-theme"> --}}
                    <div class="item-video" data-merge="3">
                        <iframe width="100%" height="360" src="https://www.youtube.com/embed/{{ $video->link }}"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                        </iframe>
                    </div>
                    {{-- </div> --}}
                </div>
                @endforeach
                <!-- Blog Slider -->
                {{-- <div class="blog-slider "> --}}
                <!-- Single Slider -->
                {{-- @foreach ($videos as $video) --}}
                {{-- <div class="single-blog">
                                <div class="video-play">
                                    <iframe width="80%" height="360" src="https://www.youtube.com/embed/Yf-dfwYWTOw" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>

                                </div>
                            </div> --}}
                {{-- @endforeach --}}
                <!--/ End Single Slider -->
                {{-- </div> --}}
                <!--/ End Blog Slider -->
            </div>

        </div>
    </section>
    <!-- ======= End Section ======= -->

    <!-- ======= Our Clients Section ======= -->
    <section id="clients" class="wow fadeInUp {{ (count($mitras) == 0) ? "d-none" : "" }}"
        style="padding: 75px 0px 0px 0px !important;">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>Mitra</h3>
            </header>
            <div class="owl-carousel clients-carousel mt-4">
                @foreach($mitras as $mitra)
                <img src="{{ Storage::url('images/mitra/'.$mitra->mitra_logo) }}" alt="{{ $mitra->mitra_name }}">
                @endforeach
            </div>
        </div>
    </section>
    <!-- ======= End Section ======= -->

</main>
@endsection

@push ('scripts')
<script
    src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"
    data-client-key="{{ config('services.midtrans.clientKey') }}">
</script>
<script>
    $(document).ready(function () {
        $('#jenis').select2({
            width: 'resolve'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#rekening').select2({
            width: 'resolve'
        });
    });
</script>
<script>
    // Add active class to the current button (highlight it)
    var header = document.getElementById("myDIV");
    var btns = header.getElementsByClassName("btn-nominal");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("aktif");
            if (current.length > 0) {
                current[0].className = current[0].className.replace(" aktif", "");
            }
            this.className += " aktif";
        });
    }
</script>
<script>
    // Add active class to the current button (highlight it)
    var header = document.getElementById("myDIV");
    var btns = header.getElementsByClassName("btn-nominal2");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("aktif");
            if (current.length > 0) {
                current[0].className = current[0].className.replace(" aktif", "");
            }
            this.className += " aktif";
        });
    }
</script>
<script>
    $("input[id='telp']").on('input', function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

    // Klik uang
    $('.btn-nominal').click(function () {
        var uang = $(this).attr('data-uang');
        $('#jumlah').val(uang);
    });

    // function submitForm() {
    //   $.ajaxSetup({
    //     headers: {
    //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    //   });
    //   $.post("{{ route('donasi.store') }}",
    //   {
    //     _method: 'POST',
    //     _token: '{{ csrf_token() }}',
    //     jenis: $('#jenis').val(),
    //     notes: $('#notes').val(),
    //     names: $('#names').val(),
    //     telp: $('#telp').val(),
    //     jumlah: $('#jumlah').val(),
    //     anony: $('#anony:checkbox:checked').val(),
    //   },
    //   function (data, status) {
    //     snap.pay(data.snap_token, {
    //       onSuccess: function (result) {
    //         location.reload();
    //       },
    //       onPending: function (result) {
    //         location.reload();
    //       },
    //       onError: function (result) {
    //         location.reload();
    //       }
    //     });
    //   });
    //   return false;
    // }

    var rupiah = document.getElementById("jumlah");
    rupiah.addEventListener("keyup", function (e) {
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }
</script>
<script>
    $('.owl-carousel').owlCarousel({
        autoplay: true,
        center: true,
        dots: false,
        loop: false,
        pagination: false,
        autoHeight: true,
        nav: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            }
        }

    });
</script>
<script>
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
                $("#AddPassport").hide();
            } else {
                $("#dvPassport").hide();
                $("#AddPassport").show();
            }
        });
    });
</script>
<!-- <script>
      $('.venobox').venobox({
    framewidth : '400px',                            // default: ''
    frameheight: '300px',                            // default: ''
    border     : '10px',                             // default: '0'
    bgcolor    : '#5dff5e',                          // default: '#fff'
    titleattr  : 'data-title',                       // default: 'title'
    numeratio  : true,                               // default: false
    infinigall : true,                               // default: false
    share      : ['facebook', 'twitter', 'download'] // default: []
  });
</script> -->

@endpush
