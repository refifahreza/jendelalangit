@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container">

    <header class="section-header judul">
      <h3 class="section-title">Manual Payment</h3>
    </header>

    <div class="card card-donasi mt-4">
      <div class="row">
        <div class="col-md-6">
          <div class="card-body form-donasi">
            <h4><strong>Data Donatur</strong></h4>
            <hr>
            <form action="{{ route('detail-donasi', $rekenings->rekening_slug) }}" method="post">
              @csrf
              <div class="input-group mt-3">
                <select type="text" class="form-control" required id="jenis" name="jenis" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                  <option value="">--- Jenis Donasi ---</option>
                  @foreach($jendons as $jendon)
                  <option value="{{ $jendon->id }}">{{ $jendon->jendon_name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="input-group mb-3 mt-3">
                <input type="text" id="jumlah" required name="jumlah" autocomplete="off" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Masukan Nominal"/>
              </div>
              <div class="row" id="myDIV">
                <div class="col-6 col-md-3">
                  <button class="form-control btn-nominal" data-uang="Rp. 10.000">
                    <span style="font-weight: 600; font-size: 13px;">Rp. 10.000</span>
                  </button>
                </div>
                <div class="col-6 col-md-3">
                  <button class="form-control btn-nominal" data-uang="Rp. 20.000">
                    <span style="font-weight: 600; font-size: 13px;">Rp. 20.000</span>
                  </button>
                </div>
                <div class="col-6 col-md-3">
                  <button class="form-control btn-nominal" data-uang="Rp. 50.000">
                    <span style="font-weight: 600; font-size: 13px;">Rp. 50.000</span>
                  </button>
                </div>
                <div class="col-6 col-md-3">
                  <button class="form-control btn-nominal" data-uang="Rp. 100.000">
                    <span style="font-weight: 600; font-size: 13px;">Rp. 100.000</span>
                  </button>
                </div>
              </div>

              <hr style="margin: 20px 0; ">
              <div class="input-group mb-3">
                <input type="text" class="form-control" required id="names" name="names" autocomplete="off" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Nama Lengkap">
              </div>
              <div class="input-group mb-3">
                <input type="email" class="form-control" required id="emails" name="emails" autocomplete="off" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Email">
              </div>
              <div class="input-group">
                <span class="mt-2 mr-2">+62</span><input type="text" minlength="10" maxlength="12" class="form-control" required id="telp" name="telp" autocomplete="off" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Nomor Ponsel">
              </div>
              <div class="row mt-3">
                <div class="input-group mb-3">
                  <div class="col-10">
                    <span>Sembunyikan nama saya (Hamba Allah)</span>
                  </div>
                  <div class="col-2">
                    <label for="nama">
                      <input type="checkbox" id="anony" name="anony" value="ya" width="10">
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="input-group mb-3">
                  <div class="col-10">
                    <span>Doa atau Pesan (Opsional)</span>
                  </div>
                  <div class="col-2">
                    <label for="chkPassport">
                      <input type="checkbox" id="chkPassport" width="10">
                    </label>
                  </div>
                </div>
              </div>
              <div id="dvPassport" style="display: none;">
                <textarea type="text" autocomplete="off" class="form-control textarea mb-3" name="notes" id="txtPassportNumber" maxlength="150" rows="2" style=" resize: none; width: 100%;min-height: 70px;height: 100%; font-family: inherit;padding: 0.75em 0.625em;border-width: 1px;border-style: solid;border-color: rgb(204, 208, 211);border-image: initial;background: rgb(255, 255, 255);border-radius: 5px;">-</textarea>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-body">
              <h4><strong>Metode Pembayaran Menggunakan : </strong></h4>
              <hr>

              <p style="text-align: center; font-size: 30px;">{{ $rekenings->rekening_name }}</p>
              <center><img class="icon mt-3 mb-3" src="{{ Storage::url('images/rekening/'.$rekenings->logo) }}" width="200" align="center"></center>
              <div class="row">
                <div class="col-md-12 mt-2">
                  <button type="submit" class="btn btn-donasi" style="border: none;">Proses Pembayaran</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
</section>
<!-- End Portfolio Section -->

@endsection

@push ('scripts')
<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("btn-nominal");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("aktif");
    if (current.length > 0) { 
      current[0].className = current[0].className.replace(" aktif", "");
    }
    this.className += " aktif";
  });
}
</script>
<script>
    // Klik uang
    $('.btn-nominal').click(function() {
      var uang = $(this).attr('data-uang');
      $('#jumlah').val(uang);
    });

    $("input[id='telp']").on('input', function() {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

    var rupiah = document.getElementById("jumlah");
    rupiah.addEventListener("keyup", function(e) {
      rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    function formatRupiah(angka, prefix) {
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }
  </script>
  <script>
    $(function () {
      $("#chkPassport").click(function () {
        if ($(this).is(":checked")) {
          $("#dvPassport").show();
          $("#AddPassport").hide();
        } else {
          $("#dvPassport").hide();
          $("#AddPassport").show();
        }
      });
    });

  </script>
  @endpush
