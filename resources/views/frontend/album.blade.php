@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container">


    <header class="section-header judul">
      <h3 class="section-title">Album Photo</h3>
    </header>

    <div class="row">
      <div class="col-lg-12">
        <ul id="portfolio-flters">
          <li data-filter="*" class="filter-active">Semua</li>
          @foreach($albums as $key => $album)
          <li data-filter=".filter-{{ strtolower(str_replace(' ','',$album->album_name)) }}">{{ strtoupper($album->album_name) }}</li>
          @endforeach
        </ul>
      </div>
    </div>

    <div class="row portfolio-container">
      @foreach($gallerys as $key => $gallery)
      <div class="col-lg-4 col-md-6 portfolio-item filter-{{ strtolower(str_replace(' ','',$gallery->album->album_name)) }}  wow fadeInUp"  style="position: relative; height: 261px !important;">
        <div class="portfolio-wrap" align="center">
         <figure>
          <img src="{{ Storage::url('images/gallery/'.$gallery->foto) }}" class="img-fluid" alt="{{ $gallery->nama_foto }}">
          <a href="{{ Storage::url('images/gallery/'.$gallery->foto) }}" class="link-preview venobox" data-lightbox="portfolio" data-title="{{ strtolower(str_replace(' ','',$gallery->album->album_name)) . ' ' . $loop->iteration }}"  data-gall="portfolioGallery" title="{{{ $gallery->nama_foto}}}" style="margin-left: 30px !important;"><i class="ion ion-eye"></i></a>
        </figure>
      </div>
    </div>
    @endforeach
    {{-- End --}}
  </div>
      </div> 
</div>
</section>
<!-- End Portfolio Section -->

@endsection
