@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container">

    <header class="section-header judul">
      <h3 class="section-title">List Donatur</h3>
    </header>

    <div class="table-wrapper-scroll-y my-custom-scrollbar" style="margin-top: 30px;">
      <table class="rwd-table mb-0">
        <thead>
          <tr>
            <th scope="col" class="align-middle text-center">Donatur</th>
            <th scope="col" class="align-middle text-center">Via Bayar</th>
            <th scope="col" class="align-middle text-center">Nominal</th>
            <th scope="col" class="align-middle text-center">Waktu Donasi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($donmans as $donman)
          <tr>
           <td scope="col" class="align-middle text-center">{{ $donman->nama_donatur }}</td>
           <td scope="col" class="align-middle text-center">{{ $donman->bank_donatur }}</td>
           <td scope="col" class="align-middle text-center">Rp. {{ number_format($donman->jumlah,0,',','.') }}</td>
           <td scope="col" class="align-middle text-center">{{ \Carbon\Carbon::parse($donman->created_at)->diffForHumans() }}</td>
         </tr>
         @endforeach
          @foreach($donlines as $donline)
          <tr>
           <td scope="col" class="align-middle text-center">{{ $donline->nama_donatur }}</td>
           <td scope="col" class="align-middle text-center">Midtrans Payment</td>
           <td scope="col" class="align-middle text-center">Rp. {{ number_format($donline->jumlah,0,',','.') }}</td>
           <td scope="col" class="align-middle text-center">{{ \Carbon\Carbon::parse($donline->created_at)->diffForHumans() }}</td>
         </tr>
         @endforeach
       </tbody>
     </table>
  </div>
</section>
<!-- End Portfolio Section -->

@endsection

@push('scripts')
{{-- <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function(){
    $('.data').DataTable();
  });
</script> --}}
@endpush
