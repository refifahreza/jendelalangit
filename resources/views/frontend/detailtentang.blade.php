@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
    <div class="container">

        <header class="section-header judul">
            <h3 class="section-title">Tentang Kami : {{$katbouts->jenis_tentang}}</h3>
        </header>
        <div class="mt-5" style="width: 100%;">
            @if ($katbouts->gambar === NULL)
            {!! $katbouts->konten !!}
            @else
            <img src="{{ Storage::url('images/katbout/'.$katbouts->gambar) }}" class="img-fluid"
                alt="{{{$katbouts->gambar}}}" align="center" style="width: 100%;">
            @endif
        </div>
    </div>
</section>
<!-- End Portfolio Section -->

@endsection
