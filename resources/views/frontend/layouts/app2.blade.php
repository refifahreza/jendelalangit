<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{$title}}</title>
    <meta name="copyright" content="Refi Fahreza">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta id="token" name="token" content="{{ csrf_token() }}">

    <!-- Favicons -->
    <!-- <link rel="icon" href="{{asset('template/images/favicon.png')}}" sizes="32x32"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@424&display=swap" rel="stylesheet">

    <!-- Select2 -->

    <!-- Vendor CSS Files -->
    <link href="{{asset('frontend/vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('template/fonts/font-awesome/css/all.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('frontend/vendor/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/libs/sweet-alert2/sweetalert21.css')}}" rel="stylesheet">
    <link href="{{asset('template/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Template Main CSS File -->
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    {{-- <link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet"> --}}
</head>

<body>

    <a href="https://api.whatsapp.com/send?phone=62{{ config('web_config')['WHATSAPP']}}?text=Assalamualaikum,%20permisi"
        target="popup"
        onclick="window.open('https://api.whatsapp.com/send?phone=62{{ config('web_config')['WHATSAPP'] }}&text=Assalamualaikum,%20permisi','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
        class="whatsapp">
        <i class="fab fa-whatsapp"></i>
    </a>

    @include('frontend.layouts.navbar2')

    @yield('content')

    @include('frontend.pembayaran')
    @include('frontend.konfirmasi')
    @include('frontend.tips1')
    @include('frontend.tips2')


    @include('frontend.layouts.footer')

    <script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/owl.carousel/owl.carousel.js')}}"></script>
    <script src="{{asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/venobox/venobox.js')}}"></script>
    <script src="{{asset('frontend/vendor/superfish/superfish.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/hoverIntent/hoverIntent.js')}}"></script>
    <script src="{{asset('frontend/vendor/jquery-touchswipe/jquery.touchSwipe.min.js')}}"></script>
    <script src="{{asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
    @if (session('message'))
    <script>
        $(document).ready(function(){
        // console.log($('#exampleInputFile').val())
        function ops() {
            Swal.fire(
            '{{session('message')}}!',
            '{{session('pesan')}}',
            '{{session('Class')}}'
            )
        }
        ops();
    });

    </script>
    @endif

    @stack('scripts')
</body>

</html>
