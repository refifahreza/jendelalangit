<!-- ======= Header ======= -->
<header id="header2">
    <div class="top-header">
        <div class="top-nav">
            <div class="to-right">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div id="logo2" class="pull-left">
            <a href="{{route('main')}}"><img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}"
                    alt="Logo Jendela Langit" style="width: 100px;"></a>
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-{{ Route::currentRouteNamed('main') ? 'active' : '' }}"><a
                        href="{{route('main')}}">Beranda</a></li>
                <li
                    class="menu-has-children menu-{{ Route::currentRouteNamed('tentangkami','detail_tentang') ? 'active' : '' }}">
                    <a href="{{route('tentangkami')}}">Tentang Kami</a>
                    @if (!empty($abouts))
                    <ul>
                        @foreach($abouts as $about)
                        <li><a href="{{route('detail_tentang',$about->
                        slug)}}">{{ $about->jenis_tentang }}</a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </li>
                <li
                    class="menu-has-children menu-{{ Route::currentRouteNamed('berita','kategori_berita') ? 'active' : '' }}">
                    <a href="{{route('berita')}}">Berita</a>
                    <ul>
                        @foreach($katbers as $katber)
                        <li><a href="{{route('kategori_berita',$katber->katber_slug)}}">{{ $katber->katber_name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                <li class="menu-{{ Route::currentRouteNamed('album') ? 'active' : '' }}"><a
                        href="{{route('album')}}">Album</a></li>
                <li class="menu-{{ Route::currentRouteNamed('faq') ? 'active' : '' }}"><a
                        href="{{route('faq')}}">F.A.Q</a></li>
                <li class="menu-{{ Route::currentRouteNamed('kontak') ? 'active' : '' }}"><a
                        href="{{route('kontak')}}">Kontak</a></li>
            </ul>
        </nav>
    </div>
</header>
<!-- End Header -->
