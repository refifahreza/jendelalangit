<!-- ======= Header ======= -->
<header id="header">
  <div class="container-fluid">

    <div id="logo" class="pull-left">
      <a href="{{route('main')}}"><img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}" alt="Logo Jendela Langit" style="width: 100px;"></a>
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu">
        <li><a href="{{route('main')}}">Beranda</a></li>
        <li><a href="{{route('tentangkami')}}">Tentang Kami</a></li>
        <li><a href="{{route('berita')}}">Berita</a></li>
        <li><a href="{{route('galeri')}}">Galeri</a></li>
        <li><a href="{{route('faq')}}">F.A.Q</a></li>
      </ul>
    </nav>

  </div>
</header>
  <!-- End Header -->