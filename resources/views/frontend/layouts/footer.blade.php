<!-- ======= Footer ======= -->

<footer id="footer" style="margin: 75px 0px 0px 0px !important;">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Agenda</h4>
                    <div class="list-group">
                        <!-- ITEM -->
                        @forelse ($agendas as $index => $agenda)
                        <a class="list-group-item list-group-item-action rounded-0 mt-1" data-toggle="collapse" href="#item{{$index}}" role="button" aria-expanded="false" aria-controls="item{{$index}}" style="padding: 0 !important;">
                            <div class="media">
                                <div class="cal mr-1" style="height: 100% !important;">
                                    <div class="cal-date">{{ $agenda->tanggal_tunggal }}</div>
                                    <div class="cal-month">{{ $agenda->bulan_tunggal }}</div>
                                </div>
                                <div class="media-body m-1">
                                    <h6 class="cal-title" style="border-bottom: 1px solid rgb(158, 158, 158); padding-bottom: 0.4rem;">
                                        {{ $agenda->judul }}
                                    </h6>
                                    <div class="cal-intro">
                                        <i class="fas fa-clock"></i>&nbsp;: {{ $agenda->jam_satu }} -
                                        {{ $agenda->jam_dua }}
                                        <br>
                                        <i class="fas fa-map-marker-alt"></i>&nbsp;: {{ $agenda->lokasi }}<br>
                                        <i class="fas fa-chalkboard-teacher"></i>&nbsp;: {{ $agenda->pengajar }}</i>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="collapse" id="item{{$index}}">
                            <a class="list-group-item list-group-item-action cal-content rounded-0" style="padding: 0.75rem 0.25rem; color: #495057;
    background-color: #f4f4f4;">

                                <h6><b>Deskripsi Acara</b></h6>
                                <p>{!! $agenda->LimitStrDua !!}</p>
                                <hr>
                                <h6><b>Waktu</b></h6>
                                ({{ $agenda->HariTunggalAwal }}) {{ $agenda->jam_satu }} - {{ $agenda->jam_dua }}
                                <hr>
                                <h6><b>Lokasi</b></h6>
                                {{ $agenda->lokasi }}
                                <hr>
                                <h6><b>Pengajar / Pengisi</b></h6>
                                {{ $agenda->pengajar }}
                                <hr>
                                <h6><b>Penyelenggara</b></h6>
                                {{ $agenda->penyelenggara }}

                                <a href="https://www.google.com/calendar/render?action=TEMPLATE&text={{str_replace(' ','+',$agenda->judul)}}&dates={{$agenda->CalendarGoogle}}/{{$agenda->CalendarGoogleAkhir}}&details={{str_replace(' ','+',strip_tags($agenda->isi_agenda)) }}&location={{strip_tags($agenda->lokasi)}}&sf=true&output=xml" class="btn btn-sm btn-block" target="_blank" style="color: #fff;
                                    background-color: #ffac32;
                                    border-color: #ffac32;">Google Calender</a>
                            </a>
                        </div>

                        <!-- END ITEM -->
                        @empty
                        <h6>Agenda Tidak Ada</h6>
                        @endforelse
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Berita Terpopuler</h4>
                    <ul>
                        @foreach ($postTerpopuler as $berita)
                        <li><i class="fas fa-chevron-right"></i>
                            <a href="{{ route('detail_berita', $berita->title_slug) }}">{{ $berita->title }}</a>
                            <p>
                                {{ \Carbon\Carbon::parse($berita->created_at)->format('d, M Y H:i') }}
                            </p>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Kontak Kami</h4>
                    <p>
                        <!-- Alamat -->
                        <i class="fas fa-map-marker-alt"></i> :<a class="venobox" data-vbtype="iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.680752836228!2d117.1542475!3d-0.4937272!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x48e13a4d1a1a6786!2sYayasan%20JENDELA%20LANGIT!5e0!3m2!1sid!2sid!4v1593975140926!5m2!1sid!2sid">
                            {{ config('web_config')['ADDRESS'] }}</a><br>

                        <!-- Whatsapp -->
                        <i class="fab fa-whatsapp"></i> :<a href="https://api.whatsapp.com/send?phone=62{{ config('web_config')['WHATSAPP']}}?text=Assalamualaikum,%20permisi" target="popup" onclick="window.open('https://api.whatsapp.com/send?phone=62{{ config('web_config')['WHATSAPP'] }}&text=Assalamualaikum,%20permisi','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                            +62{{ config('web_config')['WHATSAPP'] }}</a><br>

                        <!-- Telepon -->
                        <i class="fas fa-phone"></i> :<a href="tel:0{{ config('web_config')['TELEPON'] }}">
                            +62{{ config('web_config')['TELEPON'] }}</a><br>

                        <!-- Email -->
                        <i class="fas fa-envelope"></i> :<a href="https://mail.google.com/mail/?view=cm&fs=1&to={{ config('web_config')['EMAIL'] }}" target="popup" onclick="window.open('https://mail.google.com/mail/?view=cm&fs=1&to={{ config('web_config')['EMAIL'] }}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                            {{ config('web_config')['EMAIL'] }}</a><br>
                    </p>


                    <div class="social-links" style="margin-top: 20px;">
                        <a href="{{ config('web_config')['INSTAGRAM'] }}" target="_blank" class="instagram"><i class="fab fa-instagram"></i></a>
                        <a href="{{ config('web_config')['FACEBOOK'] }}" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="{{ config('web_config')['YOUTUBE'] }}" target="_blank" class="youtube"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
                {{-- <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Ikuti Kami di Instagram</h4>
                    <blockquote class="instagram-media"
                        data-instgrm-permalink="https://www.instagram.com/p/COpJQ_2J8lG/?utm_source=ig_embed&amp;utm_campaign=loading"
                        data-instgrm-version="13"
                        style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; width: 100%;">
                    </blockquote>
                    <script async src="//www.instagram.com/embed.js"></script> --}}
                {{-- <iframe src="//instagram.com/p/COpJQ_2J8lG/embed/" class="instagram-frame" frameborder="0"
                        scrolling="yes" allowtransparency="true"></iframe> --}}
                {{-- </div> --}}
                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Alamat Kami</h4>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.663496769088!2d117.161148!3d-0.500865!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa9d3fd377172d3e3!2sPuskesmas%20Sidomulyo!5e0!3m2!1sid!2sid!4v1674565147363!5m2!1sid!2sid" width="280" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            <span>&copy; Copyright {{date('Y')}} <a href="https://refifahreza.com">Refi Fahreza</a> - All
                rights
                reserved</span>
        </div>
    </div>
</footer>

<!-- End Footer -->
