<div class="modal fade modal-konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="text-align: center; display: block !important;">
        <h5 class="modal-title mt-0" id="myLargeModalLabel">Konfirmasi</h5>
      </div>
      <div class="modal-body">
        <form action="{{ route('cek-donasi') }}" method="get">
          <div class="col-md-12">
            <p class="mb-3">Masukan Nomor Referensi :</p>
            <div class="input-group mb-3">
              <input type="number" class="form-control" required name="nomor" aria-label="Default" autocomplete="off" aria-describedby="inputGroup-sizing-default" placeholder="Nomor Referensi">
            </div>
            <button class="btn-lg btn-primary float-right" style="height: 100%; font-size: 16px;">Cek Referensi</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>





