@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container">
    @if($donasis == null)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      Maaf, Kode Referensi yang anda cari tidak ada, silahkan Cek kembali Nomor Refrensi anda !
    </div>
    @else
    <header class="section-header judul">
      <h3 class="section-title">Terima Kasih atas Donasi Anda</h3>
    </header>

    <div class="card card-donasi mt-4">
      <div class="row">
        <div class="col-md-6">
          <div class="card-body form-donasi">
            <h4><strong>Resume Donasi : {{ $donasis->nama_donatur }}</strong></h4>
            <hr>
            <ul>
              <li>Kode Konfirmasi : {{ $donasis->kode_donasi }}
                <li>Tanggal Transaksi : {{ \Carbon\Carbon::parse($donasis->created_at)->format('d, M Y H:i') }}</li>
                <li>Jenis Donasi : {{ $donasis->jenis->jendon_name }}</li>
                <li>Jumlah Donasi : Rp. {{ number_format($donasis->jumlah,0,',','.') }}</li>
                <li>Pesan : {{ $donasis->pesan }}</li>
              </ul>

              <hr style="margin: 20px 0;">
              <p class="mb-3">- Keterangan : <br>{{ config('web_config')['KETERANGAN'] }}</p>
              <!--<a href="{{ route('notifemail') }}" target="_blank" class="btn btn-primary" style="color: white;">Kirim ke email</a>-->
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-body">
              <h4><strong>Silahkan Transfer Ke : </strong> </h4>
              <hr>
              <div class="card mb-2">
                <!-- Gambar -->
                <div class="card-header"><img class="icon mr-3" src="{{ Storage::url('images/rekening/'.$donasis->rekening->logo) }}" width="80">{{ $donasis->rekening->rekening_name }}</div>

                <div class="card-body" style="line-height: 40px; font-size: 18px;">

                  <div class="row">
                    <div class="rekening col-12 col-md-8"   style="margin: 0;padding-right: 0;">
                      <!-- Rekening -->
                      <p id="nomorrekening" hidden>{{ $donasis->rekening->rekening_number }}</p>
                      <p> No. Rekening : <b>{{ $donasis->rekening->rekening_number }}</b>&nbsp;|&nbsp;<a class="salin" id="salinrekening">SALIN</a></p>
                    </div>
                    <div class="col-12 col-md-4">
                     <div id="alertsalinrekening" class="alert-transaksi"></div>
                   </div>
                 </div>
                 <!-- Atas Nama -->
                 <p>Atas Nama : <b>{{ $donasis->rekening->rekening_author }}</b></p>
               </div>

             </div>
             <p style="font-size: 17px; text-align: center; margin-top: 20px;">{{ config('web_config')['KUTIPAN'] }}</p>

             <center>
              @if($ceks == true)
              <button class="btn btn-donasi mt-4" data-toggle="modal" data-target=".modal-konfir-donasi" style="border: none;">Konfirmasi Donasi</button>
              @else
              <div class="alert alert-success">Maaf kamu sudah pernah konfirmasi donasi dengan Kode Konfirmasi ini.</div>
              @endif
            </center>
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
</section>
<!-- End Portfolio Section -->
<div class="modal fade modal-konfir-donasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="text-align: center; display: block !important;">
        <h5 class="modal-title mt-0" id="myLargeModalLabel">Konfirmasi Donasi</h5>
      </div>
      <div class="modal-body">
        <label class="mb-2">Data Bank Donatur :</label>
        <form action="{{ route('konfirmasi-donasi', $donasis->kode_donasi) }}" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
          {{method_field('PUT')}}
          <div class="form-row">
            <div class="form-group col-md-6" style="width: 100%;">
              <select class="form-control select2" required name="banked" style="width: 100%">
                <option value="">Pilih Bank</option>
                <option value="Bank BNI">Bank BNI</option>
                <option value="Bank BCA">Bank BCA</option>
                <option value="Bank BRI">Bank BRI</option>
                <option value="Bank BRI">Bank BRI</option>
                <option value="Bank Mandiri">Bank Mandiri</option>
                <option value="Bank CIMB Niaga">Bank CIMB Niaga</option>
                <option value="Bank Mandiri Syariah">Bank Mandiri Syariah</option>
                <option value="Bank BNI Syariah">Bank BNI Syariah</option>
                <option value="Bank BRI Syariah">Bank BRI Syariah</option>
                <option value="Bank CIMB Niaga">Bank CIMB Niaga</option>
                <option value="Bank CIMB Niaga">Bank CIMB Niaga Syariah</option>
              </select>
            </div>
            <div class="form-group col-md-6">
              <input type="text" minlength="6" maxlength="16" class="form-control" autocomplete="off" required name="rekening" id="inputPassword4" placeholder="No. Rekening">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input type="text" class="form-control" autocomplete="off" required name="author" placeholder="Nama Pemilik">
            </div>
            <div class="form-group col-md-6">
              <input type="text" required name="cabang" autocomplete="off" required class="form-control" placeholder="Cabang Bank">

            </div>
          </div>

          <hr>
          <label class="mb-2">Doa / Pesan :</label>
          <textarea type="text" required autocomplete="off" class="form-control textarea mb-3" name="notes" id="txtPassportNumber" maxlength="150" rows="2" style=" resize: none; width: 100%;min-height: 70px;height: 100%; font-family: inherit;padding: 0.75em 0.625em;border-width: 1px;border-style: solid;border-color: rgb(204, 208, 211);border-image: initial;background: rgb(255, 255, 255);border-radius: 5px;">-</textarea>
          <label class="mb-2">Masukan Bukti Pembayaran :</label>
          <div class="input-group mb-2">
            <input type="file" name="image" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
          </div>
          <button type="submit" class="btn btn-lg btn-primary float-right" style="height: 100%; font-size: 16px; border: none;">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript"> 

  document.getElementById("salinrekening").addEventListener("click", copy_password1);

  function copy_password1() {
    var copyText = document.getElementById("nomorrekening");
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    $('#alertsalinrekening')
    .css({
      "background-color": "#ffac32",
      "color": "white",
      "visibility": "visible",
    });
    document.getElementById("alertsalinrekening").innerHTML = "&nbsp; Berhasil Disalin";

    setTimeout(function() {
      $('#alertsalinrekening')
      .css({
        "background-color": "transparent",
        "visibility": "hidden",
      });
    }, 3000);

    const sleep = (milliseconds) => {
      return new Promise(resolve => setTimeout(resolve, milliseconds))
    }     

    sleep(3000).then(() => {
      $('#alertsalinrekening') .css({
        "visibility": "visible",
      });
      document.getElementById("alertsalinrekening").innerHTML = "";
    })
  }
 //alert("Nominal Di copy" );
</script>
<script>
  $("input[name='rekening']").on('input', function() {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
  });
</script>
@endpush
