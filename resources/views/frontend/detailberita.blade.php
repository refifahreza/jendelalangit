@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
    <div class="container">

        <div class="col-12">
            <header class="section-header judul">
                <div class="mb-2">
                    <span class="section-category">{{ $beritas->kategori->katber_name }}</span>
                </div>
                <div style="text-align: left !important;">
                    <span class="section-title-detail" style="text-align: left !important;">
                        {{ strtoupper($beritas->title) }}
                    </span>
                </div>
                <div class="mt-2">
                    <span>
                        <i class="fas fa-calendar-alt"></i>
                        <span class="ml-2">{{ $beritas->CreatedNew }}</span>
                    </span>
                    <span class="mr-2 ml-2">-</span>
                    <span>
                        <i class="fas fa-user-alt"></i>
                        <span class="ml-2">{{ $beritas->author->name }}</span>
                    </span>
                    <span class="mr-2 ml-2">-</span>
                    <span>
                        <i class="fas fa-book-open"></i>
                        <span class="ml-2">{{ $beritas->dibaca }}</span>
                    </span>
                </div>
            </header>
        </div>

        <div class="content-wrapper" style="margin-top: 30px; padding: 0px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div align="center" style="position: relative;">
                            <img src="{{ Storage::url('images/berita/'.$beritas->thumnail) }}"
                                alt="{{ strtoupper($beritas->title) }}" width="100%">
                            <div style="background-color: #000000ad;
                            padding: 15px;
                            color: #e4e4e4; position: absolute; width: 100%;
                            bottom: 0;
                            left: 0;
                            text-align: left !important;
                            ">
                                <p>{{ $beritas->deskripsi_gambar }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-11 paragraf">
                        <p>
                            {!! $beritas->content !!}
                        </p>
                        <div id="disqus_thread" class="mt-4"></div>
                    </div>
                    <div class="col-12 col-md-1">
                        <div id="share">
                            <a class="facebook"
                                href="http://www.facebook.com/share.php?u={{route('detail_berita', $beritas->title_slug)}}"
                                target="popup"
                                onclick="window.open('http://www.facebook.com/share.php?u={{route('detail_berita', $beritas->title_slug)}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
                                target="blank"><i class="fab fa-facebook"></i>
                            </a>
                            <a class="wa" href="whatsapp://send?text={{route('detail_berita', $beritas->title_slug)}}"
                                target="blank"><i class="fab fa-whatsapp"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Portfolio Section -->

@endsection
@push('scripts')
<script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://jendelalangit.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
</script>
@endpush
