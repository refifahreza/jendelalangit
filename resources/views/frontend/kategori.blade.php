@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
    <header class="section-header judul">
        <h3 class="section-title">Kategori : {{ $katbers->katber_name }}</h3>
    </header>

    <div class="content-wrapper" style="margin-bottom: 30px;">
        @foreach($beritad as $berita)
        <div class="col-md-6 col-sm-6">
            <div class="news-card">
                <a href="#" class="news-card__card-link"></a>
                <img src="{{ Storage::url('images/berita/'.$berita->thumnail) }}" alt="" class="news-card__image">
                <div class="news-card__text-wrapper">
                    <h2 class="news-card__title">{{ $berita->title }}</h2>
                    <div class="news-card__post-date">
                        {{ \Carbon\Carbon::parse($berita->created_at)->format('d, M Y H:i') }}</div>
                    <div class="news-card__details-wrapper">
                        <p class="news-card__excerpt">{!! substr($berita->content,0,100) . '....' !!}</p>
                        <a href="{{ route('detail_berita', $berita->title_slug) }}" class="news-card__read-more">Baca
                            Selengkapnya <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!-- End Portfolio Section -->

@endsection
