@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container">

    <header class="section-header judul">
      <h3 class="section-title">Tentang Kami</h3>
    </header>

    <div class="content-wrapper mt-4">
      <div class="row">
        @foreach($katbouts as $katbout)
        <div class="col-md-6 col-sm-12">
          <div class="card2">
            <div class="img-hover-zoom">
              <img src="{{ Storage::url('images/katbout/'.$katbout->thumbnail) }}" class="img-fluid" alt="{{ strtoupper($katbout->katbout_name) }}">
            </div>
            <div class="card2-judul">
              <a href="{{route('detail_tentang', $katbout->katbout_slug)}}">
                <h2 style="font-size: 20px;padding: 6px;">{{ strtoupper($katbout->katbout_name) }}</h2>
              </a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- End Portfolio Section -->

  @endsection
