@extends('frontend.layouts.app2')
@section('content')

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="section-bg">
  <div class="container" align="center">
    <header class="section-header judul">
      <h3 class="section-title">F.A.Q</h3>
    </header>
    <div class="col-lg-8" style="margin-top: 30px;">
      <div id="accordion-three" class="accordion" style="cursor: pointer; margin: 1em 0;">
        {{-- Start --}}
        @foreach($faqs as $key => $faq)
        <div class="card" style="background-color: #fbfbfb;">
          <div class="card-header">
            <h6 class="faq mb-0 collapsed" data-toggle="collapse" data-target="#collapseOne{{ $loop->iteration }}" aria-expanded="false" aria-controls="collapseOne4"  style="">{{ $faq->pertanyaan }}</h6>
          </div>
          <div id="collapseOne{{ $loop->iteration }}" class="collapse" data-parent="#accordion-three">
            <div class="card-body">{{ $faq->jawaban }}</div>
          </div>
        </div>
        @endforeach
        {{-- End --}}
      </div>
    </div>
  </div>
</section>
<!-- End Portfolio Section -->

@endsection
