@extends('layouts.app')

@section('css')
        <!-- Plugins css -->
        <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet">
@endsection
@section('title', 'Kategori Produk')

@section('content')
            <div class="container-fluid">
                <div class='notifications top-right'></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Kategori Produk</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">Kategori Produk</a></li>
                                    </ol>


                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card m-b-20">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Seluruh Data Kategori Produk</h4>

                                        <div class="row mb-3">
                                            <div class="col-md-5">
                                                <form action="" class="form-inline">
                                                    <input type="text" class="form-control mr-2" placeholder="Cari Kategori" autocomplete="off" name='search'>
                                                    <button type="submit" class='btn btn-primary'><i class="fas fa-search"></i> Cari</button>
                                                    <div class="mr-2"></div>
                                                    <a class="btn btn-danger float-right" href="{{ route('admin.kategori_produk.index') }}"><i class="fas fa-bullseye"></i> Refresh</a>
                                                </form>
                                            </div>
                                            <div class="col-md-2 ml-auto">
                                                <a class="btn btn-primary float-right" href="{{ route('admin.kategori_produk.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                                            </div>
                                        </div>
                                        {{--
                                        @if(session('success'))
                                            <div class="alert alert-info">{{ session('success') }}</div>
                                        @endif
                                        @if(session('warning'))
                                        <div class="alert alert-warning">{{ session('warning') }}</div>
                                        @endif --}}
                                        <div class="table-responsive">
                                            <table class="table table-striped mb-0">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama Kategori</th>
                                                    <th>Slug Kategori</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($katpros as $katpro)
                                                        <tr>
                                                            <th scope="row">{{ $loop->iteration }}</th>
                                                            <td>{{ $katpro->katpro_name }}</td>
                                                            <td>{{ $katpro->katpro_slug }}</td>
                                                            <td>
                                                                <div class='d-inline-flex'>
                                                                    <a href="{{ route('admin.kategori_produk.edit', $katpro->id) }}" class='btn btn-warning mr-2'><i class="fas fa-edit"></i></a>
                                                                    <form action="{{ route('admin.kategori_produk.destroy', $katpro->id) }}" method="post">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i></button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="paginate float-right mt-3">
                                            {{$katpros->links()}}
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- end col -->

                        </div> <!-- end row -->

                    </div> <!-- container-fluid -->
@endsection

@section('script')
        <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
@endsection
