<div class="modal fade modal-edit" id="modal-edit-{{$katvid->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Kategori Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                 <form action="{{ route('admin.kategori_video.update', $katvid->id) }}" method="POST" class='mt-3' >
                    @csrf
                    @method('put')
                    <div class="form-group row">
                        <label for="" class='col-md-2 col-form-label'>Nama Kategori</label>
                        <div class="col-md-10">
                            <input type="text" value="{{ $katvid->kategori_video }}" name="name" class='form-control @error('name') is-invalid @enderror'>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class='btn-lg btn-primary waves-effect waves-light float-right'>Ubah</button>
                    <a href="" class="btn-lg btn-outline-secondary float-right mr-2" data-dismiss="modal" aria-hidden="true">Kembali</a>
                </form>
        </div>
    </div>
</div>
</div>


