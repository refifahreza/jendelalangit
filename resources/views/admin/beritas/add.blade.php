@extends('layouts.app')

@section('title', 'Tambah Berita')

@section('content')
<div class="container-fluid">

    {{-- Breadcrumb Section --}}
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Berita</h4>
            </div>
        </div>
    </div>
    {{-- End Section --}}

    {{-- Content Section --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.berita.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div>
                                        <label for="judul" class="col-form-label">Judul Berita</label>
                                    </div>
                                    <div>
                                        <input type="text" inputmode="text" name="judul" class='form-control @error('
                                            judul') is-invalid @enderror' required>
                                        @error('judul')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Judul</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kutipan" class="col-form-label">Kutipan</label>
                                    <div>
                                        <textarea name="kutipan" id="BioChars" class='form-control @error(' kutipan')
                                            is-invalid @enderror' required maxlength="200"></textarea>
                                        <small>Batas Karakter : <span id="countBio">0</span>/200</small>
                                        @error('kutipan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Kutipan</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="konten" class="col-form-label">Konten</label>
                                    <div>
                                        <textarea name="content" style="resize: none;" id="" rows="3"
                                            class="form-control my-editor"></textarea>
                                        @error('content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Konten</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label for="seo_judul" class="col-form-label">SEO Judul</label>
                                    </div>
                                    <div>
                                        <input type="text" inputmode="tel" name="seo_judul" class='form-control @error('
                                            seo_judul') is-invalid @enderror' required>
                                        @error('seo_judul')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Judul</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kategori" class="col-form-label">Kategori</label>
                                    <div>
                                        <select class="form-control @error('kategori') is-invalid @enderror select2"
                                            name="kategori" id="kat" required>
                                            <option value="" selected disabled>-- Pilih Kategori --</option>
                                            @foreach ($katber as $kat)
                                            <option value="{{ $kat->id }}">{{ $kat->katber_name }}</option>
                                            @endforeach
                                        </select>
                                        @error('kategori')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Kategori</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="tag_id" class=" form-control-label">Tag</label>
                                    </div>
                                    <div class="tags_add">
                                        <select class="select2 form-control" name="tag[]" multiple="multiple">
                                            @foreach($tags as $tag)
                                            <option value="{{$tag->id}}">{{$tag->nama_tag}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="deskripsi_meta" class="col-form-label">Meta Deskripsi</label>
                                    </div>
                                    <div>
                                        <div>
                                            <textarea name="deskripsi_meta" onkeyup="countChar(this)" cols="30" rows="5"
                                                style="resize: none;" class='form-control @error(' deskripsi_meta')
                                                is-invalid @enderror' required></textarea>
                                            @error('deskripsi_meta')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Mohon Isi Kutipan</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="thumnail" class="col-form-label">Thumbnail</label>
                                    <div>
                                        <input type="file" class='filestyle @error(' thumnail') is-invalid @enderror'
                                            id="logo" data-buttonname="btn-secondary" name="thumnail" id="logo"
                                            required>
                                        <small class="text-danger"><i>max 8MB</i></small>
                                        @error('thumnail')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Mohon Isi Thumbnail /</strong>
                                            <strong>Ukuran Thumbnail Lebih Dari 8MB</strong>
                                        </span>
                                        @enderror
                                        <div class="preview"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="deskripsi_gambar" class="col-form-label">Deskripsi Gambar</label>
                                    </div>
                                    <div>
                                        <div>
                                            <textarea name="deskripsi_gambar" onkeyup="countChar(this)" cols="30"
                                                rows="5" style="resize: none;" class='form-control @error('
                                                deskripsi_gambar') is-invalid @enderror' required></textarea>
                                            @error('deskripsi_gambar')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Mohon Isi Kutipan</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.berita.index') }}"
                            class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Section --}}

</div> <!-- container-fluid -->
@endsection
@push('scripts')
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->
{{-- <script>
    $(document).ready(function() {
        var editor_config = {
            selector: "textarea.my-editor",
            path_absolute : "/",
            height: 600,
            min_height: 600,
            plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table directionality emoticons paste",
            "save code fullscreen autoresize codesample autosave responsivefilemanager"
            ],
            menubar : false,
            toolbar1: "undo redo restoredraft | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist hr | outdent indent table searchreplace",
            toolbar2: "| fontsizeselect | styleselect | link unlink anchor | image media emoticons | forecolor backcolor | code codesample fullscreen ",
            contextmenu: "link paste image imagetools table spellchecker",
            image_advtab: true,
            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
            relative_urls: false,
            remove_script_host: false,
            external_filemanager_path:"{{ url('template/filemanager') }}/",
filemanager_title :"Responsive File Manager" , // bisa diganti terserah anda
external_plugins : {
"filemanager" : "{{ asset('template/filemanager/plugin.min.js') }}"
},
}
tinymce.init(editor_config);
});
</script> --}}
<script>
    $(document).ready(function() {
    var editor_config = {
        selector: "textarea.my-editor",
        path_absolute : "/",
        height: 600,
        min_height: 600,
        plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
        "table directionality emoticons paste",
        "save code fullscreen autoresize codesample autosave responsivefilemanager"
        ],
        menubar : false,
        toolbar1: "undo redo restoredraft | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist hr | outdent indent table searchreplace",
        toolbar2: "| fontsizeselect | styleselect | link unlink anchor | image media emoticons | forecolor backcolor | code codesample fullscreen ",
        contextmenu: "link paste image imagetools table spellchecker",
        image_advtab: true,
        fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
        relative_urls: false,
        remove_script_host: false,
        file_picker_types: 'image',
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        },
    }
    tinymce.init(editor_config);
});
</script>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function() {
    $('.select2').select2();
});

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});

// function countChar(val) {
//     var len = val.value.length;
//     if (len >= 200) {
//       val.value = val.value.substring(0, 200);
//   } else {
//       $('#charNum').text(199 - len);
//   }
// };

// COUNT CHARACTER BIOGRAFI
$(document).ready(function () {
    var length = $("#BioChars").val().length;
    $("#countBio").text(length);

    $("#BioChars").keyup(function () {
        var length = $(this).val().length;
        $("#countBio").text(length);
    });
    $("#BioChars").keydown(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        } else if (e.keyCode === 32 && this.value.slice(-1) === " ") {
            e.preventDefault();
        }
    });
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
