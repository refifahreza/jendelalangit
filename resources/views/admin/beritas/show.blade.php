@extends('layouts.app')

@section('title', 'Detail Berita')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Detail Berita</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    @csrf
                    @method('put')
                    <div class="form-group row">
                        <label for="judul" class='col-md-2 col-form-label'>Judul</label>
                        <div class="col-md-10">
                            <input type="text" name="title" class='form-control @error('title') is-invalid @enderror' value="{{ $beritas->title }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kutipan" class="col-md-2 col-form-label">Kutipan</label>
                        <div class="col-md-10">
                            <textarea name="kutipan" onkeyup="countChar(this)" class='form-control @error('kutipan') is-invalid @enderror' readonly>{{ $beritas->kutipan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kategori" class='col-md-2 col-form-label'>Kategori</label>
                        <div class="col-md-10">
                            <input type="text" name="stitle" class='form-control @error('stitle') is-invalid @enderror' value="{{ $beritas->kategori->katber_name }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="penulis" class='col-md-2 col-form-label'>Penulis</label>
                        <div class="col-md-10">
                            <input type="text" name="stitle" class='form-control @error('stitle') is-invalid @enderror' value="{{ $beritas->author->name }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal" class='col-md-2 col-form-label'>Tanggal</label>
                        <div class="col-md-10">
                            <input type="text" name="stitle" class='form-control @error('stitle') is-invalid @enderror' value="{{ \Carbon\Carbon::parse($beritas->created_at)->format('d, M Y H:i') }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="thumnail" class='col-md-2 col-form-label'>Thumbnail</label>
                        <div class="col-md-10">
                            <img src="{{ Storage::url('images/berita/'.$beritas->thumnail) }}" alt="" srcset="" style="width:200px">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="konten" class='col-md-2 col-form-label'>Kontent</label>
                        <div class="col-md-10">
                            {!! $beritas->content !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <label for="tag_id" class=" form-control-label">Tag</label>
                        </div>
                        <div class="tags_add" >
                            <select class="select2 form-control" disabled name="tag[]" multiple="multiple">
                                @foreach($beritas->tag as $t)
                                <option value="{{$t->id}}">{{$t->nama_tag}}</option>
                                @endforeach 
                            </select>
                        </div>
                    </div>
                    <a href="{{ route('admin.berita.index') }}" class='btn-lg btn-outline-secondary float-right'>Kembali</a>
                </form>

            </div> 
        </div>

    </div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#kat').select2();
        CKEDITOR.replace('content', {
                    // filebrowserUploadUrl: "{{ route('admin.berita.upload', ['_token' => csrf_token() ]) }}",
                    // filebrowserUploadMethod: 'form'
                });
    });
    
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection
