@extends('layouts.app')

@section('title', 'Edit Album')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Album</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.album.update', $albums->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Nama Album</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $albums->album_name }}" required>
                                 @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Album</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Thumbnail Sekarang</label>
                            <div class="col-md-10">
                                <img src="{{ Storage::url('images/album/'.$albums->thumbnail) }}" alt="" srcset="" style="width:310px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-2 col-form-label">Thumbnail</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('image') is-invalid @enderror' id="logo" data-buttonname="btn-secondary" name="image" id="logo">
                                <small class="text-danger"><i>max 8MB</i></small>
                               @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Thumbnail /</strong>
                                    <strong>Ukuran Thumbnail Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div> 
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.album.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
