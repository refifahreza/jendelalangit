@extends('layouts.app')

@section('title', 'Edit Agenda')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Agenda</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.agenda.update', $agendas->id) }}" method="POST">
                        @csrf
                        @method('put')@method('put')
                        <div class="form-group row">
                            <label for="judul" class='col-md-2 col-form-label'>Nama Agenda</label>
                            <div class="col-md-10">
                                <input type="text" name="judul" id="judul" class='form-control @error(' judul')
                                    is-invalid @enderror' value="{{$agendas->judul}}" required>

                                @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Agenda</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isi_agenda" class="col-md-2 col-form-label">Isi Agenda</label>
                            <div class="col-md-10">
                                <textarea name="isi_agenda" id="isi_agenda" style="resize: none;" id="" rows="2"
                                    class="form-control my-editor">{{$agendas->isi_agenda}}</textarea>

                                @error('isi_agenda')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Konten</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isi_agenda" class="col-md-2 form-control-label">Waktu</label>
                            <div class="col-md-10 input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text fa fa-clock"></span>
                                </div>

                                <input type="text" name="waktu_awal"
                                    class="form-control  {{$errors->has('waktu_awal') ? 'form-control is-invalid' : 'form-control'}} date"
                                    id="datetimepicker3" placeholder="{{$agendas->waktu_awal}}" aria-label="Waktu Awal"
                                    aria-describedby="basic-addon1" value="{{$agendas->waktu_awal}}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text ">-</span>
                                </div>
                                <input type="text" name="waktu_akhir"
                                    class="form-control  {{$errors->has('waktu_akhir') ? 'form-control is-invalid' : 'form-control'}} date"
                                    id="datetimepicker1" placeholder="{{$agendas->waktu_akhir}}"
                                    aria-label="Waktu Akhir" aria-describedby="basic-addon1"
                                    value="{{$agendas->waktu_akhir}}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text fa fa-clock"></span>
                                </div>

                            </div>
                            @error('waktu_akhir')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Waktu</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="lokasi" class='col-md-2 col-form-label'>Lokasi</label>
                            <div class="col-md-10">
                                <input type="text" name="lokasi" id="lokasi" class='form-control @error(' lokasi')
                                    is-invalid @enderror' value="{{$agendas->lokasi}}" required>

                                @error('lokasi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Lokasi</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pengajar" class='col-md-2 col-form-label'>Pengajar</label>
                            <div class="col-md-10">
                                <input type="text" name="pengajar" id="pengajar" class='form-control @error(' pengajar')
                                    is-invalid @enderror' value="{{$agendas->pengajar}}" required>

                                @error('pengajar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Pengajar</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                </div>
                <div class="form-group row">
                    <label for="penyelenggara" class='col-md-2 col-form-label'>Penyelanggara</label>
                    <div class="col-md-10">
                        <input type="text" name="penyelenggara" id="penyelenggara" class='form-control @error('
                            penyelenggara') is-invalid @enderror' value="{{$agendas->penyelenggara}}" required>

                        @error('penyelenggara')
                        <span class="invalid-feedback" role="alert">
                            <strong>Mohon Isi Penyelenggara</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class='btn-lg btn-primary float-right'>Ubah</button>
                <a href="{{ route('admin.agenda.index') }}"
                    class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('style')
<link href="{{asset('template/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{asset('template/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script>
    $(document).ready(function() {
        var editor_config = {
            selector: "textarea.my-editor",
            path_absolute : "/",
            height: 600,
            min_height: 600,
            plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table directionality emoticons paste",
            "save code fullscreen autoresize codesample autosave responsivefilemanager"
            ],
            menubar : false,
            toolbar1: "undo redo restoredraft | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist hr | outdent indent table searchreplace",
            toolbar2: "| fontsizeselect | styleselect | link unlink anchor | image media emoticons | forecolor backcolor | code codesample fullscreen ",
            contextmenu: "link paste image imagetools table spellchecker",
            image_advtab: true,
            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
            relative_urls: false,
            remove_script_host: false,
            file_picker_types: 'image',
            file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            },
        }
        tinymce.init(editor_config);
    });

    $(function () {
    // $('#datetimepicker1').datetimepicker({
    // format: 'LT'

    // });
    // $('#datetimepicker3').datetimepicker({
    // format: 'LT'
    // });
    $('#datetimepicker1').datetimepicker({
    sideBySide: true
    });
    $('#datetimepicker3').datetimepicker({
    sideBySide: true
    });
    });
</script>
@endpush
