@extends('layouts.app')
@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Pengaturan Zakat')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Pengaturan Zakat</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.settingzakat.update') }}" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="HARGA_EMAS" class='col-md-2 col-form-label'>Harga Emas / gr</label>
                            <div class="col-md-10">
                                <input type="text" name="HARGA_EMAS" class='form-control  @error('HARGA_EMAS') is-invalid @enderror' value="{{ config('zakat_setting')['HARGA_EMAS'] }}">
                                @error('HARGA_EMAS')
                                <span class="invalid-feedback" role="alert">
                                     <strong>Mohon Isi Harga Emas Sekarang</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
   
    function preview_image(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }

  function preview_image1(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image2');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }

  function preview_image2(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image3');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush

