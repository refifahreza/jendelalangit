@extends('layouts.app')
@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Profil')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Akun</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.profil.update') }}" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" class='form-control  @error('id') is-invalid @enderror' value="{{ $akun->id }}">
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Akun</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control  @error('name') is-invalid @enderror' value="{{ $akun->name }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Akun</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class='col-md-2 col-form-label'>Username</label>
                            <div class="col-md-10">
                                <input type="text" name="username" class='form-control  @error('username') is-invalid @enderror' value="{{ $akun->username }}">
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Username</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class='col-md-2 col-form-label'>Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" class='form-control  @error('email') is-invalid @enderror' value="{{ $akun->email }}">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Email</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class='col-md-2 col-form-label'>Password</label>
                            <div class="col-md-10">
                                <input type="password" name="password" class='form-control  @error('password') is-invalid @enderror'>
                                <small style="color: red;">*Kosongkan saja jika tidak ingin mengubah password (Hash now : {{ $akun->password }})"</small>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Password</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="icon" class="col-md-2 col-form-label">Icon</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('icon') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="icon" id="logo">
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('icon')
                                <span class="invalid-feedback" role="alert">
                                   <strong>Mohon Isi Icon /</strong>
                                   <strong>Ukuran Icon Lebih Dari 8MB</strong>
                               </span>
                               @enderror
                                <div class="preview"></div>
                                @if($akun->icon != NULL)
                                <img src="{{  Storage::url('images/user_icon/'.$akun->icon) }}" width="200">
                                @else
                                <img src="{{  Storage::url('images/logo/default-icon.png') }}" width="200">
                                @endif
                           </div>
                       </div>
                        <div class="form-group row">
                            <label for="created_at" class='col-md-2 col-form-label'>Akun Terdaftar</label>
                            <div class="col-md-10">
                                <input type="text" name="created_at" readonly class='form-control  @error('created_at') is-invalid @enderror' value="{{ $car }}">
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Submit</button>
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
   function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});

function countChar(val) {
    var len = val.value.length;
    if (len >= 200) {
      val.value = val.value.substring(0, 200);
  } else {
      $('#charNum').text(199 - len);
  }
};
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
