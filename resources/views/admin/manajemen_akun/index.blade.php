@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Manajemen Akun')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Akun</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col">
                            <a href="{{ route('admin.manajemen_akun.create') }}" class="btn btn-info"><i
                                    class="fas fa-plus"></i> Tambah</a>
                        </div>
                        <div class="col">
                            <form class="float-right form-inline" method="GET">
                                <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..."
                                    autocomplete="off" name='search'>
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    {{--
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="align-middle text-left">#</th>
                                <th scope="col" class="align-middle text-left">Nama</th>
                                <th scope="col" class="align-middle text-left">Username</th>
                                <th scope="col" class="align-middle text-center">Email</th>
                                <th scope="col" class="align-middle text-center">Role</th>
                                <th scope="col" class="align-middle text-center">Icon</th>
                                <th scope="col" class="align-middle text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($akuns as $index => $akun)
                            <tr>
                                <th scope="row">{{ $index+1+(($akuns->CurrentPage()-1)*$akuns->PerPage()) }}</th>
                                <td scope="row" class="align-middle text-left">{{ $akun->name }}</td>
                                <td scope="row" class="align-middle text-left">{{ $akun->username }}</td>
                                <td scope="row" class="align-middle text-left">{{ $akun->email }}</td>
                                <td scope="row" class="align-middle text-left">{{ $akun->roles }}</td>
                                <td scope="row" class="align-middle text-left">
                                    <img src="{{  Storage::url('images/user_icon/'.$akun->icon) }}" width="100"></td>
                                <td scope="row" class="align-middle text-center">
                                    <div class='d-inline-flex'>
                                        <a href="{{ route('admin.manajemen_akun.edit', $akun->id) }}"
                                            class='btn btn-warning mr-2'><i class="fas fa-edit"></i> Edit</a>
                                        @if($akun->roles == 'Superadmin')
                                        @else
                                        <form action="{{ route('admin.manajemen_akun.destroy', $akun->id) }}"
                                            method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="button" class='btn btn-danger btn-delete'><i
                                                    class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">Daftar Akun Tidak Ada</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="paginate float-right mt-3">
                    {{$akuns->links()}}
                </div>
            </div>
        </div>

    </div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
