@extends('layouts.app')

@section('title', 'Tambah Akun')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Akun</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('warning'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('warning') }}
                    </div>
                    @endif
                    <form action="{{ route('admin.manajemen_akun.store') }}" method="POST" enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Akun</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control  @error('name') is-invalid @enderror' required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Name</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class='col-md-2 col-form-label'>Username</label>
                            <div class="col-md-10">
                                <input type="text" name="username" class='form-control  @error('username') is-invalid @enderror' required>
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Username</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class='col-md-2 col-form-label'>Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" class='form-control  @error('email') is-invalid @enderror' required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Email</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class='col-md-2 col-form-label'>Password</label>
                            <div class="col-md-10">
                                 <div class="input-container">
                                              <input  type="password" class='form-control  @error('password') is-invalid @enderror' name="password" id="password-field" required>
                                              <i toggle="#password-field" class="fas fa-eye field-icon toggle-password mr-3" style="cursor: pointer;"></i>
                                          </div>
                                          @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Password</strong>
                                </span>
                                @enderror

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="icon" class="col-md-2 col-form-label">Icon</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error(' icon') is-invalid @enderror' id="logo"
                                    data-buttonname="btn-secondary" name="icon" id="logo">
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('icon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Icon /</strong>
                                    <strong>Ukuran Icon Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div>
                        </div>
                             <div class="form-group row">
                            <label for="roles" class='col-md-2 col-form-label'>Role Akun</label>
                            <div class="col-md-10">
                                <select name="roles" class='form-control  @error('roles') is-invalid @enderror' required>
                                      <option value="superadmin">Superadmin</option>
                                    <option value="admin">Admin</option>
                                    <option value="petugas">Petugas</option>
                                </select>
                                @error('roles')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Roles</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                         <a href="{{ route('admin.manajemen_akun.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push ('scripts')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo").change(function () {
        readURL(this);
        $('.img-responsive').remove();
    });

    $('.close').on('click', function(){
        $('#image').remove();
    });

    function countChar(val) {
        var len = val.value.length;
        if (len >= 200) {
            val.value = val.value.substring(0, 200);
        } else {
            $('#charNum').text(199 - len);
        }
    };
    $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
