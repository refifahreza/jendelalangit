@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Manajemen Donasi')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Donasi</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.export_donasi2') }}?bulan={{$bulan}}&tahun={{$tahun}}" target="_blank" class="btn btn-info"><i class="fas fa-download"></i> Export to Excel</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <select name="search" class="form-control mr-2" id="">
                                <option value="">-- Filter Status --</option>
                                <option value="pending">Pending</option>
                                <option value="sukses">Sukses</option>
                                <option value="ditolak">Ditolak</option>
                            </select>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                {{--
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0">
                        <thead>
                           <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Donasi</th>
                            <th width="115" scope="col">Nama</th>
                            <th scope="col">Nomor Telp</th>
                            <th scope="col">Jenis Donasi</th>
                            <th width="110" scope="col">Jumlah</th>
                            <th scope="col" class="align-middle text-center">Status</th>
                            <th  scope="col" class="align-middle text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($donmans as $index => $donman)
                        <tr>
                            <th scope="row" class="align-middle text-left">{{ $index+1+(($donmans->CurrentPage()-1)*$donmans->PerPage()) }}</th>
                            <td scope="row" class="align-middle text-left">{{ $donman->kode_donasi }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donman->nama_donatur }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donman->notelp }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donman->jenis->jendon_name }}</td>
                            <td scope="row" class="align-middle text-left">Rp. {{ number_format($donman->jumlah,0,',','.') }}</td>
                            <td scope="row" class="align-middle text-center">
                                @if($donman->status == 'pending')
                                <span class="badge badge-primary" style="font-size: 12px">Pending</span>
                                @elseif($donman->status == 'sukses')
                                <span class="badge badge-success" style="font-size: 12px">Sukses</span>
                                @elseif($donman->status == 'ditolak')
                                <span class="badge badge-danger" style="font-size: 12px">Ditolak</span>
                                @endif
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="{{ route('admin.donasi_manual.show', $donman->kode_donasi) }}" class="btn btn-primary mr-2"><i class="fas fa-eye"></i></a>
                                    <a href="https://api.whatsapp.com/send?phone={{ $donman->notelp }}&text=Assalamualaikum%20kak%20{{ $donman->nama_donatur }},{{ $c }}" target="_blank" class="btn btn-warning mr-2"><i class="fas fa-heart"></i></a>
                                    @if($donman->status == 'ditolak')
                                    <form action="{{ route('admin.donasi_manual.destroy', $donman->kode_donasi) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i></button>
                                    </form>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">Daftar Donasi Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="paginate float-right mt-3">
                {{ $donmans->links() }}
            </div>
        </div>
    </div>

</div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection



@push('scripts')
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
