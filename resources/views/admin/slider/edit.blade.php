@extends('layouts.app')

@section('title', 'Edit Slider')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Slider</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.slider.update', $sliders->id) }}" method="POST" class='mt-3' enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Slider</label>
                            <div class="col-md-10">
                                <input type="text" name="header_text" class='form-control @error('header_text') is-invalid @enderror' value="{{ $sliders->header_text }}" required>
                                @error('header_text')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Slider</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gambar" class='col-md-2 col-form-label'>Gambar Sekarang</label>
                            <div class="col-md-10">
                                <img src="{{ Storage::url('images/slider/'.$sliders->picture) }}" alt="{{ $sliders->header_text }}" srcset="" style="width:200px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gambar" class="col-md-2 col-form-label">Gambar</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('thumnail') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="thumnail" id="logo">
                                <small class="text-danger"><i>max 8MB</i></small>
                                <br>
                                <small style="color: red;">*Jika tidak ingin mengubah Gambar. kosongkan saja bagian ini!</small>
                                @error('thumnail')
                                <span class="invalid-feedback" role="alert">
                                     <strong>Mohon Isi Gambar Slider /</strong>
                                     <strong>Ukuran Gambar Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div> 
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.slider.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection

@push('scripts')
<script>
   function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush

