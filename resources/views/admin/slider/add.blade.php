@extends('layouts.app')

@section('title', 'Tambah Slider')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Slider</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.slider.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Slider</label>
                            <div class="col-md-10">
                                <input type="text" name="header_text" class='form-control @error('header_text') is-invalid @enderror' required>
                                @error('header_text')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Slider</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gambar" class="col-md-2 col-form-label">Gambar</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('image') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="image" id="logo" required>
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                     <strong>Mohon Isi Gambar Slider /</strong>
                                     <strong>Ukuran Gambar Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div> 
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.slider.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
