@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Donasi Online')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Data Donasi Online</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.export_donasi') }}" target="_blank" class="btn btn-info"><i class="fas fa-download"></i> Export to Excel</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <select name="search" class="form-control mr-2" id="">
                                <option value="">-- Filter Status --</option>
                                <option value="pending">Pending</option>
                                <option value="success">Sukses</option>
                                <option value="failed">Gagal</option>
                                <option value="expired">Expired</option>
                            </select>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>

                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                         <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Donasi</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Nomor Telp</th>
                            <th scope="col">Jenis Donasi</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Status</th>
                            <th width="195" scope="col" class="align-middle text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($donlines as $index => $donline)
                        <tr>
                            <th scope="row" class="align-middle text-left">{{ $loop->iteration }}</th>
                            <td scope="row" class="align-middle text-left">{{ $donline->kode_donasi }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donline->nama_donatur }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donline->notelp }}</td>
                            <td scope="row" class="align-middle text-left">{{ $donline->jenis->jendon_name }}</td>
                            <td scope="row" class="align-middle text-left">Rp. {{ number_format($donline->jumlah,0,',','.') }}</td>
                            <td scope="row" class="align-middle text-left">
                                @if($donline->status == 'pending')
                                    <span class="badge badge-primary" style="font-size: 12px">Pending</span>
                                @elseif($donline->status == 'success')
                                    <span class="badge badge-success" style="font-size: 12px">Sukses</span>
                                @elseif($donline->status == 'failed')
                                    <span class="badge badge-danger" style="font-size: 12px">Gagal</span>
                                @elseif($donline->status == 'expired')
                                    <span class="badge badge-dark" style="font-size: 12px">Expired</span>
                                @endif
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="https://api.whatsapp.com/send?phone={{ $donline->notelp }}&text=Assalamualaikum%20kak%20{{ $donline->nama_donatur }},{{ $c }}" target="_blank" class="btn btn-warning mr-2"><i class="fas fa-heart"></i> Terima Kasih</a>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">Daftar Donasi Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="paginate float-right mt-3">
                {{ $donlines->links() }}
            </div>
        </div>
    </div>

</div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection



@push('scripts')
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
