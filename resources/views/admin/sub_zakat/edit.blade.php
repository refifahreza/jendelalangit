@extends('layouts.app')

@section('title', 'Edit Sub Zakat')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Sub Zakat</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.sub-zakat.update', $subzakats->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Jenis Zakat</label>
                            <div class="col-md-10">
                                <select class="form-control @error('zakat') is-invalid @enderror kat" name="zakat" id="kat" required>
                                    <option value="">-- Pilih Zakat --</option>
                                    @foreach ($zakats as $zakat)
                                    <option value="{{ $zakat->id }}" <?php if($subzakats->zakat_id == $zakat->id) { echo "selected";} ?>>{{ $zakat->nama_zakat }}</option>
                                    @endforeach
                                </select>
                                @error('zakat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Nama Sub Zakat</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $subzakats->nama_sub_zakat }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.sub-zakat.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>                               
                    </form>
                </div>
            </div>
        </div> 
    </div> 
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('.kat').select2();
    });
</script>
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush