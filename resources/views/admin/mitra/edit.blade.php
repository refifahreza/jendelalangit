@extends('layouts.app')

@section('title', 'Edit Mitra')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Mitra</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.mitra.update', $mitras->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Mitra</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $mitras->mitra_name }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                      <strong>Mohon Isi Nama Mitra</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class='col-md-2 col-form-label'>Logo Sekarang</label>
                            <div class="col-md-10">
                                <img src="{{ Storage::url('images/mitra/'.$mitras->mitra_logo) }}" alt="{{ $mitras->mitra_name }}" srcset="" style="width:200px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-2 col-form-label">Logo Mitra</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('name="image" ') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="image" id="logo" >
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                     <strong>Mohon Isi Foto Mitra /</strong>
                                         <strong>Ukuran Foto Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.mitra.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
