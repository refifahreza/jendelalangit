@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Pengaturan')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Pengaturan</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">

                    {{-- @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif --}}
                <form action="{{ route('admin.settings.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label for="WEB_TITLE" class='col-md-2 col-form-label'>Judul Website</label>
                        <div class="col-md-10">
                            <input type="text" name="WEB_TITLE" class='form-control  @error(' WEB_TITLE') is-invalid
                                @enderror' value="{{ config('web_config')['WEB_TITLE'] }}">
                            @error('WEB_TITLE')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Nama Website</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="WEB_LOGO" class='col-md-2 col-form-label'>Logo Website</label>
                        <div class="col-md-10">
                            <input type="file" name="WEB_LOGO" accept="image/*" onchange="preview_image(event)"
                                class='filestyle @error(' WEB_LOGO') is-invalid @enderror'>
                            @error('WEB_LOGO')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Logo Website</strong>
                            </span>
                            @enderror
                            <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO']) }}" alt="logo"
                                width="150" class='mt-2'>
                            <img id="output_image" class="ml-3 mt-2" width="150" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="WEB_FAVICON" class='col-md-2 col-form-label'>Favicon Website</label>
                        <div class="col-md-10">
                            <input type="file" name="WEB_FAVICON" accept="image/*" onchange="preview_image1(event)"
                                class='filestyle  @error(' WEB_FAVICON') is-invalid @enderror'>
                            @error('WEB_FAVICON')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Logo Favicon</strong>
                            </span>
                            @enderror
                            <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_FAVICON']) }}"
                                alt="favicon" width="150" class='mt-2'>
                            <img id="output_image2" class="ml-3 mt-2" width="150" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="WEB_LOGO_WHITE" class='col-md-2 col-form-label'>Logo Website (Light) /
                            (Footer)</label>
                        <div class="col-md-10">
                            <input type="file" name="WEB_LOGO_WHITE" accept="image/*" onchange="preview_image2(event)"
                                class='filestyle @error(' WEB_LOGO_WHITE') is-invalid @enderror'>
                            @error('WEB_LOGO_WHITE')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Logo Footer</strong>
                            </span>
                            @enderror
                            @if (config('web_config')['WEB_LOGO_WHITE'] === NULL)
                            <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO_WHITE']) }}"
                                alt="logo" width="150" class='mt-2'>
                            @else
                            <img src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO_WHITE']) }}"
                                alt="logo" width="150" class='mt-2'>
                            @endif
                            <img id="output_image3" class="ml-3 mt-2" width="150" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TEXT_HEADER" class='col-md-2 col-form-label'>Title Website</label>
                        <div class="col-md-10">
                            <input type="text" name="TEXT_HEADER" class='form-control  @error(' TEXT_HEADER') is-invalid
                                @enderror' value="{{ config('web_config')['TEXT_HEADER'] }}">
                            @error('TEXT_HEADER')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Text Header</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="MIN_DONATE" class='col-md-2 col-form-label'>Minimal Donasi</label>
                        <div class="col-md-10">
                            <input type="text" name="MIN_DONATE" class='form-control  @error(' MIN_DONATE') is-invalid
                                @enderror' id="min-donasi" value="{{ config('web_config')['MIN_DONATE'] }}">
                            @error('MIN_DONATE')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Minimal Donasi</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TELEPON" class='col-md-2 col-form-label'>Telepon</label>
                        <div class="col-md-10" style="display: flex;">
                            <span style="margin: 7px 10px 0px 0px;">+62</span>
                            <input type="text" name="TELEPON" class='form-control  @error(' TELEPON') is-invalid
                                @enderror' id="telp" minlength="10" maxlength="12"
                                value="{{ config('web_config')['TELEPON'] }}">
                            @error('TELEPON')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi No. Telepon</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="WHATSAPP" class='col-md-2 col-form-label'>Whatsapp</label>
                        <div class="col-md-10" style="display: flex;">
                            <span style="margin: 7px 10px 0px 0px;">+62</span>
                            <input type="text" name="WHATSAPP" class='form-control  @error(' WHATSAPP') is-invalid
                                @enderror' id="whatsapp" minlength="10" maxlength="12"
                                value="{{ config('web_config')['WHATSAPP'] }}">
                            @error('WHATSAPP')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi No. Whatsapp</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="FACEBOOK" class='col-md-2 col-form-label'>Facebook</label>
                        <div class="col-md-10">
                            <input type="text" name="FACEBOOK" class='form-control  @error(' FACEBOOK') is-invalid
                                @enderror' value="{{ config('web_config')['FACEBOOK'] }}">
                            @error('FACEBOOK')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Link Facebook</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="INSTAGRAM" class='col-md-2 col-form-label'>Instagram</label>
                        <div class="col-md-10">
                            <input type="text" name="INSTAGRAM" class='form-control  @error(' INSTAGRAM') is-invalid
                                @enderror' value="{{ config('web_config')['INSTAGRAM'] }}">
                            @error('INSTAGRAM')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Link Instagram</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="YOUTUBE" class='col-md-2 col-form-label'>Youtube</label>
                        <div class="col-md-10">
                            <input type="text" name="YOUTUBE" class='form-control  @error(' YOUTUBE') is-invalid
                                @enderror' value="{{ config('web_config')['YOUTUBE'] }}">
                            @error('YOUTUBE')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Link Youtube</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="EMAIL" class='col-md-2 col-form-label'>E-mail</label>
                        <div class="col-md-10">
                            <input type="text" name="EMAIL" class='form-control  @error(' EMAIL') is-invalid @enderror'
                                value="{{ config('web_config')['EMAIL'] }}">
                            @error('EMAIL')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Alamat Email</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ADDRESS" class='col-md-2 col-form-label'>Alamat</label>
                        <div class="col-md-10">
                            <textarea name="ADDRESS" style="resize: none;" id="" rows="3"
                                class="form-control">{{ config('web_config')['ADDRESS'] }}</textarea>
                            @error('ADDRESS')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Alamat</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="KETERANGAN" class='col-md-2 col-form-label'>Keterangan (Donasi Manual)</label>
                        <div class="col-md-10">
                            <textarea name="KETERANGAN" style="resize: none;" id="" rows="3"
                                class="form-control">{{ config('web_config')['KETERANGAN'] }}</textarea>
                            @error('KETERANGAN')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Keterangan</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="KUTIPAN" class='col-md-2 col-form-label'>Kutipan (Donasi Manual)</label>
                        <div class="col-md-10">
                            <textarea name="KUTIPAN" style="resize: none;" id="" rows="3"
                                class="form-control">{{ config('web_config')['KUTIPAN'] }}</textarea>
                            @error('KUTIPAN')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Kutipan</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TEXT_WHATSAPP" class='col-md-2 col-form-label'>Kutipan (Teks Whatsapp)</label>
                        <div class="col-md-10" style="">
                            <textarea name="TEXT_WHATSAPP" style="resize: none;line-height: 0 !important;" id=""
                                rows="3"
                                class="form-control my-editor">{{ str_replace('%20',' ',config('web_config')['TEXT_WHATSAPP']) }}</textarea>
                            @error('TEXT_WHATSAPP')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Text Whatsapp</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TEXT_FOOTER" class='col-md-2 col-form-label'>Teks Footer</label>
                        <div class="col-md-10">
                            <textarea name="TEXT_FOOTER" style="resize: none;" id="" rows="3"
                                class="form-control">{{ config('web_config')['TEXT_FOOTER'] }}</textarea>
                            @error('TEXT_FOOTER')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Text Footer</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class='btn-lg btn-primary float-right'>Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection


@push('scripts')
<script>
    $(document).ready(function() {
        var editor_config = {
            path_absolute : "/",
            height : 150,
            selector: "textarea.my-editor",
            menubar: false,
            placeholder: 'Silahkan Ketik Disini',
            toolbar: "undo redo | bold italic | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
              if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
});
    function preview_image(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }

  function preview_image1(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image2');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }

  function preview_image2(event)
    {
       var reader = new FileReader();
       reader.onload = function()
       {
          var output = document.getElementById('output_image3');
          output.src = reader.result;
      }
      reader.readAsDataURL(event.target.files[0]);
  }
</script>
<!-- Select2 -->
<script>
    $("input[id='whatsapp']").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
    $("input[id='telp']").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
    $("input[id='min-donasi']").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
</script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
