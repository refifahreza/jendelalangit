@extends('layouts.app')

@section('title', 'Edit Tentang Kami')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Tentang Kami</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.katbout.update', $katbouts->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="jenis_tentang" class="col-sm-2 col-form-label">Jenis Tentang Kami</label>
                            <div class="col-sm-10">
                                <input type="text" name="jenis_tentang" class='form-control @error(' jenis_tentang')
                                    is-invalid @enderror' value="{{$katbouts->jenis_tentang}}" required>
                                @error('jenis_tentang')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi jenis Tentang</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <label for="type" class=" form-control-label">Gambar ?</label> <input type="checkbox"
                                    id="myCheck" name="type" onclick="hiddenField()"
                                    {{ $katbouts->type != 1 ?: 'checked' }}>
                            </div>
                        </div>
                        <div id="isiHidden">
                            <!-- konten -->
                            <div class="form-group row">
                                <label for="konten" class="col-md-2 col-form-label">Konten</label>
                                <div class="col-md-10">
                                    <textarea name="konten" style="resize: none;" id="" rows="3"
                                        class="form-control my-editor">{{$katbouts->konten}}</textarea>
                                    @error('konten')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Mohon Isi Konten</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div id="text" style="display:none">
                            <!-- gambar -->
                            <div class="form-group row">
                                <label for="thumbnail" class="col-md-2 col-form-label">Thumbnail</label>
                                <div class="col-md-10">
                                    <input type="file" class="filestyle" id="logo" data-buttonname="btn-secondary"
                                        name="image" id="logo">
                                    <small class="text-danger"><i>max 8MB</i></small>
                                    {{-- @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <<strong>Mohon Isi Thumbnail /</strong>
                                    <strong>Ukuran Thumbnail Lebih Dari 8MB</strong>
                                </span>
                                @enderror --}}
                                    <div class="preview"></div>
                                    @if (isset($katbouts) && $katbouts->gambar)
                                    <div class="form-group">
                                        <img class="img-profile img-responsive" width="50%"
                                            src="{{ Storage::url('images/katbout/'.$katbouts->gambar) }}">
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="form-group" align="right">
                            <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                            <a href="{{ route('admin.katbout.index') }}"
                                class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                        </div>
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection


@push('scripts')
<script>
    function hiddenField() {
// Get the checkbox
var checkBox = document.getElementById("myCheck");
// Get the output text
var text = document.getElementById("text");
var isiHidden = document.getElementById("isiHidden");

// If the checkbox is checked, display the output text
if (checkBox.checked == true){
    text.style.display = "block";
    isiHidden.style.display = "none";
} else {
    text.style.display = "none";
    isiHidden.style.display = "block";
}
}
$(document).ready(function() {
    var editor_config = {
        selector: "textarea.my-editor",
        path_absolute : "/",
        height: 600,
        min_height: 600,
        plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
        "table directionality emoticons paste",
        "save code fullscreen autoresize codesample autosave responsivefilemanager"
        ],
        menubar : false,
        toolbar1: "undo redo restoredraft | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist hr | outdent indent table searchreplace",
        toolbar2: "| fontsizeselect | styleselect | link unlink anchor | image media emoticons | forecolor backcolor | code codesample fullscreen ",
        contextmenu: "link paste image imagetools table spellchecker",
        image_advtab: true,
        fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
        relative_urls: false,
        remove_script_host: false,
        file_picker_types: 'image',
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        },
    }
    tinymce.init(editor_config);
});
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#logo").change(function () {
        readURL(this);
        $('.img-responsive').remove();
    });

    $('.close').on('click', function(){
        $('#image').remove();
    });
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
