@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Tentang Kami')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Tentang Kami</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.katbout.create') }}" class="btn btn-info"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..." autocomplete="off" name='search'>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
              
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                           <tr>
                            <th scope="col" class="align-middle text-left">#</th>
                            <th scope="col" class="align-middle text-left">Nama Tentang</th>
                            <th scope="col" class="align-middle text-center">Status</th>
                            <th width="185" class="align-middle text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($katbouts as $index => $katbout)
                        <tr>
                            <th scope="row" class="align-middle text-left">{{ $index+1+(($katbouts->CurrentPage()-1)*$katbouts->PerPage()) }}</th>
                            <td scope="row" class="align-middle text-left">{{ $katbout->jenis_tentang }}</td>
                            {{-- <td scope="row" class="align-middle text-center"><img src="{{ Storage::url('images/katbout/'.$katbout->gambar) }}" alt="{{ $katbout->jenis_tentang }}" srcset="" height="100"></td> --}}
                            <td scope="row" class="align-middle text-center">
                                <form>
                                    <label class="switch" title="Aktif / Non-Aktif : Tentang Kami">
                                        <input type="checkbox" name="toggle" class="aktifCheck" data-toggle="toggle" data-off="Disabled"  data-target="{{$katbout->id}}" data-on="Enabled" {{ $katbout->aktif == "Y" ? 'checked' :'' }}>
                                    </form>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="{{ route('admin.katbout.edit', $katbout->id) }}" class="btn btn-warning mr-2"><i class="fas fa-edit"></i> Edit</a>
                                    <form action="{{ route('admin.katbout.destroy', $katbout->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i> Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">Daftar Tentang Kami tidak ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="paginate float-right mt-3">
                {{ $katbouts->links() }}
            </div>
        </div>
    </div>

</div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection


@push('scripts')
<script>

    $('.aktifCheck').change(function () {
        var mode = $(this).prop('checked');
        var id = $(this).attr('data-target');
        var _token = "{{ csrf_token() }}";

        var dataHide = $(this).attr('data-hide');


        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: "{{route('admin.katbout.aktif')}}",
            data: {
                id: id,
                mode: mode,
                _token: _token
            },

            success: function (data) {


                if (mode) {
                    $(dataHide).css('display','block');
                } else {
                    $(dataHide).css("display","none");
                }
            }
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#logo").change(function () {
        readURL(this);
        $('.img-responsive').remove();
    });

    $('.close').on('click', function(){
        $('#image').remove();
    });
</script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush




