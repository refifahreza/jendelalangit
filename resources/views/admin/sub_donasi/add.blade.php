@extends('layouts.app')

@section('title', 'Tambah Sub Donasi')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Sub Donasi</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.sub_donasi.store') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Nama Jenis</label>
                            <div class="col-md-10">
                                <select class="form-control @error('jenis') is-invalid @enderror" name="jenis" id="kat" required>
                                    <option value="">-- Pilih Jenis Donasi --</option>
                                    @foreach ($jendons as $jen)
                                    <option value="{{ $jen->id }}">{{ $jen->jendon_name }}</option>
                                    @endforeach
                                </select>
                                @error('jenis')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Nama Sub Donasi</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror'>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.rekening.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>                          
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('#kat').select2();
    });
</script>
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush