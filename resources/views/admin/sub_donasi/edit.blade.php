@extends('layouts.app')

@section('title', 'Edit Sub Donasi')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Sub Donasi</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.sub_donasi.update', $subdons->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Jenis Donasi</label>
                            <div class="col-md-10">
                                <select class="form-control @error('jenis') is-invalid @enderror" name="jenis" id="kat" required>
                                    <option value="">-- Pilih Jenis --</option>
                                    @foreach ($jendons as $jen)
                                    <option value="{{ $jen->id }}" <?php if($subdons->jendon_id == $jen->id) { echo "selected";} ?>>{{ $jen->jendon_name }}</option>
                                    @endforeach
                                </select>
                                @error('jenis')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Nama Sub Donasi</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $subdons->subdon_name }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.sub_donasi.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>                               
                    </form>
                </div>
            </div>
        </div> 
    </div> 
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#kat').select2();
    });
</script>
@endpush
