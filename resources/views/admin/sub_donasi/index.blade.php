@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Sub Donasi')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Sub Donasi</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.sub_donasi.create') }}" class="btn btn-info"><i class="fas fa-plus"></i> Tambah</a>
                        <a href="{{ route('admin.jenis_donasi.index') }}" class="btn btn-outline-success"><i class="fas fa-arrow-left"></i> Kembali</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..." autocomplete="off" name='search'>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
{{--
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                            <tr>
                             <th scope="col">#</th>
                             <th scope="col">Nama Sub Donasi</th>
                             <th scope="col">Jenis</th>
                             <th scope="col" class="align-middle text-center">Aktif</th>
                             <th width="185" scope="col" class="align-middle text-center">Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                        @forelse ($subdons as $index => $subdon)

                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td scope="row">{{ $subdon->subdon_name }}</td>
                            <td scope="row">{{ $subdon->jenis->jendon_name }}</td>
                            <td scope="row" class="align-middle text-center">
                                <form> 
                                    <label class="switch" title="Aktif / Non-Aktif : Sub Donasi"> 
                                        <input type="checkbox" name="toggle" class="aktifCheck" data-toggle="toggle" data-off="Disabled"  data-target="{{$subdon->id}}" data-on="Enabled" {{ $subdon->aktif == "Y" ? 'checked' :'' }}>
                                    </form>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="{{ route('admin.sub_donasi.edit', $subdon->id) }}" class='btn btn-warning mr-2'><i class="fas fa-edit"></i> Edit</a>
                                    <form action="{{ route('admin.sub_donasi.destroy', $subdon->id) }}" method="post">
                                        <form action="{{ route('admin.sub_donasi.destroy', $subdon->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">Daftar Sub Donasi Tidak Ada</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="paginate float-right mt-3">
                   {{$subdons->links()}}
               </div>
           </div>
       </div>

   </div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
    $('.aktifCheck').change(function () {
        var mode = $(this).prop('checked');
        var id = $(this).attr('data-target');
        var _token = "{{ csrf_token() }}";

        var dataHide = $(this).attr('data-hide');


        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: "{{route('admin.sub_donasi.aktif')}}",
            data: {
                id: id,
                mode: mode,
                _token: _token
            },

            success: function (data) {


                if (mode) {
                    $(dataHide).css('display','block');
                } else {
                    $(dataHide).css("display","none");
                }
            }
        });


    });
</script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
