@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Detail Donasi Transfer Bank')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Detail Donasi : {{ $donmans->kode_donasi }}</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col">
                            <a href="{{ route('admin.donasi_manual.index') }}" class="btn btn-outline-success"><i class="fas fa-arrow-left"></i> Kembali</a>
                        </div>
                    </div>
                    <form action="{{ route('admin.update_donasi.change', $donmans->kode_donasi) }}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="" class='col-md-2 col-form-label'>Status Donasi</label>
                            <div class="col-md-10">
                                <select class="form-control @error('status') is-invalid @enderror" name="status" id="kat" required>
                                    <option value="">-- Pilih Status Donasi --</option>
                                    @if($ceks == true)
                                    <option value="pending" <?php if($donmans->status == "pending") { echo "selected";} ?>>Pending</option>
                                    @elseif($ceks == false AND $donmans->status == 'pending')
                                    <option value="sukses" <?php if($donmans->status == "sukses") { echo "selected";} ?>>Sukses</option>
                                    <option value="ditolak" <?php if($donmans->status == "ditolak") { echo "selected";} ?>>Ditolak</option>
                                    @endif
                                    @if($donmans->status == 'sukses')
                                    <option value="sukses" <?php if($donmans->status == "sukses") { echo "selected";} ?>>Sukses</option>
                                    @endif
                                    @if($donmans->status == 'ditolak')
                                    <option value="ditolak" <?php if($donmans->status == "ditolak") { echo "selected";} ?>>Ditolak</option>
                                    @endif
                                </select>
                                @if($ceks == true)
                                <small style="color:red">*Dropdown "Sukses & Ditolak" hanya akan ada saat donatur mengirim Bukti Pembayaran.</small>
                                @endif
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h5 class="mb-3">Resume Donasi :</h5>
                            <hr>
                            <table class="table table-borderless mb-0">
                                <thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">No. Referensi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->kode_donasi }}</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Nama</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->nama_donatur }}</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Tanggal Transaksi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ \Carbon\Carbon::parse($donmans->created_at)->diffForHumans() }}</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Jenis Donasi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->jenis->jendon_name }}</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Jumlah Donasi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Rp. {{ number_format($donmans->jumlah,0,',','.') }}</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Pesan</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->pesan }}</h6></td>
                                        </tr>
                                    </tbody>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <h5 class="mb-3">Data Bank :</h5>
                            <hr>
                            @if($ceks == true)
                            <table class="table table-borderless mb-0">
                                <thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Bank</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Belum Konfirmasi</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">No. Rekening</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Belum Konfirmasi</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Nama Pemilik</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Belum Konfirmasi</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Cabang</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Rp. Belum Konfirmasi</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Donasi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: Belum Konfirmasi</h6></td>
                                        </tr>
                                    </tbody>
                                </thead>
                            </table>
                            @else
                            <table class="table table-borderless mb-0">
                                <thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Bank</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->bank_donatur }}</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">No. Rekening</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->norek_donatur }}</h6></td>
                                        </tr> 
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Nama Pemilik</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->atas_nama }}</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Cabang</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ $donmans->cabang }}</h6></td>
                                        </tr>
                                        <tr>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">Donasi</h6></td>
                                            <td scope="row" class="align-middle text-left"><h6 style="margin: 0;">: {{ number_format($donmans->jumlah,0,',','.') }}</h6></td>
                                        </tr>
                                    </tbody>
                                </thead>
                            </table>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <h5 class="mb-3">Bukti Pembayaran :</h5>
                            <hr>
                            @if($ceks == true)
                            Belum Konfirmasi
                            @else
                            <a href="#" class="pop">
                                <img  src="{{ Storage::url('images/buktied/'.$donmans->bukti_donasi) }}" alt="Bukti Pembayaran" width="80%">
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="" class="imagepreview" style="width: 100%;" >
            </div>
        </div>
    </div>
</div>
@endsection



@push('scripts')
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
<script>
    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');   
        });     
    });
</script>
@endpush
