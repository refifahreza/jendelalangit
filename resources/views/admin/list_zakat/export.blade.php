@php 
ob_start();
@endphp
<!DOCTYPE html>
<html lang="en">
<head xmlns:x="urn:schemas-microsoft-com:office:excel">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Donasi</title>
    @php
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Donasi.xls"'); 
    header('Cache-Control: max-age=0');
    @endphp
</head>
<body>
    <table border="1" cellpadding="10" cellspacing="0" style="table-layout:fixed; width:100%;">
     <tr>
        <td colspan="7" align="center" style="font-size: 12px; text-align: center;"><h1><b>Laporan Donasi : Bank transfer | Bulan : {{$bulan}}</b></h1></td>
    </tr>
    <tr></tr>
    <thead>
       <tr>
        <th scope="col" class="align-middle text-left" width="5%">No.</th>
        <th scope="col" class="align-middle text-left">No. Donasi</th>
        <th scope="col" class="align-middle text-left">Nama</th>
        <th scope="col" class="align-middle text-left">Nomor Telp</th>
        <th scope="col" class="align-middle text-left">Jenis Donasi</th>
        <th scope="col" class="align-middle text-center">Status</th>
        <th scope="col" class="align-middle text-center">Jumlah</th>
    </tr>
</thead>
<tbody>
    @forelse ($donmans as $index => $donman)
    <tr>
        <td scope="row" style="text-align: center; vertical-align: middle;">&nbsp;{{ $loop->iteration }}.</td>
        <td scope="row" style="text-align: left; vertical-align: middle;">&nbsp;{{ $donman->kode_donasi }}</td>
        <td scope="row" style="text-align: left; vertical-align: middle;">{{ $donman->nama_donatur }}</td>
        <td scope="row" style="text-align: left; vertical-align: middle;">&nbsp; {{ $donman->notelp }}</td>
        <td scope="row" style="text-align: left; vertical-align: middle;">{{ $donman->jenis->jendon_name }}</td>
        <td scope="row" style="text-align: center; vertical-align: middle;">
            @if($donman->status == 'pending')
            <span class="badge badge-primary">Pending</span>
            @elseif($donman->status == 'sukses')
            <span class="badge badge-success">Sukses</span>
            @elseif($donman->status == 'ditolak')
            <span class="badge badge-danger">Ditolak</span>
            @endif
        </td>
        <td scope="row" style="text-align: right; vertical-align: middle;">&nbsp;Rp. {{ number_format($donman->jumlah,0,',','.') }}</td>
    </tr>
    @empty
    <tr>
        <td colspan="8">Daftar Donasi Tidak Ada</td>
    </tr>
    @endforelse
</tbody>
<tfoot>
    <tr>
        <td colspan="6"  scope="row" style="text-align: right; vertical-align: middle;">Total Donasi</td>
        <td scope="row" style="text-align: right; vertical-align: middle;">&nbsp;Rp.{{ number_format($sum1,0,',','.') }}</td>
    </tr>
</tfoot>
</table>
</body>
</html>
@php 
ob_end_flush();
@endphp