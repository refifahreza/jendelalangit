@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Manajemen List Zakat')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen List Zakat</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.export_listzakat') }}?bulan={{$bulan}}&tahun={{$tahun}}" target="_blank" class="btn btn-info"><i class="fas fa-download"></i> Export to Excel</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <select name="search" class="form-control mr-2" id="">
                                <option value="">-- Filter Status --</option>
                                <option value="pending">Pending</option>
                                <option value="sukses">Sukses</option>
                                <option value="ditolak">Ditolak</option>
                            </select>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                {{--
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0">
                        <thead>
                           <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. List Zakat</th>
                            <th width="115" scope="col">Nama</th>
                            <th scope="col">Nomor Telp</th>
                            <th scope="col">Jenis List Zakat</th>
                            <th width="110" scope="col">Jumlah</th>
                            <th scope="col" class="align-middle text-center">Status</th>
                            <th  scope="col" class="align-middle text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($list_zakats as $index => $list_zakat)
                        <tr>
                            <th scope="row" class="align-middle text-left">{{ $index+1+(($list_zakats->CurrentPage()-1)*$list_zakats->PerPage()) }}</th>
                            <td scope="row" class="align-middle text-left">{{ $list_zakat->kode_donasi }}</td>
                            <td scope="row" class="align-middle text-left">{{ $list_zakat->nama_donatur }}</td>
                            <td scope="row" class="align-middle text-left">{{ $list_zakat->notelp }}</td>
                            <td scope="row" class="align-middle text-left">{{ $list_zakat->zakat->nama_zakat }}</td>
                            <td scope="row" class="align-middle text-left">Rp. {{ number_format($list_zakat->jumlah,0,',','.') }}</td>
                            <td scope="row" class="align-middle text-center">
                                @if($list_zakat->status == 'pending')
                                <span class="badge badge-primary" style="font-size: 12px">Pending</span>
                                @elseif($list_zakat->status == 'sukses')
                                <span class="badge badge-success" style="font-size: 12px">Sukses</span>
                                @elseif($list_zakat->status == 'ditolak')
                                <span class="badge badge-danger" style="font-size: 12px">Ditolak</span>
                                @endif
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="{{ route('admin.donasi_manual.show', $list_zakat->kode_donasi) }}" class="btn btn-primary mr-2"><i class="fas fa-eye"></i></a>
                                    <a href="https://api.whatsapp.com/send?phone={{ $list_zakat->notelp }}&text=Assalamualaikum%20kak%20{{ $list_zakat->nama_donatur }},{{ $c }}" target="_blank" class="btn btn-warning mr-2"><i class="fas fa-heart"></i></a>
                                    @if($list_zakat->status == 'ditolak')
                                    <form action="{{ route('admin.donasi_manual.destroy', $list_zakat->kode_donasi) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i></button>
                                    </form>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">Daftar List Zakat Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="paginate float-right mt-3">
                {{ $list_zakats->links() }}
            </div>
        </div>
    </div>

</div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection



@push('scripts')
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
