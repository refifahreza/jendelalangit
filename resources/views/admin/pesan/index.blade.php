@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Pesan')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Pesan</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col">
                            <form class="float-right form-inline" method="GET">
                                <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..." autocomplete="off" name='search'>
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped mb-0 table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" class="align-middle text-left">#</th>
                                    <th scope="col" class="align-middle text-left">Nama Pengirim</th>
                                    <th scope="col" class="align-middle text-left">Email</th>
                                    <th scope="col" class="align-middle text-left">Subjek</th>
                                    <th width="185" scope="col" class="align-middle text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($pesans as $index => $pesan)
                                <tr>
                                    <th scope="row" class="align-middle text-left">{{ $index+1+(($pesans->CurrentPage()-1)*$pesans->PerPage()) }}</th>
                                    <td scope="row" class="align-middle text-left">{{ $pesan->nama }}</td>
                                    <td scope="row" class="align-middle text-left">{{ $pesan->email }}</td>
                                    <td scope="row" class="align-middle text-left">{{ $pesan->subjek }}</td>
                                    <td scope="row" class="align-middle text-center">
                                        <div class='d-inline-flex'>
                                            <button type="button" class="btn btn-warning mr-2" data-toggle="modal" data-target="#modal-edit-{{$pesan->id}}"><i class="fas fa-eye"></i> Lihat </button>
                                            @include('admin.pesan.edit')

                                            <form action="{{ route('admin.pesan.destroy', $pesan->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i> Hapus</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">Daftar Pesan Tidak Ada</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="paginate float-right mt-3">
                        {{$pesans->links()}}
                    </div>
                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection


@push('scripts')

<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush