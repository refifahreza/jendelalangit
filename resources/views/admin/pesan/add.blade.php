<div class="modal fade modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Tambah Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.video.store') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="judul_video" class="col-sm-3 col-form-label">Judul Video</label>
                        <div class="col-sm-9">
                            <input type="text" name="judul_video" class='form-control @error('judul_video') is-invalid @enderror' required>
                            @error('judul_video')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Judul Video</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="link" class="col-sm-3 col-form-label">Link Video</label>
                        <div class="col-sm-9">
                            <input type="text" name="link" class='form-control @error('link') is-invalid @enderror' required>
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Link Video</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="katvid_id" class="col-sm-3 col-form-label">Kategori Video</label>
                        <div class="col-sm-9">
                            <select name="katvid_id" class="form-control @error('katvid_id') is-invalid @enderror" required>
                                @foreach($katvids as $katvid)
                                <option value="{{$katvid->id}}">{{$katvid->kategori_video}}</option>
                                @endforeach
                            </select>
                            @error('katvid_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Kategori Video</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class='btn-lg btn-primary waves-effect waves-light float-right'>Tambah</button>
                    <a href="" class="btn-lg btn-outline-secondary float-right mr-2" data-dismiss="modal" aria-hidden="true">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</div>

