<div class="modal fade modal-edit" id="modal-edit-{{$pesan->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Detail Isi Pesan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="judul_video" class="col-sm-3 col-form-label">Nama Pengirim</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama_pengirim" value="{{$pesan->nama}}" class='form-control @error('judul_video') is-invalid @enderror' readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="link" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" value="{{$pesan->email}}" class='form-control' readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="link" class="col-sm-3 col-form-label">Subjek</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" value="{{$pesan->subjek}}" class='form-control' readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="link" class="col-sm-3 col-form-label">Isi Pesan</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" readonly>{{$pesan->pesan}}</textarea>
                    </div>
                </div>

                <a class="btn-lg btn-primary waves-effect waves-light float-right" href="mailto:{{$pesan->email}}?subject=RE:{{$pesan->subjek}}&amp;body=<<Tulis%20balasan%20anda%20disini>>%0D%0A%0D%0A%0D%0A%0D%0A--------------------------%0D%0A%0D%0A%0D%0A{{$pesan->pesan}}">Kirim Email</a>
                <a href="" class="btn-lg btn-outline-secondary float-right mr-2" data-dismiss="modal" aria-hidden="true">Kembali</a>
            </div>
        </div>
    </div>
</div>


