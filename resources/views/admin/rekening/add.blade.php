@extends('layouts.app')

@section('title', 'Tambah Rekening')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Rekening</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.rekening.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Bank</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Bank /</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class='col-md-2 col-form-label' >Kode Bank</label>
                            <div class="col-md-10">
                                <input type="text" minlength="2" maxlength="3" name="code" class='form-control @error('code') is-invalid @enderror' required>
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Kode Bank </strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number" class='col-md-2 col-form-label' >No. Rek</label>
                            <div class="col-md-10">
                                <input type="number" minlength="6" maxlength="16" name="number" class='form-control @error('number') is-invalid @enderror' required>
                                @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi No. Rekening</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author" class='col-md-2 col-form-label' >Atas Nama</label>
                            <div class="col-md-10">
                                <input type="text" name="author" class='form-control @error('author') is-invalid @enderror' required>
                                @error('author')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Author</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-md-2 col-form-label" >Logo Bank</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('name="image" ') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="image" id="logo" required>
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('name="image" ')
                                <span class="invalid-feedback" role="alert">
                                     <strong>Mohon Isi Logo Bank /</strong>
                                         <strong>Ukuran Logo Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Tambah</button>
                        <a href="{{ route('admin.rekening.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
    $("input[name='code']").on('input', function() {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
    // $("input[name='number']").on('input', function() {
    //   $(this).val($(this).val().replace(/[^0-9]/g, ''));
    // });
    });
   function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush

