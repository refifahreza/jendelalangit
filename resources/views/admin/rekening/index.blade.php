@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Rekening')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Rekening</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col">
                        <a href="{{ route('admin.rekening.create') }}" class="btn btn-info"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                    <div class="col">
                        <form class="float-right form-inline" method="GET">
                            <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..." autocomplete="off" name='search'>
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                {{--
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                            <tr>
                               <th scope="col" class="align-middle text-left" >#</th>
                               <th scope="col" class="align-middle text-left">Nama Bank</th>
                               <th scope="col" class="align-middle text-left">Kode Bank</th>
                               <th scope="col" class="align-middle text-left">No Rekening</th>
                               <th scope="col" class="align-middle text-left">Pemilik</th>
                               <th scope="col" class="align-middle text-center">Logo</th>
                               <th scope="col" class="align-middle text-center">Status</th>
                               <th width="185" scope="col" class="align-middle text-center">Aksi</th>
                           </tr>
                       </thead>
                       <tbody>
                        @forelse ($rekenings as $index => $rekening)
                        <tr>
                            <th scope="row" class="align-middle text-left">{{ $index+1+(($rekenings->CurrentPage()-1)*$rekenings->PerPage()) }}</th>
                            <td scope="row" class="align-middle text-left">{{ $rekening->rekening_name }}</td>
                            <td scope="row" class="align-middle text-left">{{ $rekening->rekening_code }}</td>
                            <td scope="row" class="align-middle text-left">{{ $rekening->rekening_number }}</td>
                            <td scope="row" class="align-middle text-left">{{ $rekening->rekening_author }}</td>
                            <td scope="row" class="align-middle text-center"><img src="{{ Storage::url('images/rekening/'.$rekening->logo) }}" alt="" srcset="" style="width:130px"></td>
                            <td scope="row" class="align-middle text-center">
                                <form>
                                    <label class="switch" title="Aktif / Non-Aktif : Rekening">
                                        <input type="checkbox" name="toggle" class="aktifCheck" data-toggle="toggle" data-off="Disabled"  data-target="{{$rekening->id}}" data-on="Enabled" {{ $rekening->aktif == "Y" ? 'checked' :'' }}>
                                    </form>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td scope="row" class="align-middle text-center">
                                <div class='d-inline-flex'>
                                    <a href="{{ route('admin.rekening.edit', $rekening->id) }}" class='btn btn-warning mr-2'><i class="fas fa-edit"></i> Edit</a>
                                    <form action="{{ route('admin.rekening.destroy', $rekening->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class='btn btn-danger btn-delete'><i class="fas fa-trash"></i> Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">Daftar Rekening Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="paginate float-right mt-3">
             {{$rekenings->links()}}
         </div>
     </div>
 </div>

</div> <!-- end col -->

</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
    $('.aktifCheck').change(function () {
        var mode = $(this).prop('checked');
        var id = $(this).attr('data-target');
        var _token = "{{ csrf_token() }}";

        var dataHide = $(this).attr('data-hide');


        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: "{{route('admin.rekening.aktif')}}",
            data: {
                id: id,
                mode: mode,
                _token: _token
            },

            success: function (data) {


                if (mode) {
                    $(dataHide).css('display','block');
                } else {
                    $(dataHide).css("display","none");
                }
            }
        });


    });
</script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
