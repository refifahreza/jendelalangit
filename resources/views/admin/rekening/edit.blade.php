@extends('layouts.app')

@section('title', 'Edit Rekening')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Rekening</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.rekening.update', $rekenings->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Bank</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $rekenings->rekening_name }}" required>
                                @error('name')
                                 <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Nama Bank /</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="code" class='col-md-2 col-form-label'>Kode Bank</label>
                            <div class="col-md-10">
                                <input type="number" name="code" class='form-control @error('code') is-invalid @enderror' value="{{ $rekenings->rekening_code }}" required>
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number" class='col-md-2 col-form-label'>No. Rek</label>
                            <div class="col-md-10">
                                <input type="number" name="number" class='form-control @error('number') is-invalid @enderror' value="{{ $rekenings->rekening_number }}" required>
                                @error('number')
                               <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Kode Bank </strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author" class='col-md-2 col-form-label'>Atas Nama</label>
                            <div class="col-md-10">
                                <input type="text" name="author" class='form-control @error('author') is-invalid @enderror' value="{{ $rekenings->rekening_author }}" required>
                                @error('author')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Author</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="logo" class='col-md-2 col-form-label'>Logo Sekarang</label>
                            <div class="col-md-10">
                                <img src="{{ Storage::url('images/rekening/'.$rekenings->logo) }}" alt="{{ $rekenings->rekening_name }}" srcset="" style="width:310px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-md-2 col-form-label">Logo</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('name="image" ') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="image" id="logo">
                                <small class="text-danger"><i>*Kosongkan jika tidak ingin mengubah logo!</i></small>
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                       <strong>Mohon Isi Logo Bank /</strong>
                                         <strong>Ukuran Logo Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.rekening.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>

                </div>
            </div>

        </div> <!-- end col -->

    </div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
    $("input[name='code']").on('input', function() {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
//    $("input[name='number']").on('input', function() {
//       $(this).val($(this).val().replace(/[^0-9]/g, ''));
//     });
    });
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
