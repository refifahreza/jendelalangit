@extends('layouts.app')

@section('title', 'Edit File')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit File</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.file.update', $file->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                         <div class="form-group row">
                            <label for="judul" class='col-md-2 col-form-label'>Judul</label>
                            <div class="col-md-10">
                                <input type="text" name="judul" class='form-control @error('judul') is-invalid @enderror' value="{{$file->judul}}">
                                @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Judul File</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-2 col-form-label">File Upload</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('file') is-invalid @enderror' data-buttonname="btn-secondary" name="file" id="logo" required>
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('file')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi File /</strong>
                                    <strong>Ukuran File Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="judul" class='col-md-2 col-form-label'>Keterangan File</label>
                            <div class="col-md-10">
                                <input type="text" name="keterangan_file" class='form-control @error('keterangan_file') is-invalid @enderror' value="{{$file->keterangan_file}}">
                                @error('keterangan_file')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi keterangan File</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.file.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
//  function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();

//         reader.onload = function (e) {
//             $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
//         }

//         reader.readAsDataURL(input.files[0]);
//     }
// }

// $("#logo").change(function () {
//     readURL(this);
//     $('.img-responsive').remove();
// });

// $('.close').on('click', function(){
//     $('#image').remove();
// });
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
