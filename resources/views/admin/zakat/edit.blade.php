<div class="modal fade modal-edit" id="modal-edit-{{$zakat->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Zakat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.zakat.update', $zakat->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label">Nama Zakat</label>
                        <div class="col-sm-9">
                          <input type="text" name="name" class='form-control @error('name') is-invalid @enderror' value="{{ $zakat->nama_zakat }}" required>
                          @error('name')
                          <span class="invalid-feedback" role="alert">
                            <strong>Mohon Isi Nama Zakat</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                   <button type="submit" class='btn-lg btn-primary waves-effect waves-light float-right'>Tambah</button>
                     <a href="" class="btn-lg btn-outline-secondary float-right mr-2" data-dismiss="modal" aria-hidden="true">Kembali</a>
            </form>
        </div>
    </div>
</div>
</div>


