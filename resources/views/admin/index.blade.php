@extends('layouts.app')
@section('content')

@section('title', 'Dashboard')
<!-- start page title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-title-box">
      <h4>Dashboard</h4>
    </div>
  </div>
</div>
<!-- end page title -->

<div class="row">
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-hand-holding-usd float-right" style="background-color: #ffac32 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Donasi</h6>
          <h2>Rp. {{ number_format($count,0,',','.') }}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-money-check float-right" style="background-color: #29bbe3 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Rekening</h6>
          <h2>{{$rekening}}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-image float-right" style="background-color:  #16ce46 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Slider</h6>
          <h2>{{$slider}}</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-handshake float-right" style="background-color: #ffac32 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Mitra</h6>
          <h2>{{$mitra}}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-newspaper float-right" style="background-color: #29bbe3 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Berita</h6>
          <h2>{{$berita}}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-4 col-md-6">
    <div class="card mini-stat bg-primary">
      <div class="card-body mini-stat-img">
        <div class="mini-stat-icon">
          <i class="fas fa-images float-right" style="background-color: #16ce46 !important;"></i>
        </div>
        <div class="text-white">
          <h6 class="text-uppercase mb-3 font-size-16">Total Album</h6>
          <h2>{{$album}}</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end row -->
@endsection
