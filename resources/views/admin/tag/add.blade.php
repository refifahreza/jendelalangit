<div class="modal fade modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Tambah Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.tag.store') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="nama_tag" class="col-sm-3 col-form-label">Nama Tag</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_tag" class='form-control @error('nama_tag') is-invalid @enderror' required>
                            @error('nama_tag')
                            <span class="invalid-feedback" role="alert">
                                <strong>Mohon Isi Nama Tag</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <button type="submit" class='btn-lg btn-primary waves-effect waves-light float-right'>Tambah</button>
                    <a href="" class="btn-lg btn-outline-secondary float-right mr-2" data-dismiss="modal" aria-hidden="true">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</div>

