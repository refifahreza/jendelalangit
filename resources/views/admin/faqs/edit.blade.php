@extends('layouts.app')

@section('title', 'Edit Faqs')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Faqs</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.faqs.update', $faqs->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="pertanyaan" class='col-md-2 col-form-label'>Pertanyaan</label>
                            <div class="col-md-10">
                                <input type="text" name="pertanyaan" class='form-control @error('pertanyaan') is-invalid @enderror' value="{{ $faqs->pertanyaan }}" required>
                               @error('pertanyaan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Pertanyaan</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jawaban" class='col-md-2 col-form-label'>Jawaban</label>
                            <div class="col-md-10">
                                <textarea name="jawaban" style="resize: none;" id="" rows="3" class="form-control" required>{{ $faqs->jawaban }}</textarea>
                                @error('jawaban')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Jawaban</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{ route('admin.faqs.index') }}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
