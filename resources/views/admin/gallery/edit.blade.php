@extends('layouts.app')

@section('title', 'Edit Gallery')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Gallery</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.gallery.update', $id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label for="name" class='col-md-2 col-form-label'>Nama Foto</label>
                            <div class="col-md-10">
                                <input type="text" value="{{$gallerys->nama_foto}}" name="name" class='form-control @error('name') is-invalid @enderror'>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="foto" class='col-md-2 col-form-label' required>Foto Sekarang</label>
                            <div class="col-md-10">
                                <img src="{{ Storage::url('images/gallery/'.$gallerys->foto) }}" alt="{{$gallerys->nama_foto}}" srcset="" style="width:200px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="foto" class="col-md-2 col-form-label">Foto</label>
                            <div class="col-md-10">
                                <input type="file" class='filestyle @error('image') is-invalid @enderror' id="logo"  data-buttonname="btn-secondary" name="image" id="logo">
                                <small class="text-danger"><i>max 8MB</i></small>
                                @error('image')
                                 <span class="invalid-feedback" role="alert">
                                    <strong>Mohon Isi Thumbnail /</strong>
                                    <strong>Ukuran Thumbnail Lebih Dari 8MB</strong>
                                </span>
                                @enderror
                                <div class="preview"></div>
                            </div> 
                        </div>
                        <button type="submit" class='btn-lg btn-primary float-right'>Edit</button>
                        <a href="{{route('admin.gallery.index', $gallerys->album_id)}}" class="btn-lg btn-outline-secondary float-right mr-2">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div> 
@endsection


@push('scripts')
<script>
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(".preview").html("<img src='" + e.target.result + "' width='200' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#logo").change(function () {
    readURL(this);
    $('.img-responsive').remove();
});

$('.close').on('click', function(){
    $('#image').remove();
});
</script>
<!-- Select2 -->
<script src="{{asset('template/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('template/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script>
@endpush
