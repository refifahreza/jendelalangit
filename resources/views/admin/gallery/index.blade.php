@extends('layouts.app')

@push('style')
<!-- Plugins css -->
<link href="{{ asset('template/libs/sweet-alert2/sweetalert2.css')}}" rel="stylesheet">
@endpush
@section('title', 'Foto')

@section('content')
<div class="container-fluid">
    <div class='notifications top-right'></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Manajemen Foto</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col">
                            <a href="{{ route('admin.gallery.create', $album_id) }}" class="btn btn-info"><i
                                    class="fas fa-plus"></i> Tambah</a>
                            <a href="{{ route('admin.album.index') }}" class="btn btn-outline-success"><i
                                    class="fas fa-arrow-left"></i> Kembali</a>
                        </div>
                        <div class="col">
                            <form class="float-right form-inline" method="GET">
                                <input type="text" class="form-control mr-2" placeholder="Masukan Pencarian..."
                                    autocomplete="off" name='search'>
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    {{--
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                </div>
                @endif
                @if(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('warning') }}
                </div>
                @endif
                @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('danger') }}
                </div>
                @endif --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-0 table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="align-middle text-left">#</th>
                                <th scope="col" class="align-middle text-left">Nama Foto</th>
                                <th scope="col" class="align-middle text-left">Foto</th>
                                <th scope="col" class="align-middle text-center">Aktif</th>
                                <th width="185" scope="col" class="align-middle text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($gallerys as $index => $gallery)
                            <tr>
                                <th scope="row">{{ $index+1+(($gallerys->CurrentPage()-1)*$gallerys->PerPage()) }}</th>
                                <td scope="row" class="align-middle text-left">{{ $gallery->nama_foto }}</td>
                                <td scope="row" class="align-middle text-center"><img
                                        src="{{ Storage::url('images/gallery/'.$gallery->foto) }}"
                                        alt="{{ $gallery->nama_foto }}" style="width:130px"></td>
                                <td scope="row" class="align-middle text-center">
                                    <form>
                                        <label class="switch" title="Aktif / Non-Aktif : gallery">
                                            <input type="checkbox" name="toggle" class="aktifCheck" data-toggle="toggle"
                                                data-off="Disabled" data-target="{{$gallery->id}}" data-on="Enabled"
                                                {{ $gallery->aktif == "Y" ? 'checked' :'' }}>
                                    </form>
                                    <span class="slider round"></span>
                                    </label>
                                </td>
                                <td scope="row" class="align-middle text-center">
                                    <div class='d-inline-flex'>
                                        <a href="{{ route('admin.gallery.edit', $gallery->id) }}"
                                            class='btn btn-warning mr-2'><i class="fas fa-edit"></i> Edit</a>
                                        <form action="{{ route('admin.gallery.destroy', $gallery->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="button" class='btn btn-danger btn-delete'><i
                                                    class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">Daftar Foto Tidak Ada</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="paginate float-right mt-3">
                    {{$gallerys->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
<script>
    $('.aktifCheck').change(function () {
        var mode = $(this).prop('checked');
        var id = $(this).attr('data-target');
        var _token = "{{ csrf_token() }}";

        var dataHide = $(this).attr('data-hide');


        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: "{{route('admin.gallery.aktif')}}",
            data: {
                id: id,
                mode: mode,
                _token: _token
            },

            success: function (data) {


                if (mode) {
                    $(dataHide).css('display','block');
                } else {
                    $(dataHide).css("display","none");
                }
            }
        });


    });
</script>
<script src="{{ asset('template/libs/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
@endpush
