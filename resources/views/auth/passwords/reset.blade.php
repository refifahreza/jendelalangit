<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <title>Lupa Password | Jendela Langit | </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ Storage::url('/images/logo/'.config('web_config')['WEB_FAVICON'])}}"> 

    <!-- Bootstrap Css -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Icons Css -->
    <link href="{{asset('template/css/icons.min.css')}}" rel="stylesheet" type="text/css">
    <!-- App Css-->
    <link href="{{asset('template/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css">
    <!-- Style Css -->
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css">

</head>

<body style="background-color: #096387 !important;">
    <div class="account-pages my-4 pt-sm-5 login">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="card-body pt-0">
                            <h3 class="text-center mt-4">
                                <img class="logo logo-admin" src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}"
                                height="100" alt="logo">
                            </h3>
                            <div class="p-3">
                                <h4 class="text-muted font-18 m-b-5 text-center">{{ __('Reset Password') }}</h4>
                                @if($errors->any())
                                <div class="alert alert-danger">Username atau password salah!</div>
                                @endif

                                <form class="form-horizontal mt-4" method="POST" action="{{ route('password.update') }}">
                                    @csrf

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-group">
                                        <label for="username">{{ __('E-Mail Address') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="userpassword">{{ __('Password') }}</label>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="userpassword">{{ __('Confirm Password') }}</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    <div class="form-group row mt-4">
                                        <div class="col-6 text-right">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">{{ __('Reset Password') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="{{asset('template/libs/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('template/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('template/libs/metismenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('template/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('template/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{asset('template/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- App js -->
    <script src="{{asset('template/js/app.js')}}"></script>
</body>
</html>
