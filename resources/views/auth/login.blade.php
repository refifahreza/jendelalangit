<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themesbrand.com/lexa/layouts/purple/vertical/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Jun 2020 06:23:23 GMT -->

<head>
    <meta charset="utf-8" />
    <title>Login | Jendela Langit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ Storage::url('/images/logo/'.config('web_config')['WEB_FAVICON'])}}">

    <!-- Bootstrap Css -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">

    <!-- Icons Css -->
    <link href="{{asset('template/css/icons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/fonts/font-awesome/css/all.css')}}" rel="stylesheet" type="text/css">

    <!-- App Css-->
    <link href="{{asset('template/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css">

    <!-- Style Css -->
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css">
</head>

<body style="background-color: #096387 !important;">
    <div class="account-pages">
        <div class="container login">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <h3 class="text-center mt-4">
                                <img class="logo logo-admin"
                                    src="{{ Storage::url('/images/logo/'.config('web_config')['WEB_LOGO'])}}"
                                    height="100" alt="logo">
                            </h3>
                            <div class="p-3">
                                <h4 class="text-muted font-18 m-b-5 text-center">Selamat Datang</h4>
                                <p class="text-muted text-center">Masuk untuk melanjutkan ke Panel Admin</p>
                                @if($errors->any())
                                <div class="alert alert-danger">Username atau password salah!</div>
                                @endif
                                <form class="form-horizontal mt-4" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" required name="username" id="username"
                                            placeholder="Enter username">
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="userpassword">Password</label>

                                            <div class="input-container">
                                                <input type="password" class="form-control" name="password"
                                                    id="password-field" />
                                                <i toggle="#password-field"
                                                    class="fa fa-fw fa-eye field-icon toggle-password mr-2"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mt-4">
                                        <div class="col-12 text-right">
                                            <button class="btn btn-primary w-md waves-effect waves-light"
                                                type="submit">Log In</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="{{asset('template/libs/jquery/jquery.min.js')}}"></script>
    <script>
        $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
    </script>
    <script src="{{asset('template/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('template/libs/metismenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('template/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('template/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{asset('template/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- App js -->
    <script src="{{asset('template/js/app.js')}}"></script>
</body>

</html>
