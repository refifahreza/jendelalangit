<?php

use Illuminate\Database\Seeder;
use App\Models\Subdonasi;

class SubdonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subdonasi::insert([
            ['jendon_id' => '1',
            'subdon_name' =>'Pembangunan Pesantren'],
            ['jendon_id' => '2',
            'subdon_name' =>'Donasi Yayasan'],
        ]);
    }
}
