<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'Super Admin',
                'username' => 'superadmin',
                'email' => 'super@admin.com',
                'password' => Hash::make('secret'),
                'roles' => 'Superadmin',
                'icon' => '1623212524_Logo.jpg'
            ],
            [
                'name' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'roles' => 'Admin',
                'icon' => '1623212412_logo.png'
            ],
            [
                'name' => 'Petugas',
                'username' => 'petugas',
                'email' => 'petugas@petugas.com',
                'password' => Hash::make('petugas'),
                'roles' => 'Petugas',
                'icon' => '1623212399_logo.png'
            ],
        ]);
    }
}
