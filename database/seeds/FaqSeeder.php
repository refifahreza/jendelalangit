<?php

use Illuminate\Database\Seeder;
use App\Models\Faqs;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faqs::insert([
            ['pertanyaan' => 'Apakah website ini terpercaya?', 'jawaban' => 'Tentu saja, karena yayasan ini sudah berdiri sejak 1999'],
            ['pertanyaan' => 'Donasi akan disalurkan kemana?', 'jawaban' => 'Akan kami gunakan untuk kepentingan anak yatim'],
            ['pertanyaan' => 'Apakah ada pungutan pajak?', 'jawaban' => 'Tidak ada.'],
        ]);
    }
}
