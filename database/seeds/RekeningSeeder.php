<?php

use Illuminate\Database\Seeder;
use App\Models\Rekening;

class RekeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rekening::insert([
            ['rekening_name' => 'Bank Kaltim Syariah', 
            'rekening_slug' => 'bank-kaltim-syariah', 
            'rekening_code' => '124', 
            'rekening_number' => '5101326897', 
            'rekening_author' => 'HERYANI (Bendahara)', 
            'logo' => 'bankaltim.png'],

            ['rekening_name' => 'BCA', 
            'rekening_slug' => 'bca', 
            'rekening_code' => '014', 
            'rekening_number' => '0272844702', 
            'rekening_author' => 'HERYANI (Bendahara)', 
            'logo' => 'bca.png'],

            ['rekening_name' => 'MANDIRI', 
            'rekening_slug' => 'mandiri', 
            'rekening_code' => '008', 
            'rekening_number' => '1480017609648', 
            'rekening_author' => 'HERYANI (Bendahara)', 
            'logo' => 'mandiri.png'],
        ]);
    }
}
