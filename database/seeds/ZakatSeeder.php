<?php

use Illuminate\Database\Seeder;
use App\Models\Zakat;

class ZakatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Zakat::insert([
            ['nama_zakat' =>'Zakat Fitrah'],
            ['nama_zakat' =>'Zakat Maal'],
        ]);
    }
}
