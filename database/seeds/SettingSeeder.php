<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::insert([
            ['key' => 'WEB_TITLE', 'value' => 'Jendela Langit Semesta'],
            ['key' => 'WEB_LOGO', 'value' => 'logo.png'],
            ['key' => 'WEB_FAVICON', 'value' => 'favicon.png'],
            ['key' => 'WEB_LOGO_WHITE', 'value' => 'logo.png'],
            ['key' => 'TEXT_HEADER', 'value' => 'Selamat Datang di Jendela Langit Semesta'],
            ['key' => 'MIN_DONATE', 'value' => '10000'],
            ['key' => 'TELEPON', 'value' => '81212219999'],
            ['key' => 'WHATSAPP', 'value' => '81212219999'],
            ['key' => 'FACEBOOK', 'value' => 'https://www.facebook.com/jendelalangit.id'],
            ['key' => 'INSTAGRAM', 'value' => 'https://www.instagram.com/jendelalangit.id/'],
            ['key' => 'YOUTUBE', 'value' => 'https://www.youtube.com/channel/UCQpDX10DiUXbo8erE5KCFLg?view_as=subscriber'],
            ['key' => 'EMAIL', 'value' => 'jendelalangitsemesta@gmail.com'],
            ['key' => 'ADDRESS', 'value' => 'Jalan. D. Poso, Sungai Pinang Luar, Kec. Samarinda Kota, Kota Samarinda, Kalimantan Timur 75242'],
            ['key' => 'KETERANGAN', 'value' => 'Di mohon untuk melakukan konfirmasi donasi maksimal 1 x 24 Jam setelah melakukan donasi'],
            ['key' => 'KUTIPAN', 'value' => 'Terima Kasih'],
            ['key' => 'TEXT_WHATSAPP', 'value' => 'Makasih%20udah%20Donasi'],
            ['key' => 'TEXT_FOOTER', 'value' => 'Jendela Langit Semesta'],
        ]);
    }
}
