<?php

use Illuminate\Database\Seeder;
use App\Models\Jendonasi;

class JendonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jendonasi::insert([
            ['jendon_name' => 'Umum'],
            ['jendon_name' => 'Sedekah'],
        ]);
    }
}
