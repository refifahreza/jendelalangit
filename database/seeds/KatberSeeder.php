<?php

use Illuminate\Database\Seeder;
use App\Models\Katber;

class KatberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Katber::insert([
            [
                'katber_name' => 'Bantuan Logistik Donatur',
                'katber_slug' => 'bantuan-logistik-donatur',
            ],
            [
                'katber_name' => 'Penggunaan Dana Bantuan',
                'katber_slug' => 'penggunaan-dana-bantuan',
            ],
            [
                'katber_name' => 'Renovasi Yayasan',
                'katber_slug' => 'renovasi-yayasan',
            ],
            [
                'katber_name' => 'Kunjungan Donatur',
                'katber_slug' => 'kunjungan-donatur',
            ],
            [   
                'katber_name' => 'Acara Bersama',
                'katber_slug' => 'acara-bersama',
            ],
        ]);
    }
}
