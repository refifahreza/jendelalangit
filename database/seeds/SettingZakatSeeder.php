<?php

use Illuminate\Database\Seeder;
use App\Models\Settingzakat;

class SettingZakatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settingzakat::insert([
            ['key' => 'HARGA_EMAS', 'value' => 900000],
        ]);
    }
}
