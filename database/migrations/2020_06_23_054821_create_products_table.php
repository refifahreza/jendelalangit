<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('produk_name','255');
            $table->unsignedBigInteger('katpro_id');
            $table->string('produk_slug','255');
            $table->string('produk_pict','255');
            $table->text('produk_desc');
            $table->bigInteger('produk_price');
            $table->bigInteger('produk_stok');
            $table->enum('produk_status', ['aktif', 'draft', 'nonaktif']);
            $table->timestamps();

            $table->foreign('katpro_id')->references('id')->on('kategori_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
