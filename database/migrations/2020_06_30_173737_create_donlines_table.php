<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donlines', function (Blueprint $table) {
            $table->id();
            $table->string('kode_donasi', 25);
            $table->string('nama_donatur', 50);
            $table->string('notelp', 15);
            $table->text('pesan')->nullable();
            $table->unsignedBigInteger('jenis_id');
            $table->string('detail_jenis', 50)->nullable();
            $table->string('jumlah', 20);
            $table->string('status', 50)->default('pending');
            $table->string('snap_token', 150)->nullable();
            $table->timestamps();

            $table->foreign('jenis_id')->references('id')->on('jenis_donasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donlines');
    }
}
