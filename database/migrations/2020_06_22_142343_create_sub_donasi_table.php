<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubDonasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_donasi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jendon_id');
            $table->string('subdon_name','255');
            $table->enum('aktif', ['Y', 'N']);
            $table->timestamps();

            $table->foreign('jendon_id')->references('id')->on('jenis_donasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_donasi');
    }
}
