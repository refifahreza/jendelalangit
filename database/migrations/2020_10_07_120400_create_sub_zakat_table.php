<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubZakatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_zakat', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('zakat_id');
            $table->string('nama_sub_zakat','255');
            $table->string('rumus_pertama','255')->nullable();
            $table->string('rumus_kedua','255')->nullable();
            $table->string('rumus_ketiga','255')->nullable();
            $table->enum('aktif', ['Y', 'N']);
            $table->timestamps();

            $table->foreign('zakat_id')->references('id')->on('zakats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_zakat');
    }
}
