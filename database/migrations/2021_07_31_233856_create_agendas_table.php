<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul', '255');
            $table->string('lokasi', '255');
            $table->string('slug', '255');
            $table->string('pengajar', '255');
            $table->string('penyelenggara', '255');
            $table->longText('isi_agenda');
            $table->dateTime('waktu_awal');
            $table->dateTime('waktu_akhir');
            $table->enum('aktif', ['Y', 'N']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
