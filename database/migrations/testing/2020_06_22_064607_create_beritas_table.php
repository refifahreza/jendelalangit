<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beritas', function (Blueprint $table) {
            $table->id();
            $table->string('title','255');
            $table->string('slug_title','255');
            $table->enum('aktif', ['Y', 'N']);
            $table->unsignedBigInteger('kategori_id');
            $table->unsignedBigInteger('author_id');
            $table->string('thumnail','255')->nullable();
            $table->longtext('content');
            $table->text('kutipan');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('kategori_id')->references('id')->on('kategori_beritas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beritas');
    }
}
