<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKatboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('katbouts', function (Blueprint $table) {
            $table->id();
            $table->string('jenis_tentang', 50);
            $table->string('slug', 50);
            $table->string('gambar', 150)->nullable();
            $table->text('konten')->nullable();
            $table->enum('aktif', ['Y','N']);
            $table->integer('type')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('katbouts');
    }
}
