<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beritas', function (Blueprint $table) {
            $table->id();
            $table->string('title', '255')->nullable();
            $table->string('title_slug', '255')->nullable();
            $table->string('seo_judul', '255')->nullable();
            $table->enum('aktif', ['Y', 'N'])->default('Y');
            $table->enum('headline', ['Y', 'N'])->default('Y');
            $table->unsignedBigInteger('kategori_id')->nullable();
            $table->unsignedBigInteger('author_id')->nullable();
            $table->string('thumnail', '255')->nullable();
            $table->string('deskripsi_gambar')->nullable();
            $table->longtext('content')->nullable();
            $table->text('deskripsi_meta')->nullable();
            $table->text('kutipan')->nullable();
            $table->integer('dibaca')->default(0);
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('kategori_id')->references('id')->on('kategori_beritas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beritas');
    }
}
