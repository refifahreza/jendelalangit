<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListZakatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_zakat', function (Blueprint $table) {
            $table->id();
            $table->string('kode_zakat', 25);
            $table->string('nama_donatur', 50);
            $table->string('notelp', 15);
            $table->unsignedBigInteger('zakat_id');
            
            $table->unsignedBigInteger('rekening_id');
            $table->string('jumlah', 20);
            $table->enum('status', ['pending', 'ditolak', 'sukses'])->default('pending');
            $table->string('bank_donatur', 40)->nullable();
            $table->string('norek_donatur', 20)->nullable();
            $table->string('atas_nama', 40)->nullable();
            $table->string('cabang', 60)->nullable();
            $table->string('bukti_donasi', 180)->nullable();
            $table->timestamps();

            $table->foreign('zakat_id')->references('id')->on('zakats');
            $table->foreign('rekening_id')->references('id')->on('rekenings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_zakat');
    }
}
