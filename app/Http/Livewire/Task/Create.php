<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Task;

class Create extends Component
{
    public $title;
    public $content;

    public function store()
    {
        $this->validate([
            'title'   => 'required',
            'content' => 'required',
        ]);

        $post = Task::create([
            'title'     => $this->title,
            'content'   => $this->content
        ]);

        //flash message
        session()->flash('message', 'Data Berhasil Disimpan.');

        //redirect
        return redirect()->route('task.index');
    }

    public function render()
    {
        return view('livewire.task.create');
    }
}
