<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Task;
use Illuminate\Http\Request;

class Edit extends Component
{
    public $taskId;
    public $title;
    public $content;

    public function mount($id)
    {
        $task = Task::find($id);

        if($task) {
            $this->taskId   = $task->id;
            $this->title    = $task->title;
            $this->content  = $task->content;
        }
    }

    public function update()
    {
        $this->validate([
            'title'   => 'required',
            'content' => 'required',
        ]);

        if($this->taskId) {

            $task = Task::find($this->taskId);

            if($task) {
                $task->update([
                    'title'     => $this->title,
                    'content'   => $this->content
                ]);
            }
        }

        //flash message
        session()->flash('message', 'Data Berhasil Diupdate.');

        //redirect
        return redirect()->route('task.index');
    }

    public function render()
    {
        return view('livewire.task.edit');
    }
}
