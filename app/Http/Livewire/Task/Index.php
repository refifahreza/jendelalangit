<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Task;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.task.index', [
            'tasks' => Task::latest()->paginate(5)
        ]);
    }

    public function destroy($taskId)
    {
        $task = task::find($taskId);

        if($task) {
            $task->delete();
        }

        //flash message
        session()->flash('message', 'Data Berhasil Dihapus.');

        //redirect
        return redirect()->route('task.index');

    }
}
