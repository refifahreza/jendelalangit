<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jendonasi;
use App\Models\Subdonasi;

class SubdonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $subdons = Subdonasi::where('subdon_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $subdons->appends(['search' => $search]);
        return view('admin.sub_donasi.index', compact('subdons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jendons = Jendonasi::where('aktif','Y')->orderBy('id','desc')->get();
        return view('admin.sub_donasi.add', compact('jendons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'jenis' => 'required',
        ]);

        $valid = Subdonasi::where('subdon_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.sub_donasi.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Sub Donasi : ' . $request->name  . ' karena Sub Donasi ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Subdonasi::create([
                'jendon_id' => $request->jenis,
                'subdon_name' => $request->name,
            ]);
            return redirect()->route('admin.sub_donasi.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Sub Donasi : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subdons = Subdonasi::findOrFail($id);
        $jendons = Jendonasi::where('aktif','Y')->orderBy('id','desc')->get();
        return view('admin.sub_donasi.edit', compact('subdons', 'jendons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'jenis' => 'required',
        ]);

        $create = Subdonasi::find($id)->update([
            'jendon_id' => $request->jenis,
            'subdon_name' => $request->name,
        ]);
        return redirect()->route('admin.sub_donasi.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Sub Donasi : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subdon = Subdonasi::findOrFail($id);
        $subdon->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses Menghapus Sub Donasi')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $subdon = Subdonasi::find($id);
        if ($mode == "true") {
            $subdon->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $subdon->aktif = 'N';
            // return 2;
        }
        $subdon->update();

        return response()->json($subdon, 200);
    }
}
