<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pesan;

class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $pesans = Pesan::where('nama', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $pesans->appends(['search' => $search]);
        return view('admin.pesan.index', compact('pesans'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pesan = Pesan::findOrFail($id);
        $pesan->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Pesan')->with('Class', 'success');
    }
}
