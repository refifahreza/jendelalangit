<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Katber;
use App\Models\Tag;
use Str;
use Auth;
use Storage;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $beritas = Berita::where('title', 'LIKE', "%$search%")->orderBy('id', 'desc')->paginate(10);
        $beritas->appends(['search' => $search]);
        return view('admin.beritas.index', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $katber = Katber::where('aktif', 'Y')->orderBy('id', 'desc')->get();
        $tags = Tag::get();
        return view('admin.beritas.add', compact('katber', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $request->validate([
            'judul' => 'required',
            'seo_judul' => 'required',
            'kategori' => 'required',
            'thumnail' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'deskripsi_gambar' => 'required',
            'content' => 'required',
            'deskripsi_meta' => 'required',
            'kutipan' => 'required',
        ]);

        $valid = Berita::where('title', '=', $request->judul)->count();
        if ($valid > 0) {
            return redirect()->route('admin.berita.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Berita : ' . $request->judul . ' karena Berita ' . $request->judul . ' sudah ada')->with('Class', 'warning');
        } else {
            $image = $request->file('thumnail');
            $image_extension = $image->extension();
            $image_name = time() . "_" . $image->getClientOriginalName();
            $image->storeAs('/images/berita', $image_name, 'public');
            $create = Berita::create([
                'title' => $request->judul,
                'title_slug' => Str::slug($request->judul),
                'seo_judul' => $request->seo_judul,
                'kategori_id' => $request->kategori,
                'author_id' => Auth::user()->id,
                'thumnail' => $image_name,
                'deskripsi_gambar' => $request->deskripsi_gambar,
                'content' => $request->content,
                'deskripsi_meta' => $request->deskripsi_meta,
                'kutipan' => $request->kutipan,
            ]);
            if (isset($request->tag)) {
                $create->tag()->sync($request->tag);
            }
            return redirect()->route('admin.berita.index')->with('header', 'Sukses')->with('message', 'Berhasil menambah Berita : ' . $request->judul)->with('Class', 'success');
        }
    }

    public function posted(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Katber::where('katber_name', '=', $request->name)->count();
        if ($valid > 0) {
            return redirect()->route('admin.berita.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Kategori Berita : ' . $request->name . 'karena sudah ada')->with('Class', 'warning');
        } else {
            $create = Katber::create([
                'katber_name' => $request->name,
            ]);
            return redirect()->route('admin.berita.create')->with('header', 'Sukses')->with('message', 'Berhasil menambah Kategori Berita : ' . $request->name)->with('Class', 'success');
        }
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

            $file->move(public_path('uploads'), $fileName);

            $ckeditor = $request->input('CKEditorFuncNum');
            $url = asset('uploads/' . $fileName);
            $msg = 'Upload image sukses!';

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            return $response;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beritas = Berita::findOrFail($id);
        return view('admin.beritas.show', compact('beritas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $beritas = Berita::findOrFail($id);
        $katber = Katber::where('aktif', 'Y')->orderBy('id', 'desc')->get();;
        $tags = Tag::get();
        return view('admin.beritas.edit', compact('beritas', 'katber', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|string',
            'seo_judul' => 'required',
            'kategori' => 'required',
            'thumnail' => 'nullable|file|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required',
            'kutipan' => 'required',
        ]);

        $beritas = Berita::findOrFail($id);
        $beritas->title = $request->judul;
        $beritas->kategori_id = $request->kategori;
        $beritas->title_slug = Str::slug($request->judul);
        $beritas->seo_judul = $request->seo_judul;
        $beritas->content = $request->content;
        $beritas->deskripsi_meta = $request->deskripsi_meta;
        $beritas->kutipan = $request->kutipan;
        $beritas->deskripsi_gambar = $request->deskripsi_gambar;
        if ($request->hasFile('thumnail')) {
            Storage::delete('public/images/berita/' . $beritas->thumnail);
            $image = $request->file('thumnail');
            $image_extension = $image->extension();
            $image_name = time() . "_" . $image->getClientOriginalName();
            $image->storeAs('/images/berita', $image_name, 'public');
            $beritas->thumnail = $image_name;
        }
        $beritas->save();
        if (isset($request->tag)) {
            $beritas->tag()->sync($request->tag);
        }

        return redirect()->route('admin.berita.index')->with('header', 'Sukses')->with('message', 'Berhasil mengubah Berita : ' . $request->judul)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beritas = Berita::findOrFail($id);
        Storage::delete('public/images/berita/' . $beritas->thumnail);
        $beritas->delete();

        return redirect()->back()->with('header', 'Sukses')->with('message', 'Berhasil menghapus Berita')->with('Class', 'danger');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $beritas = Berita::find($id);
        if ($mode == "true") {
            $beritas->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $beritas->aktif = 'N';
            // return 2;
        }
        $beritas->update();

        return response()->json($beritas, 200);
    }
}
