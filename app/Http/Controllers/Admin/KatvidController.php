<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Katvid;
use Str;

class KatvidController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('search');
        $katvids = Katvid::where('kategori_video', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $katvids->appends(['search' => $search]);
        return view('admin.kategori_video.index', compact('katvids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori_video.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Katvid::where('kategori_video', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.kategori_video.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Kategori Video : ' . $request->name . ' karena Kategori Video ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Katvid::create([
                'kategori_video' => $request->name,
                'katvid_slug' => Str::slug($request->name),
            ]);
            return redirect()->route('admin.kategori_video.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Kategori Video : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $katvids = Katvid::findOrFail($id);
        return view('admin.kategori_video.edit', compact('katvids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $create = Katvid::find($id)->update([
            'kategori_video' => $request->name,
            'katvid_slug' => Str::slug($request->name),
        ]);
        return redirect()->route('admin.kategori_video.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Kategori Video : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $katvid = Katvid::findOrFail($id);
        $katvid->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Kategori Video')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $katvid = Katvid::find($id);
        if ($mode == "true") {
            $katvid->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $katvid->aktif = 'N';
            // return 2;
        }
        $katvid->update();

        return response()->json($katvid, 200);
    }
}
