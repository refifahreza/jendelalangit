<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Katpro;
use Storage;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $produks = Produk::where('produk_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(3);
        $produks->appends(['search' => $search]);
        return view('admin.produk.index', compact('produks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $katpros = Katpro::all();
        return view('admin.produk.add', compact('katpros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'katpro' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/produk', $image_name, 'public');
            $create = Produk::create([
                'katpro_id' => $request->katpro,
                'foto' => $image_name,
                'aktif' => 'ya'
            ]);
            return redirect()->route('admin.produk.index')->with('header', 'Sukses!')->with('message', 'Sukses Menambah Produk!')->with('Class', 'success');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produks = Produk::findOrFail($id);
        $katpros = Katpro::all();
        return view('admin.produk.edit', compact('produks', 'katpros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'katpro' => 'required',
            'image' => 'nullable|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $produk = Produk::findOrFail($id);
        $produk->katpro_id = $request->katpro;
        if($request->hasFile('image')) {
            Storage::delete('public/images/produk/'.$produk->foto);
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/produk', $image_name, 'public');
            $produk->foto = $image_name;
        }
        $produk->save();
        return redirect()->route('admin.produk.index')->with('header', 'Sukses!')->with('message', 'Sukses Mengubah Produk!')->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id);
        Storage::delete('public/images/produk/'.$produk->foto);
        $produk->delete();
        return redirect()->back()->with('header', 'Sukses!')->with('message', 'Sukses Menghapus Data!')->with('Class', 'success');
    }
}
