<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Album;
use App\Models\Rekening;
use App\Models\Slider;
use App\Models\Mitra;
use App\Models\Donline;
use App\Models\Donman;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $berita = Berita::count();
       $beritas = Berita::all();
       $album = Album::count();
       $albums = Album::all();
       $mitra = Mitra::count();
       $mitras = Mitra::all();
       $rekening = Rekening::count();
       $rekenings = Rekening::all();
       $slider = Slider::count();
       $sliders = Slider::all();

       $sum0 = Donline::where('status','=','success')->sum('jumlah');
       $sum1 = Donman::where('status','=','sukses')->sum('jumlah');
       $count = $sum0 + $sum1;

       return view('admin.index', compact('berita', 'beritas', 'album', 'albums', 'mitra', 'mitras','rekening', 'rekenings','slider', 'sliders', 'count', 'sum0', 'sum1'));
   }

   public function berita(Request $request)
   {
        $berita = [];
        if ($request->ajax()) {
                // $berita = berita::find($request->id);
        $berita = Berita::where('id', $request->id)->get();

        return response()->json($berita);
        }
    }

    public function album(Request $request)
    {
        $album = [];
        if ($request->ajax()) {
                // $album = album::find($request->id);
            $album = Album::where('id', $request->id)->get();

            return response()->json($album);
        }
    }

    public function mitra(Request $request)
    {
        $mitra = [];
        if ($request->ajax()) {
                // $mitra = mitra::find($request->id);
            $mitra = Mitra::where('id', $request->id)->get();

            return response()->json($mitra);
        }
    }

    public function slider(Request $request)
    {
        $slider = [];
        if ($request->ajax()) {
                // $slider = slider::find($request->id);
            $slider = Slider::where('id', $request->id)->get();

            return response()->json($slider);
        }
    }

    public function rekening(Request $request)
    {
        $rekening = [];
        if ($request->ajax()) {
                // $rekening = rekening::find($request->id);
            $rekening = Rekening::where('id', $request->id)->get();

            return response()->json($rekening);
        }
    }
}
