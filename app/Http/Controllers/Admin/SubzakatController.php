<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zakat;
use App\Models\Subzakat;

class SubzakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $subzakats = Subzakat::where('nama_sub_zakat', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $subzakats->appends(['search' => $search]);
        return view('admin.sub_zakat.index', compact('subzakats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zakats = Zakat::where('aktif','Y')->orderBy('id','desc')->get();
        return view('admin.sub_zakat.add', compact('zakats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'zakat' => 'required',
        ]);

        $valid = Subzakat::where('nama_sub_zakat', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.sub-zakat.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Sub Zakat : ' . $request->name . ' karena Sub Zakat ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Subzakat::create([
                'zakat_id' => $request->zakat,
                'nama_sub_zakat' => $request->name,
                'rumus_pertama' => $request->rumus_pertama,
                'rumus_kedua' => $request->rumus_kedua,
                'rumus_ketiga' => $request->rumus_ketiga,
            ]);
            return redirect()->route('admin.sub-zakat.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Sub Zakat : ' . $request->name)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subzakats = Subzakat::findOrFail($id);
        $zakats = Zakat::where('aktif','Y')->orderBy('id','desc')->get();
        return view('admin.sub_zakat.edit', compact('subzakats', 'zakats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'zakat' => 'required',
        ]);

        $create = Subzakat::find($id)->update([
            'zakat_id' => $request->zakat,
            'nama_sub_zakat' => $request->name,
            'rumus_pertama' => $request->rumus_pertama,
            'rumus_kedua' => $request->rumus_kedua,
            'rumus_ketiga' => $request->rumus_ketiga,
        ]);
        return redirect()->route('admin.sub-zakat.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Sub Donasi : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subzakat = Subzakat::findOrFail($id);
        $subzakat->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Sub Zakat')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $subzakat = Subzakat::find($id);
        if ($mode == "true") {
            $subzakat->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $subzakat->aktif = 'N';
            // return 2;
        }
        $subzakat->update();

        return response()->json($subzakat, 200);
    }
}
