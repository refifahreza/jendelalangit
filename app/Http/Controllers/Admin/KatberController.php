<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Katber;
use App\Models\Berita;
use Str;

class KatberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $katbers = Katber::where('katber_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $katbers->appends(['search' => $search]);
        return view('admin.kategori_berita.index', compact('katbers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori_berita.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Katber::where('katber_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.kategori_berita.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Kategori Berita : ' . $request->name . ' karena Kategori Berita ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Katber::create([
                'katber_name' => $request->name,
                'katber_slug' => Str::slug($request->name),
            ]);
            return redirect()->route('admin.kategori_berita.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Kategori Berita : ' . $request->name)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $katbers = Katber::findOrFail($id);
        return view('admin.kategori_berita.edit', compact('katbers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $create = Katber::find($id)->update([
            'katber_name' => $request->name,
            'katber_slug' => Str::slug($request->name),
        ]);
        return redirect()->route('admin.kategori_berita.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Kategori Berita : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $katber = Katber::findOrFail($id);
        $berita = Berita::where('kategori_id', $katber->id)->first();
        if($berita) {
            return redirect()->bak()->with('header', 'Gagal')->with('message', 'Gagal menghapus Kategori Berita karena masih digunakan pada Berita')->with('Class', 'warning');
       } else {
           $katber->delete();
           return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Kategori Berita')->with('Class', 'success');
       }
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $katber = Katber::find($id);
        if ($mode == "true") {
            $katber->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $katber->aktif = 'N';
            // return 2;
        }
        $katber->update();

        return response()->json($katber, 200);
    }
}
