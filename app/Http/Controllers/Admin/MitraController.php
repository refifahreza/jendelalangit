<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mitra;
use Storage;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $mitras = Mitra::where('mitra_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $mitras->appends(['search' => $search]);
        return view('admin.mitra.index', compact('mitras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mitra.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $valid = Mitra::where('mitra_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.mitra.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Mitra : ' . $request->name . ' karena Mitra ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/mitra', $image_name, 'public');
            $create = Mitra::create([
                'mitra_name' => $request->name,
                'mitra_logo' => $image_name,
            ]);
            return redirect()->route('admin.mitra.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Mitra : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mitras = Mitra::findOrFail($id);
        return view('admin.mitra.edit', compact('mitras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'nullable|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $mitra = Mitra::findOrFail($id);
        $mitra->mitra_name = $request->name;
        if($request->hasFile('image')) {
            Storage::delete('public/images/mitra/'.$mitra->mitra_logo);
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/mitra', $image_name, 'public');
            $mitra->mitra_logo = $image_name;
        }
        $mitra->save();
        return redirect()->route('admin.mitra.index')->with('header', 'Sukses')->with('message', 'Sukses Menambah Mitra : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mitra = Mitra::findOrFail($id);
        Storage::delete('public/images/mitra/'.$mitra->mitra_logo);
        $mitra->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Mitra')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $mitra = Mitra::find($id);
        if ($mode == "true") {
            $mitra->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $mitra->aktif = 'N';
            // return 2;
        }
        $mitra->update();

        return response()->json($mitra, 200);
    }
}
