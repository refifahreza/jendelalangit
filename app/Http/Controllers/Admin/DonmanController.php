<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donman;
use App\Exports\DonmanExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Storage;

class DonmanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->get('search')) {
            $search = $request->get('search');
            $donmans = Donman::where('status', '=', $search)->orderBy('id', 'desc')->paginate(10);
            $let = str_replace(' ', '%20', config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>', '', $let);
            $b = str_replace('</p>', '', $a);
            $c = str_replace('&nbsp;', '%0A', $b);
            $donmans->appends(['search' => $search]);
            $tahun_sekarang = Carbon::now()->format('Y');
            $tahun = request()->input('tahun') ?: $tahun_sekarang;
            $bulan_sekarang = Carbon::now()->format('F');
            $bulan = request()->get('bulan') ?: $bulan_sekarang;
            return view('admin.donman.index', compact('donmans', 'c', 'tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
        } else {
            $let = str_replace(' ', '%20', config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>', '', $let);
            $b = str_replace('</p>', '', $a);
            $c = str_replace('&nbsp;', '%0A', $b);
            $donmans = Donman::orderBy('id', 'desc')->paginate(10);
            $tahun_sekarang = Carbon::now()->format('Y');
            $tahun = request()->input('tahun') ?: $tahun_sekarang;
            $bulan_sekarang = Carbon::now()->format('F');
            $bulan = request()->get('bulan') ?: $bulan_sekarang;
            return view('admin.donman.index', compact('donmans', 'c', 'tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
        }
    }

    public function show($kode_donasi)
    {
        $donmans = Donman::where('kode_donasi', '=', $kode_donasi)->first();
        $ceks = Donman::where('kode_donasi', '=', $kode_donasi)->whereNull('bank_donatur')->first();
        return view('admin.donman.show', compact('donmans', 'ceks'));
    }

    public function change(Request $request, $kode_donasi)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $create = Donman::where('kode_donasi', $kode_donasi)->update([
            'status' => $request->status,

        ]);
        return redirect()->route('admin.donasi_manual.index')->with('header', 'Sukses!')->with('message', 'Sukses mengubah Status : ' . $request->status)->with('Class', 'warning');
    }

    public function export()
    {
        $donmans = Donman::orderBy('id', 'desc')->get();
        $sum1 = Donman::where('status', '=', 'sukses')->sum('jumlah');
        $tahun_sekarang = Carbon::now()->format('Y');
        $tahun = request()->input('tahun') ?: $tahun_sekarang;
        $bulan_sekarang = Carbon::now()->format('F');
        $bulan = request()->get('bulan') ?: $bulan_sekarang;
        return view('admin.donman.export', compact('donmans', 'sum1', 'tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
    }

    public function destroy($kode_donasi)
    {
        $donman = Donman::whereKodeDonasi($kode_donasi)->first();
        $donman->delete();
        Storage::delete('public/images/buktied/' . $donman->bukti_donasi);
        return redirect()->back()->with('header', 'Sukses!')->with('message', 'Sukses menghapus Data')->with('Class', 'success');
    }
}
