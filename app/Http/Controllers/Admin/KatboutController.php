<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Katbout;
use Str;
use Storage;

class KatboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $katbouts = Katbout::where('jenis_tentang', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $katbouts->appends(['search' => $search]);
        return view('admin.katbout.index', compact('katbouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.katbout.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis_tentang' => 'required',
            'image' => 'file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $type = request()->input('type');
        $image = $request->file('image');

        $valid = Katbout::where('jenis_tentang', '=', $request->jenis_tentang)->count();
        if($valid > 0) {
            return redirect()->route('admin.katbout.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Tentang Kami : ' . $request->name . ' karena Tentang Kami ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } elseif (isset($type)) {
            $type = 1;
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/katbout', $image_name, 'public');
            $create = Katbout::create([
                'jenis_tentang' => $request->jenis_tentang,
                'type' => 1,
                'slug' => Str::slug($request->jenis_tentang),
                'gambar' => $image_name,
                'aktif' => 'Y',
            ]);
        } else {
            $create = Katbout::create([
                'jenis_tentang' => $request->jenis_tentang,
                'slug' => Str::slug($request->jenis_tentang),
                'konten' => $request->konten,
                'aktif' => 'Y',
            ]);
        }
        return redirect()->route('admin.katbout.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Tentang Kami : ' . $request->jenis_tentang)->with('Class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $katbouts = Katbout::findOrFail($id);
        return view('admin.katbout.edit', compact('katbouts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis_tentang' => 'required',
            'image'         => 'file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $type = request()->input('type');
        $foto = $request->file('foto');

        $katbout = Katbout::findOrFail($id);
        $katbout->jenis_tentang = $request->jenis_tentang;
        $katbout->slug = Str::slug($request->jenis_tentang);

        if (isset($type)) {
            $type = 1;
            $katbout->konten = NULL;
            if($request->hasFile('image')) {
                Storage::delete('public/images/katbout/'.$katbout->gambar);
                $image = $request->file('image');
                $image_extension = $image->extension();
                $image_name = time()."_".$image->getClientOriginalName();
                $image->storeAs('/images/katbout', $image_name, 'public');
                $katbout->gambar = $image_name;
            }
        } else{
            $request->validate([
                'konten' => 'required',
            ]);
            $katbout->konten = $request->konten;
            $katbout->gambar = NULL;
            $type = 0;
            Storage::delete('public/images/katbout/'.$katbout->gambar);
        }

        $katbout->type = $type;
        $katbout->update();
        return redirect()->route('admin.katbout.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Tentang Kami : ' . $request->jenis_tentang)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $katbout = Katbout::findOrFail($id);
        $katbout->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Data')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $katbout = Katbout::find($id);
        if ($mode == "true") {
            $katbout->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $katbout->aktif = 'N';
            // return 2;
        }
        $katbout->update();

        return response()->json($katbout, 200);
    }
}
