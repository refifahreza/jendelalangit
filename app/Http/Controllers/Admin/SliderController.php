<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $sliders = Slider::where('header_text', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $sliders->appends(['search' => $search]);
        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'header_text' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $valid = Slider::where('header_text', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.slider.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Slider : ' . $request->header_text . ' karena Slider ' . $request->header_text . ' sudah ada')->with('Class', 'warning');
        } else {
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/slider', $image_name, 'public');
            $create = Slider::create([
                'header_text' => $request->header_text,
                'picture' => $image_name,
            ]);
            return redirect()->route('admin.slider.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Slider : ' . $request->header_text)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sliders = Slider::findOrFail($id);
        return view('admin.slider.edit', compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'header_text' => 'required',
            'thumnail' => 'nullable|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $slider = Slider::findOrFail($id);
        $slider->header_text = $request->header_text;
        if($request->hasFile('thumnail')) {
            Storage::delete('public/images/slider/'.$slider->picture);
            $image = $request->file('thumnail');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/slider', $image_name, 'public');
            $slider->picture = $image_name;
        }
        $slider->save();
        return redirect()->route('admin.slider.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Slider : ' . $request->header_text)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        Storage::delete('public/images/slider/'.$slider->picture);
        $slider->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Slider')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $slider = Slider::find($id);
        if ($mode == "true") {
            $slider->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $slider->aktif = 'N';
            // return 2;
        }
        $slider->update();

        return response()->json($slider, 200);
    }
}
