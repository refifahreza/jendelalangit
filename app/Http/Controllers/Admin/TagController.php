<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use Str;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cari = request()->get('cari') ?: '';
        $tags = Tag::where('nama_tag', 'like', '%' . $cari . '%')->paginate(10);
        return view('admin.tag.index', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama',
        ];

        $this->validate($request, [
            'nama_tag'       => 'required',
        ], $messages);

        $tag = new Tag;
        $tag->nama_tag = $request->nama_tag;
        $tag->slug = Str::slug($request->nama_tag);
        $tag->save();
        return redirect()->route('admin.tag.index')->with('header', 'Berhasil!')->with('message', 'Berhasil Menambah Tag!')->with('Class', 'success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama',
        ];

        $this->validate($request, [
            'nama_tag'       => 'required',
        ], $messages);

        $tag = Tag::find($id);
        $tag->nama_tag = $request->nama_tag;
        $tag->slug = Str::slug($request->nama_tag);
        $tag->update();
        return redirect()->route('admin.tag.index')->with('header', 'Berhasil!')->with('message', 'Berhasil Mengubah Tag!')->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect()->route('admin.tag.index')->with('header', 'Berhasil!')->with('message', 'Berhasil Menghapus Tag!')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $tag = Tag::find($id);
        if ($mode == "true") {
            $tag->aktif = 'Y';
        // return 1;
        } elseif ($mode == "false") {
            $tag->aktif = 'N';
            // return 2;
        }
        $tag->update();
        
        return response()->json($tag, 200);
    }
}
