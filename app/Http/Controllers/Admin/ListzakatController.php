<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Listzakat;
use App\Exports\ListzakatExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Storage;

class ListzakatController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('search')) {
            $search = $request->get('search');
            $list_zakats = Listzakat::where('status','=',$search)->orderBy('id','desc')->paginate(10);
            $let = str_replace(' ','%20',config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>','',$let);
            $b = str_replace('</p>','',$a);
            $c = str_replace('&nbsp;','%0A',$b);
            $list_zakats->appends(['search' => $search]);
            $tahun_sekarang = Carbon::now()->format('Y');
            $tahun = request()->input('tahun') ?: $tahun_sekarang;
            $bulan_sekarang = Carbon::now()->format('F');
            $bulan = request()->get('bulan') ?: $bulan_sekarang;
            return view('admin.list_zakat.index', compact('list_zakats','c','tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
        } else {
            $let = str_replace(' ','%20',config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>','',$let);
            $b = str_replace('</p>','',$a);
            $c = str_replace('&nbsp;','%0A',$b);
            $list_zakats = Listzakat::orderBy('id','desc')->paginate(10);
            $tahun_sekarang = Carbon::now()->format('Y');
            $tahun = request()->input('tahun') ?: $tahun_sekarang;
            $bulan_sekarang = Carbon::now()->format('F');
            $bulan = request()->get('bulan') ?: $bulan_sekarang;
            return view('admin.list_zakat.index', compact('list_zakats', 'c','tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
        }
    }

    public function show($kode_zakat)
    {
        $list_zakats = Listzakat::where('kode_zakat','=',$kode_zakat)->first();
        $ceks = Listzakat::where('kode_zakat','=',$kode_zakat)->whereNull('bank_donatur')->first();
        return view('admin.list_zakat.show', compact('list_zakats', 'ceks'));
    }

    public function change(Request $request, $kode_zakat)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $create = Listzakat::where('kode_zakat',$kode_zakat)->update([
            'status' => $request->status,

        ]);
        return redirect()->route('admin.list_zakat.index')->with('header', 'Sukses')->with('message', 'Sukses ubah Status : ' . $request->status)->with('Class', 'warning');
    }

    public function export()
    {
        $list_zakats = Listzakat::orderBy('id','desc')->get();
        $sum1 = Listzakat::where('status','=','sukses')->sum('jumlah');
        $tahun_sekarang = Carbon::now()->format('Y');
        $tahun = request()->input('tahun') ?: $tahun_sekarang;
        $bulan_sekarang = Carbon::now()->format('F');
        $bulan = request()->get('bulan') ?: $bulan_sekarang;
        return view('admin.list_zakat.export', compact('list_zakats', 'sum1','tahun_sekarang', 'tahun', 'bulan', 'bulan_sekarang'));
    }

    public function destroy($kode_zakat)
    {
        $list_zakat = Listzakat::whereKodeZakatt($kode_zakat)->first();
        $list_zakat->delete();
        // Storage::delete('public/images/buktied/'.$list_zakat->bukti_donasi);
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Zakat')->with('Class', 'success');
    }
}
