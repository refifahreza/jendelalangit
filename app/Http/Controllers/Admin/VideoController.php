<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Katvid;
use App\Models\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cari = request()->get('cari') ?: '';
        $katvids = Katvid::get();
        $videos = Video::where('judul_video', 'like', '%' . $cari . '%')->paginate(10);
        return view('admin.video.index', compact('videos', 'katvids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'judul_video' => 'required',
            'link' => 'required',
            'katvid_id' => 'required',
        ]);

        $valid = Video::where('judul_video', '=', $request->judul_video)->count();
        if($valid > 0) {
            return redirect()->route('admin.video.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Video : ' . $request->name . ' karena Video ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Video::create([
                'judul_video' => $request->judul_video,
                'link' => $request->link,
                'katvid_id' => $request->katvid_id,
            ]);
            return redirect()->route('admin.video.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Video : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul_video' => 'required',
            'link' => 'required',
            'katvid_id' => 'required',
        ]);

        $create = Video::find($id)->update([
            'judul_video' => $request->judul_video,
            'link' => $request->link,
            'katvid_id' => $request->katvid_id,
        ]);
        return redirect()->route('admin.video.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Video : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);
        $video->delete();
        return redirect()->route('admin.video.index')->with('header', 'Berhasil')->with('message', 'Berhasil menghapus Video')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $video = Video::find($id);
        // return $video;
        if ($mode == "true") {
            $video->aktif = 'Y';
        // return 1;
        } elseif ($mode == "false") {
            $video->aktif = 'N';
            // return 2;
        }
        $video->update();

        return response()->json($video, 200);
    }
}
