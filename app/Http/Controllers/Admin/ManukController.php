<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Str;
use Hash;

class ManukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $akuns = User::where('name', 'LIKE',"%$search%")->orWhere('username', 'LIKE',"%$search%")->orWhere('email', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $akuns->appends(['search' => $search]);
        return view('admin.manajemen_akun.index', compact('akuns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manajemen_akun.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'username' => 'required|max:30',
            'password' => 'required',
            'roles' => 'required',
        ]);

        $valid = User::where('username', '=', $request->username)->count();
        if($valid > 0) {
            return redirect()->route('admin.manajemen_akun.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Akun : ' . $request->name . ' karena Akun ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            if(!empty($request->file('icon'))){
                $image = $request->file('icon');
                $image_extension = $image->extension();
                $image_name = time() . "_" . $image->getClientOriginalName();
                $image->storeAs('/images/user_icon', $image_name, 'public');
                $create = User::create([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'roles' => $request->roles,
                    'icon' => $image_name,
                ]);
            } else{
                $create = User::create([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'roles' => $request->roles,
                ]);
            }
            return redirect()->route('admin.manajemen_akun.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Akun : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $akuns = User::findOrFail($id);
        return view('admin.manajemen_akun.edit', compact('akuns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'username' => 'required|max:30',
            'password' => 'nullable',
            'roles' => 'required',
        ]);

        if(empty($request->password)) {
            $image = $request->file('icon');
            $image_extension = $image->extension();
            $image_name = time() . "_" . $image->getClientOriginalName();
            $image->storeAs('/images/user_icon', $image_name, 'public');
            $create = User::where('id',$id)->update([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'roles' => $request->roles,
                'icon' => $image_name,
            ]);
            return redirect()->route('admin.manajemen_akun.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Akun : ' . $request->name . ' tanpa mengubah Password')->with('Class', 'success');
            } else {
                if(!empty($request->file('icon'))){
                    $image = $request->file('icon');
                    $image_extension = $image->extension();
                    $image_name = time() . "_" . $image->getClientOriginalName();
                    $image->storeAs('/images/user_icon', $image_name, 'public');
                    $create = User::where('id',$id)->update([
                        'name' => $request->name,
                        'username' => $request->username,
                        'email' => $request->email,
                        'password' => Hash::make($request->password),
                        'roles' => $request->roles,
                         'icon' => $image_name,
                    ]);
                }else{
                    $create = User::where('id',$id)->update([
                        'name' => $request->name,
                        'username' => $request->username,
                        'email' => $request->email,
                        'password' => Hash::make($request->password),
                        'roles' => $request->roles,
                         'icon' => $image_name,
                    ]);
                }
                return redirect()->route('admin.manajemen_akun.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Akun : ' . $request->name . ' dengan mengubah Password')->with('Class', 'success');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $akuns = User::findOrFail($id);
        $akuns->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Akun')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $akuns = User::find($id);
        if ($mode == "true") {
            $akuns->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $akuns->aktif = 'N';
            // return 2;
        }
        $akuns->update();

        return response()->json($akuns, 200);
    }
}
