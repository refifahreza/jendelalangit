<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\Album;
use Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $album_id)
    {
        $search = $request->get('search');
        $gallerys = Gallery::where('album_id', $album_id)->where('album_id', 'LIKE',"%$search%")->paginate(10);
        $gallerys->appends(['search' => $search]);

        return view('admin.gallery.index', compact('gallerys','album_id'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($album_id)
    {
        $gallery = Gallery::where('album_id', $album_id)->get();
        return view('admin.gallery.add', compact('gallery', 'album_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $album_id)
    {
        // return $album_id;
        $request->validate([
            'name' => 'required',
            'foto' => 'required|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

      $valid = Gallery::where('nama_foto', '=', $request->name)->count();
      if($valid > 0) {
        return redirect()->route('admin.gallery.index', $album_id)->with('header', 'Gagal')->with('message', 'Gagal menambah Galeri : ' . $request->judul . ' karena Galeri ' . $request->judul . ' sudah ada')->with('Class', 'warning');
    } else {
        $image = $request->file('foto');
        $image_extension = $image->extension();
        $image_name = time()."_".$image->getClientOriginalName();
        // return $image_name;
        $image->storeAs('/images/gallery', $image_name, 'public');
        $create = Gallery::create([
            'nama_foto' => $request->name,
            'foto' => $image_name,
            'album_id' => $album_id
        ]);
        return redirect()->route('admin.gallery.index', $album_id)->with('header', 'Sukses')->with('message', 'Berhasil menambah Galeri : ' . $request->name)->with('Class', 'success');
    }
}



    public function change(Request $request)
    {
        $gallery = Gallery::find($request->id);
        $gallery->aktif = $request->aktif;
        $gallery->save();
        return redirect()->route('admin.album.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Status Foto')->with('Class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallerys = Gallery::findOrFail($id);
        // $gallery = Gallery::where('album_id', $album_id)->get();
        $albums = Album::where('aktif', 'Y')->get();
        return view('admin.gallery.edit', compact('gallerys', 'albums', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'nullable|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $gallery = Gallery::findOrFail($id);
        $gallery->nama_foto = $request->name;
        if($request->hasFile('image')) {
            Storage::delete('public/images/gallery/'.$gallery->foto);
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/gallery', $image_name, 'public');
            $gallery->foto = $image_name;
        }
        $gallery->save();
        return redirect()->route('admin.gallery.index', $gallery->album_id)->with('header', 'Sukses')->with('message', 'Sukses mengubah Galeri : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $album_id=null)
    {
        $gallery = Gallery::findOrFail($id);
        Storage::delete('public/images/gallery/'.$gallery->foto);
        $gallery->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Galeri')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $gallery = Gallery::find($id);
        if ($mode == "true") {
            $gallery->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $gallery->aktif = 'N';
            // return 2;
        }
        $gallery->update();

        return response()->json($gallery, 200);
    }
}
