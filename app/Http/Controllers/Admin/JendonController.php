<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jendonasi;
use App\Models\Subdonasi;

class JendonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $jendons = Jendonasi::where('jendon_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $jendons->appends(['search' => $search]);
        return view('admin.jenis_donasi.index', compact('jendons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jenis_donasi.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Jendonasi::where('jendon_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.jenis_donasi.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Jenis Donasi : ' . $request->name  . ' karena Jenis Donasi ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Jendonasi::create([
                'jendon_name' => $request->name,
            ]);
            return redirect()->route('admin.jenis_donasi.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Jenis Donasi : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jendons = Jendonasi::findOrFail($id);
        return view('admin.jenis_donasi.edit', compact('jendons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $create = Jendonasi::find($id)->update([
            'jendon_name' => $request->name,
        ]);
        return redirect()->route('admin.jenis_donasi.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Jenis Donasi : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jendon = Jendonasi::findOrFail($id);
        $subdon = Subdonasi::where('jendon_id', $jendon->id)->first();
        if($subdon) {
            return redirect()->back()->with('header', 'Gagal')->with('message', 'Gagal menghapus Jenis Donasi karena masih digunakan pada Sub Donasi')->with('Class', 'warning');
       } else {
           $jendon->delete();
           return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Jenis Donasi')->with('Class', 'success');
       }
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $jendon = Jendonasi::find($id);
        if ($mode == "true") {
            $jendon->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $jendon->aktif = 'N';
            // return 2;
        }
        $jendon->update();

        return response()->json($jendon, 200);
    }
}
