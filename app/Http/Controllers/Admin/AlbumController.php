<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Gallery;
use Storage;
use Illuminate\Support\Str;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $albums = Album::where('album_name', 'LIKE', "%$search%")->orderBy('id', 'desc')->paginate(10);
        $albums->appends(['search' => $search]);
        return view('admin.album.index', compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.album.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $valid = Album::where('album_name', '=', $request->name)->count();
        if ($valid > 0) {
            return redirect()->route('admin.album.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Album : ' . $request->name . ' karena Album ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time() . "_" . $image->getClientOriginalName();
            $image->storeAs('/images/album', $image_name, 'public');
            $create = Album::create([
                'album_name' => $request->name,
                'album_slug' => Str::slug($request->name),
                'thumbnail' => $image_name,
            ]);
            return redirect()->route('admin.album.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Album : ' . $request->name)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $albums = Album::findOrFail($id);
        return view('admin.album.edit', compact('albums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'nullable|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $album = Album::findOrFail($id);
        $album->album_name = $request->name;
        $album->album_slug = Str::slug($request->name);
        if ($request->hasFile('image')) {
            Storage::delete('public/images/album/' . $album->thumbnail);
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time() . "_" . $image->getClientOriginalName();
            $image->storeAs('/images/album', $image_name, 'public');
            $album->thumbnail = $image_name;
        }
        $album->save();
        return redirect()->route('admin.album.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Album : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        $gallery = Gallery::where('album_id', $album->id)->first();
        if ($gallery) {
            return redirect()->back()->with('header', 'Gagal')->with('message', 'Gagal menghapus Album karena masih digunakan pada Gallery')->with('Class', 'warning');
        } else {
            $album->delete();
            Storage::delete('public/images/album/' . $album->thumbnail);
            return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Album')->with('Class', 'success');
        }
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $album = Album::find($id);
        if ($mode == "true") {
            $album->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $album->aktif = 'N';
            // return 2;
        }
        $album->update();

        return response()->json($album, 200);
    }
}
