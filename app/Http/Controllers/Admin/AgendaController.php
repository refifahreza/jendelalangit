<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Str;
use App\Models\Agenda;
use Carbon\Carbon;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $agendas = Agenda::where('judul', 'LIKE', "%$search%")->orderBy('id', 'desc')->paginate(10);
        $agendas->appends(['search' => $search]);
        return view('admin.agenda.index', compact('agendas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agenda.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'lokasi' => 'required',
            'pengajar' => 'required',
            'penyelenggara' => 'required',
            'isi_agenda' => 'required',
            'waktu_awal' => 'required',
            'waktu_akhir' => 'required',
        ]);

        $valid = Agenda::where('judul', '=', $request->judul)->count();
        if ($valid > 0) {
            return redirect()->route('admin.agenda.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Agenda : ' . $request->judul . ' karena Agenda ' . $request->judul . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Agenda::create([
                'judul' => $request->judul,
                'lokasi' => $request->lokasi,
                'slug' => Str::slug($request->judul),
                'pengajar' => $request->pengajar,
                'penyelenggara' => $request->penyelenggara,
                'isi_agenda' => $request->isi_agenda,
                'waktu_awal' => date("Y-m-d-H:i", strtotime(request()->input('waktu_awal'))),
                'waktu_akhir' => date("Y-m-d-H:i", strtotime(request()->input('waktu_akhir')))
            ]);
            return redirect()->route('admin.agenda.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Agenda : ' . $request->judul)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agendas = Agenda::findOrFail($id);
        return view('admin.agenda.edit', compact('agendas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'lokasi' => 'required',
            'pengajar' => 'required',
            'penyelenggara' => 'required',
            'isi_agenda' => 'required',
        ]);

        $create = Agenda::find($id)->update([
            'judul' => $request->judul,
            'lokasi' => $request->lokasi,
            'slug' => Str::slug($request->judul),
            'pengajar' => $request->pengajar,
            'penyelenggara' => $request->penyelenggara,
            'isi_agenda' => $request->isi_agenda,
            'waktu_awal' => date("Y-m-d-H:i", strtotime(request()->input('waktu_awal'))),
            'waktu_akhir' => date("Y-m-d-H:i", strtotime(request()->input('waktu_akhir')))
        ]);
        return redirect()->route('admin.agenda.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Agenda : ' . $request->judul)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agendas = Agenda::findOrFail($id);
        $agendas->delete();

        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses Menghapus Agenda')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $agendas = Agenda::find($id);
        if ($mode == "true") {
            $agendas->aktif = 'Y';
        } elseif ($mode == "false") {
            $agendas->aktif = 'N';
        }
        $agendas->update();
        return response()->json($agendas, 200);
    }
}
