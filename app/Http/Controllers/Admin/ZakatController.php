<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zakat;
use App\Models\Subzakat;

class ZakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $zakats = Zakat::where('nama_zakat', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $zakats->appends(['search' => $search]);
        return view('admin.zakat.index', compact('zakats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.zakat.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Zakat::where('nama_zakat', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.zakat.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Jenis Zakat : ' . $request->name  . ' karena Jenis Zakat ' . $request->name . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Zakat::create([
                'nama_zakat' => $request->name,
            ]);
            return redirect()->route('admin.zakat.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Jenis Zakat : ' . $request->name)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zakats = Zakat::findOrFail($id);
        return view('admin.zakat.edit', compact('zakats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $create = Zakat::find($id)->update([
            'nama_zakat' => $request->name,
        ]);
        return redirect()->route('admin.zakat.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Jenis Zakat : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zakat = Zakat::findOrFail($id);
        $subdon = Subzakat::where('zakat_id', $zakat->id)->first();
        if($subdon) {
            return redirect()->back()->with('header', 'Gagal')->with('message', 'Gagal menghapus Jenis Zakat karena masih digunakan pada Sub Zakat')->with('Class', 'warning');
       } else {
           $zakat->delete();
           return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Jenis Zakat')->with('Class', 'success');
       }
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $zakat = Zakat::find($id);
        if ($mode == "true") {
            $zakat->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $zakat->aktif = 'N';
            // return 2;
        }
        $zakat->update();

        return response()->json($zakat, 200);
    }
}
