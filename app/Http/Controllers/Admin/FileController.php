<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use App\Models\Files;
use Str;
use File;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->get('search');
        $files = Files::where('judul', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $files->appends(['search' => $search]);
        return view('admin.file.index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.file.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'             => 'required',
            'file'              => 'required|file|mimes:jpeg,bmp,png,jpg,gif,zip,rar,pdf,doc,docx,ppt,pptx,xls,xlsx,txt,mp3,mp4|max:8192',
            'keterangan_file'   => 'required',
        ]);

        $valid = Files::where('judul', '=', $request->judul)->count();
        if($valid > 0) {
            return redirect()->route('admin.file.index')->with('header', 'Gagal')->with('message', 'Gagal menambah File : ' . $request->judul . ' karena File ' . $request->judul . ' sudah ada')->with('Class', 'warning');
        } else {
            $nama_file = Str::slug($request->judul) . '-' . uniqid() . '.' . $request->file('file')->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('file')) {
                Storage::disk('public')->makeDirectory('file');
            }
            Storage::disk('public')->put('file/' . $nama_file, File::get($request->file('file')));

            $create = Files::create([
                'judul' => $request->judul,
                'slug' => Str::slug($request->judul),
                'file' => $nama_file,
                'keterangan_file' => $request->keterangan_file,
                'user_id' => auth()->user()->id,
            ]);
            return redirect()->route('admin.file.index')->with('header', 'Sukses')->with('message', 'Sukses menambah File : ' . $request->judul)->with('Class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = Files::findOrFail($id);
        return view('admin.file.edit', compact('file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'judul'             => 'required',
            'file'              => 'required|file|mimes:jpeg,bmp,png,jpg,gif,zip,rar,pdf,doc,docx,ppt,pptx,xls,xlsx,txt,mp3,mp4|max:8192',
            'keterangan_file'   => 'required',
        ]);

        $file = Files::findOrFail($id);
        $file->judul = $request->judul;
        $file->keterangan_file = $request->keterangan_file;
        $file->slug = Str::slug($request->judul);
        if (isset($file)) {
            $nama_file = Str::slug($request->judul) . '-' . uniqid() . '.' . $request->file('file')->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('file')) {
                Storage::disk('public')->makeDirectory('file');
            }

            if (Storage::disk('public')->exists('file/' . $file->file)) {
                Storage::disk('public')->delete('file/' . $file->file);
            }

            Storage::disk('public')->put('file/' . $nama_file, File::get($request->file('file')));
            $file->file = $nama_file;
        }
        $file->save();
        return redirect()->route('admin.file.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah File : ' . $request->judul)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = Files::findOrFail($id);
        $file->delete();
        Storage::delete('public/file/'.$file->file);
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Album')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $file = Files::find($id);
        if ($mode == "true") {
            $file->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $file->aktif = 'N';
            // return 2;
        }
        $file->update();

        return response()->json($file, 200);
    }
}
