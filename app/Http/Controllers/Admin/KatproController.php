<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Katpro;
use Str;

class KatproController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $katpros = Katpro::where('katpro_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $katpros->appends(['search' => $search]);
        return view('admin.kategori_produk.index', compact('katpros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori_produk.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $valid = Katpro::where('katpro_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.kategori_produk.index')->with('header', 'Gagal!')->with('message', 'Gagal Menambah Kategori Produk : ' . $request->name . 'karena sudah ada!')->with('Class', 'warning');
        } else {
            $create = Katpro::create([
                'katpro_name' => $request->name,
                'katpro_slug' => Str::slug($request->name),
            ]);
            return redirect()->route('admin.kategori_produk.index')->with('header', 'Sukses!')->with('message', 'Sukses Menambah Kategori Produk : ' . $request->name . '!')->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $katpros = Katpro::findOrFail($id);
        return view('admin.kategori_produk.edit', compact('katpros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $create = Katpro::find($id)->update([
            'katpro_name' => $request->name,
            'katpro_slug' => Str::slug($request->name),
        ]);
        return redirect()->route('admin.kategori_produk.index')->with('header', 'Sukses!')->with('message', 'Sukses Mengubah Kategori Produk : ' . $request->name . '!')->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $katpro = Katpro::findOrFail($id);
        $katpro->delete();
        return redirect()->back()->with('header', 'Sukses!')->with('message', 'Sukses Menghapus Data!')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $katpro = Katpro::find($id);
        if ($mode == "true") {
            $katpro->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $katpro->aktif = 'N';
            // return 2;
        }
        $katpro->update();

        return response()->json($katpro, 200);
    }
}
