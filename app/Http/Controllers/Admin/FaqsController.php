<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faqs;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $faqs = Faqs::where('pertanyaan', 'LIKE',"%$search%")->orWhere('jawaban', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $faqs->appends(['search' => $search]);
        return view('admin.faqs.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faqs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);

        $valid = Faqs::where('pertanyaan', '=', $request->pertanyaan)->count();
        if($valid > 0) {
            return redirect()->route('admin.faqs.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Faqs : ' . $request->pertanyaan . ' karena Faqs ' . $request->pertanyaan . ' sudah ada')->with('Class', 'warning');
        } else {
            $create = Faqs::create([
                'pertanyaan' => $request->pertanyaan,
                'jawaban' => $request->jawaban,
            ]);
            return redirect()->route('admin.donasi_manual.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Faqs : ' . $request->pertanyaan)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faqs = Faqs::findOrFail($id);
        return view('admin.faqs.edit', compact('faqs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);

        $create = Faqs::find($id)->update([
            'pertanyaan' => $request->pertanyaan,
            'jawaban' => $request->jawaban,
        ]);
        return redirect()->route('admin.faqs.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Faqs : ' . $request->pertanyaan)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faqs = Faqs::findOrFail($id);
        $faqs->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Faqs')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $faqs = Faqs::find($id);
        if ($mode == "true") {
            $faqs->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $faqs->aktif = 'N';
            // return 2;
        }
        $faqs->update();

        return response()->json($faqs, 200);
    }
}
