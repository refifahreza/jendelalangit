<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Akun;
use Auth;
use Hash;
use Illuminate\Support\Carbon;

class AkunController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $akun = Akun::where('id','=',$id)->first();
        $car = Carbon::parse($akun->created_at)->format('d, M Y H:i');
        return view('admin.change_akun.index', compact('akun', 'car'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'username' => 'required|max:30',
            'password' => 'nullable',
            'icon' => 'file|image|mimes:jpeg,png,jpg|max:8012',
        ]);

        $id = $request->id;


        if($request->hasFile('icon')) {
            // Storage::delete('public/images/user_icon/'.$beritas->icon);
            $image = $request->file('icon');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/user_icon', $image_name, 'public');
            if(empty($request->password)) {
                $create = Akun::where('id',$id)->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'icon' => $image_name,
                ]);
                return redirect()->route('admin.profil.index')->with('header', 'Sukses')->with('message', 'Berhasil mengubah Profil : ' . $request->name . ' tanpa mengubah Password')->with('Class', 'success');
            } else {
                $create = Akun::where('id',$id)->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'icon' => $image_name,
                ]);
                return redirect()->route('admin.profil.index')->with('header', 'Sukses')->with('message', 'Berhasil mengubah Profil : ' . $request->name . ' dengan mengubah Password')->with('Class', 'success');
            }
        } else {
            if(empty($request->password)) {
                $create = Akun::where('id',$id)->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                ]);
                return redirect()->route('admin.profil.index')->with('header', 'Sukses')->with('message', 'Berhasil mengubah Profil : ' . $request->name . ' tanpa mengubah Password')->with('Class', 'success');
            } else {
                $create = Akun::where('id',$id)->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                ]);
                return redirect()->route('admin.profil.index')->with('header', 'Sukses')->with('message', 'Berhasil mengubah Profil : ' . $request->name . ' dengan mengubah Password')->with('Class', 'success');
            }
        }

    }
}


