<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donline;
use App\Exports\DonlineExport;
use Maatwebsite\Excel\Facades\Excel;

class DonlineController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('search')) {
            $search = $request->get('search');
            $donlines = Donline::where('status','=',$search)->orderBy('id','desc')->paginate(5);
            $donlines->appends(['search' => $search]);
            $let = str_replace(' ','%20',config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>','',$let);
            $b = str_replace('</p>','',$a);
            $c = str_replace('&nbsp;','%0A',$b);
            return view('admin.donline.index', compact('donlines', 'c'));
        } else {
            $let = str_replace(' ','%20',config('web_config')['TEXT_WHATSAPP']);
            $a = str_replace('<p>','',$let);
            $b = str_replace('</p>','',$a);
            $c = str_replace('&nbsp;','%0A',$b);
            $donlines = Donline::orderBy('id','desc')->paginate(10);
            return view('admin.donline.index', compact('donlines', 'c'));
        }
    }

    public function export()
    {
        return Excel::download(new DonlineExport, 'donasi_online.xlsx');
    }
}
