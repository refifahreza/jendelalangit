<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settingzakat;

class SettingzakatController extends Controller
{
    public function index()
    {
        $settingzakat = Settingzakat::all()->pluck('value','key');
        return view('admin.setting_zakat.index', compact('settingzakat'));
    }

    public function update(Request $request)
    {
        $updates = $request->all();
        foreach($updates as $key => $value) {
            if($key == 'WEB_LOGO' || $key == 'WEB_LOGO_WHITE' || $key == 'WEB_FAVICON') {
                $file = $request->file($key);
                $filename = $file->getClientOriginalName();
                $file->storeAs('images/logo/', $filename, 'public');
                $value = $filename;
            }
            // return $value;
            Settingzakat::where('key',$key)->update(['value' => $value]);
        }
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses mengubah Pengaturan Zakat')->with('Class', 'success');
    }
}
