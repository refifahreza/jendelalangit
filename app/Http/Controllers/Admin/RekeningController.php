<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rekening;
use Storage;
use Str;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $rekenings = Rekening::where('rekening_name', 'LIKE',"%$search%")->orderBy('id','desc')->paginate(10);
        $rekenings->appends(['search' => $search]);
        return view('admin.rekening.index', compact('rekenings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rekening.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required|max:4',
            'number' => 'required|max:16',
            'author' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $valid = Rekening::where('rekening_name', '=', $request->name)->count();
        if($valid > 0) {
            return redirect()->route('admin.rekening.index')->with('header', 'Gagal')->with('message', 'Gagal menambah Rekening : ' . $request->name . ' karena Rekening ' . $request->name . ' sudah ada')->with('Class', 'warning');;
        } else {
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/rekening', $image_name, 'public');
            $create = Rekening::create([
                'rekening_name' => $request->name,
                'rekening_slug' => Str::slug($request->name),
                'rekening_code' => $request->code,
                'rekening_number' => $request->number,
                'rekening_author' => $request->author,
                'logo' => $image_name
            ]);
            return redirect()->route('admin.rekening.index')->with('header', 'Sukses')->with('message', 'Sukses menambah Rekening : ' . $request->name)->with('Class', 'success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rekenings = Rekening::findOrFail($id);
        return view('admin.rekening.edit', compact('rekenings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $request->validate([
            'name' => 'required',
            'code' => 'required|max:4',
            'number' => 'required|max:16',
            'author' => 'required',
            'image' => 'nullable|file|image|mimes:jpeg,png,jpg|max:8192',
        ]);

        $rekening = Rekening::findOrFail($id);
        $rekening->rekening_name = $request->name;
        $rekening->rekening_slug = Str::slug($request->name);
        $rekening->rekening_code = $request->code;
        $rekening->rekening_number = $request->number;
        $rekening->rekening_author = $request->author;
        if($request->hasFile('image')) {
            Storage::delete('public/images/rekening/'.$rekening->logo);
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = time()."_".$image->getClientOriginalName();
            $image->storeAs('/images/rekening', $image_name, 'public');
            $rekening->logo = $image_name;
        }
        $rekening->save();
        return redirect()->route('admin.rekening.index')->with('header', 'Sukses')->with('message', 'Sukses mengubah Rekening : ' . $request->name)->with('Class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekening = Rekening::findOrFail($id);
        Storage::delete('public/images/rekening/'.$rekening->logo);
        $rekening->delete();
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Sukses menghapus Rekening')->with('Class', 'success');
    }

    public function aktif(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        $rekening = Rekening::find($id);
        if ($mode == "true") {
            $rekening->aktif = 'Y';
            // return 1;
        } elseif ($mode == "false") {
            $rekening->aktif = 'N';
            // return 2;
        }
        $rekening->update();

        return response()->json($rekening, 200);
    }
}
