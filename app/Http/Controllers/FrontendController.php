<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Berita;
use App\Models\Mitra;
use App\Models\Faqs;
use App\Models\Album;
use App\Models\Gallery;
use App\Models\Katbout;
use App\Models\Katber;
use App\Models\Rekening;
use App\Models\Jendonasi;
use App\Models\Subdonasi;
use App\Models\Donline;
use App\Models\Donman;
use App\Models\Video;
use App\Models\Pesan;
use App\Models\Agenda;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;
use Storage;
use Str;
use DB;
use App\Mail\Notif;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    public function index()
    {
        $title = 'Jendela Langit Semesta';
        $sliders = Slider::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        $beritas = Berita::where('aktif', '=', 'Y')->orderBy('id', 'desc')->limit(4)->get();
        $mitras = Mitra::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        $rekenings = Rekening::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        $albums = Album::where('aktif', '=', 'Y')->orderBy('id', 'desc')->limit(6)->get();
        $gallerys = Gallery::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        $jendons = Jendonasi::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        $donlines = Donline::where('status', '=', 'success')->orderBy('id', 'desc')->limit(5)->get();
        $donmans = Donman::where('status', '=', 'sukses')->orderBy('id', 'desc')->limit(5)->get();
        $videos = Video::where('aktif', 'Y')->orderBy('id', 'desc')->get();
        $sum0 = Donline::where('status', '=', 'success')->sum('jumlah');
        $sum1 = Donman::where('status', '=', 'sukses')->sum('jumlah');
        $count = $sum0 + $sum1;
        return view('frontend.content', compact('title', 'sliders', 'beritas', 'mitras', 'rekenings', 'albums', 'gallerys', 'jendons', 'donlines', 'donmans', 'count', 'videos'));
    }



    public function jenis(Request $request)
    {
        $id = $request->jenis;
        $subs = Subdonasi::where('jendon_id', '=', $id)->where('aktif', '=', 'Y')->get();
        return view('frontend.option', compact('subs'));
    }



    public function berita()
    {
        $title = 'Jendela Langit | Berita';
        $beritas = Berita::where('aktif', '=', 'Y')->get();
        return view('frontend.berita', compact('title', 'beritas'));
    }

    public function kategori($katber_slug)
    {
        $title = 'Kategori | Jendela Langit';
        $katbers = Katber::where('katber_slug', '=', $katber_slug)->first();
        $get = $katbers->id;
        $beritad = Berita::where('kategori_id', '=', $get)->get();
        return view('frontend.kategori', compact('title', 'katbers', 'beritad'));
    }

    public function listdonatur()
    {
        $title = 'List Donatur | Jendela Langit';
        $donlines = Donline::where('status', '=', 'success')->orderBy('id', 'desc')->get();
        $donmans = Donman::where('status', '=', 'sukses')->orderBy('id', 'desc')->get();
        return view('frontend.listdonatur', compact('title', 'donlines', 'donmans'));
    }

    public function album()
    {
        $title = 'Album Photo | Jendela Langit';
        $albums = Album::where('aktif', '=', 'Y')->get();
        $gallerys = Gallery::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        return view('frontend.album', compact('title', 'albums', 'gallerys'));
    }


    public function faq()
    {
        $title = 'F.A.Q | Jendela Langit ';
        $faqs = Faqs::where('aktif', '=', 'Y')->get();
        return view('frontend.faq', compact('title', 'faqs'));
    }

    public function kontak()
    {
        $title = 'Kontak | Jendela Langit ';
        // $faqs = Faqs::where('aktif', '=', 'Y')->get();
        return view('frontend.kontak', compact('title'));
    }

    public function rekening()
    {
        $title = 'Rekening | Jendela Langit';
        $rekenings = Rekening::where('aktif', '=', 'Y')->get();
        return view('frontend.rekening', compact('title', 'rekenings'));
    }

    public function tentang()
    {
        $title = 'Tentang | Jendela Langit  ';
        return view('frontend.tentang', compact('title'));
    }



    public function detailberita($title_slug)
    {
        $title = 'Detail Berita | Jendela Langit';
        $beritas = Berita::where('title_slug', '=', $title_slug)->first();
        return view('frontend.detailberita', compact('title', 'beritas'));
    }



    public function detailalbum($album_slug)
    {
        $title = 'Detail Album | Jendela Langit ';
        $albums = Album::where('album_slug', '=', $album_slug)->first();
        $get = $albums->id;
        $galleryd = Gallery::where('album_id', '=', $get)->get();
        return view('frontend.detailalbum', compact('title', 'albums', 'galleryd'));
    }



    public function tentangkami()
    {
        $title = 'Tentang Kami | Jendela Langit';
        $katbouts = Katbout::where('aktif', '=', 'Y')->get();
        return view('frontend.tentangkami', compact('title', 'katbouts'));
    }



    public function detailtentang($slug)
    {
        $title = 'Detail Tentang | Jendela Langit';
        $katbouts = Katbout::where('slug', '=', $slug)->first();
        return view('frontend.detailtentang', compact('title', 'katbouts'));
    }



    public function submit()
    {
        DB::transaction(function () {
            $tipe = $this->request->tipe_name;
            $uang0 = str_replace('Rp. ', '', $this->request->jumlah);
            $uang = str_replace('.', '', $uang0);
            $code = date('Ymdhis');
            $notel = '+62' . $this->request->telp;

            if ($this->request->anony == 'ya') {
                $nama = 'Hamba Allah';
            } else {
                $nama = $this->request->names;
            }

            $donasi = Donline::create([
                'kode_donasi' => $code,
                'nama_donatur' => $nama,
                'notelp' => $notel,
                'pesan' => $this->request->notes,
                'jenis_id' => $this->request->jenis,
                'jumlah' => $uang,
            ]);



            // $payload = [

            //     'transaction_details' => [

            //         'order_id'      => $donasi->id,

            //         'gross_amount'  => $donasi->jumlah,

            //     ],

            //     'customer_details' => [

            //         'first_name'    => $donasi->nama_donatur,

            //         'email'         => 'bot@bot.com',

            //         'phone'         => $donasi->notelp,

            //         'address'       => 'Jl. Bot',

            //     ],

            //     'item_details' => [

            //         [

            //             'id'       => $donasi->subdon_id,

            //             'price'    => $donasi->jumlah,

            //             'quantity' => 1,

            //             'name'     => ucwords($donasi->nama_donatur)

            //         ]

            //     ]

            // ];
            $transaction = array(
                'transaction_details' => array(
                    'order_id' => $code,
                    'gross_amount' => $uang
                )
            );

            $snapToken = Veritrans_Snap::getSnapToken($transaction);
            $donasi->snap_token = $snapToken;
            $donasi->save();
            $this->response['snap_token'] = $snapToken;
        });

        return response()->json($this->response);
    }

    public function notificationHandler(Request $request)
    {

        $notif = new Veritrans_Notification();
        DB::transaction(function () use ($notif) {
            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $orderId = $notif->order_id;
            $fraud = $notif->fraud_status;
            $donasi = Donline::where('kode_donasi', $orderId)->firstOrFail();

            if ($transaction == 'capture') {
                if ($type == 'credit_card') {
                    if ($fraud == 'challenge') {
                        $donasi->setPending();
                    } else {
                        $donasi->setSuccess();
                    }
                }
            } elseif ($transaction == 'settlement') {
                $donasi->setSuccess();
            } elseif ($transaction == 'pending') {
                $donasi->setPending();
            } elseif ($transaction == 'deny') {
                $donasi->setFailed();
            } elseif ($transaction == 'expire') {
                $donasi->setExpired();
            } elseif ($transaction == 'cancel') {
                $donasi->setFailed();
            }
        });
        return;
    }

    public function detaildonasi(Request $request)
    {
        // return $request;
        $title = 'Detail Donasi | Jendela Langit';

        $request->validate([
            'jenis' => 'required',
            'jumlah' => 'required',
            'names' => 'required',
            'rekening' => 'required',
            // 'telp' => 'required',
            'anony' => 'nullable',
            'emails' => 'required',
        ]);

        $uang0 = str_replace('Rp. ', '', $request->jumlah);
        $uang = str_replace('.', '', $uang0);
        $code = date('Ymdhis');
        // $notel = '+62' . $request->telp;
        $cek = Rekening::where('id', '=', $request->rekening)->first();
        $get = $cek->id;

        if ($request->anony == 'ya') {
            $nama = 'Hamba Allah';
        } else {
            $nama = $request->names;
        }

        $create = Donman::create([
            'kode_donasi' => $code,
            'nama_donatur' => $nama,
            // 'notelp' => $notel,
            'jenis_id' => $request->jenis,
            'email' => $request->emails,
            'rekening_id' => $get,
            'jumlah' => $uang,
        ]);
        //  return $create;
        Mail::send('frontend/notifemail', ['noref' => $code, 'pesan' => 'Kode valid hanya 1x24 jam'], function ($message) use ($request) {
            $message->subject('Kode Konfirmasi Donasi Yayasan Jendela Langit Semesta');
            $message->from('jendelalangit99@gmail.com', 'Jendela Langit Semesta');
            $message->to($request->emails);
        });
        $ide = $create->kode_donasi;
        $donasis = Donman::where('kode_donasi', '=', $ide)->first();
        return view('frontend.detaildonasi', compact('title', 'donasis'));
    }

    public function pembayaran()
    {
        $title = 'Pembayaran | Jendela Langit';
        return view('frontend.pembayaran', compact('title'));
    }

    public function konfirmasidonasi(Request $request, $kode_donasi)
    {
        // return $kode_donasi;
        $request->validate([
            'banked' => 'required',
            'rekening' => 'required',
            'author' => 'required',
            'cabang' => 'required',
            'notes' => 'required',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $image = $request->file('image');
        $image_extension = $image->extension();
        $image_name = time() . "_" . $image->getClientOriginalName();
        $image->storeAs('/images/buktied', $image_name, 'public');
        $create = Donman::where('kode_donasi', $kode_donasi)->update([
            'pesan' => $request->notes,
            'bank_donatur' => $request->banked,
            'norek_donatur' => $request->rekening,
            'atas_nama' => $request->author,
            'cabang' => $request->cabang,
            'bukti_donasi' => $image_name,
        ]);
        // return view('frontend.content', compact('title')->with('message', 'Berhasil')->with('class','success'));
        return redirect()->route('main')->with('message', 'Donasi Berhasil')->with('Class', 'success')->with('pesan', 'Terimakasih Atas Donasi Anda');
    }

    public function statusdonasi($kode_donasi)
    {
        $title = 'Status Donasi | Jendela Langit';
        $donasis = Donman::where('kode_donasi', '=', $kode_donasi)->first();
        $ceks = Donman::where('kode_donasi', '=', $kode_donasi)->whereNull('bank_donatur')->first();
        return view('frontend.statusdonasi', compact('title', 'donasis', 'ceks'));
    }

    public function cekdonasi(Request $request)
    {
        $kode = $this->request->nomor;
        $title = 'Status Donasi | Jendela Langit';
        $donasis = Donman::where('kode_donasi', '=', $kode)->first();
        $ceks = Donman::where('kode_donasi', '=', $kode)->whereNull('bank_donatur')->first();
        if ($donasis == null) {
            return view('frontend.gagal', compact('title'));
        } else {
            return view('frontend.statusdonasi', compact('title', 'donasis', 'ceks'));
        }
    }

    public function manualpembayaran($rekening_slug)
    {
        // return $rekening_slug;
        $title = 'Manual Donasi | Jendela Langit';
        $rekenings = Rekening::where('rekening_slug', '=', $rekening_slug)->first();
        $jendons = Jendonasi::where('aktif', '=', 'Y')->orderBy('id', 'desc')->get();
        return view('frontend.manualpembayaran', compact('title', 'rekenings', 'jendons'));
    }

    // Insert pesan ke table pesan
    public function store_pesan(Request $request)
    {
        $request->validate([
            'nama'      => 'required',
            'email'     => 'required',
            'subjek'    => 'required',
            'pesan'     => 'required',
        ]);

        $create = Pesan::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'subjek' => $request->subjek,
            'pesan' => $request->pesan,
        ]);
        return redirect()->back()->with('header', 'Sukses')->with('message', 'Terima Kasih Telah Mengirimkan Pesan')->with('Class', 'success');
    }

    public function agenda()
    {
        $agendas = Agenda::where('aktif', 'Y')->paginate(10);
        $user_id = Auth::id();

        return view('frontend.agenda', compact('agendas'));
    }
}
