<?php

namespace App\Exports;

use App\Models\Donline;
use Maatwebsite\Excel\Concerns\FromCollection;

class DonlineExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Donline::orderBy('id','desc')->get();
    }
}
