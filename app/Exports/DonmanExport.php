<?php

namespace App\Exports;

use App\Models\Donman;
use Maatwebsite\Excel\Concerns\FromCollection;

class DonmanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Donman::orderBy('id','desc')->get();
    }
}
