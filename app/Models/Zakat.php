<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subzakat;
class Zakat extends Model
{
    protected $table = 'zakats';

    protected $fillable = ['nama_zakat'];

    public function subzakat()
    {
    	return $this->hasMany(Subzakat::class);
    }
}
