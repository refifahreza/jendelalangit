<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Katvid;

class Video extends Model
{
    protected $table = 'videos';
    protected $fillable = [];
    protected $guarded = [];

    public function katvid()
    {
    	return $this->belongsTo(Katvid::class, 'katvid_id');
    }

    public function getNamaKategoriAttribute()
    {
        if ($this->katvid) {
            return $this->katvid->nama;
        }
    }
}
