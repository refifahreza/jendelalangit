<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Katvid extends Model
{
    protected $table = 'kategori_video';

    protected $fillable = ['kategori_video', 'katvid_slug', 'aktif'];
}
