<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $table = 'rekenings';

    protected $fillable = ['rekening_name', 'rekening_slug', 'rekening_code', 'rekening_number', 'rekening_author', 'logo'];
}
