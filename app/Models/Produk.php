<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'products';

    protected $fillable = ['produk_name', 'katpro_id', 'produk_slug', 'produk_pict', 'produk_desc', 'produk_price', 'produk_stok', 'produk_status'];

    public function katpro() {
        return $this->belongsTo('App\Models\Katpro', 'katpro_id');
    }
}
