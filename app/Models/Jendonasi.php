<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jendonasi extends Model
{
    protected $table = 'jenis_donasi';

    protected $fillable = ['jendon_name'];
}
