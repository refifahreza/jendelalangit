<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'files';

    protected $fillable = [
    	'judul', 'slug', 'file', 'keterangan_file', 'hits', 'aktif', 'user_id'
	];
}
