<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settingzakat extends Model
{
    protected $table = 'setting_zakat';
    protected $fillable = ['key','value'];
}
