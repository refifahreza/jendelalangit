<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Berita;

class Tag extends Model
{
    protected $table = 'tags';
    protected $fillable = [];
    protected $guarded = []; 

    public function beritas()
    {
        return $this->belongsToMany(Berita::class, 'berita_tags');
    }
    
    public function getBeritasIdAttribute()
    {
        return $this->beritas->pluck('id');
    }
}
