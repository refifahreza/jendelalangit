<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Katpro extends Model
{
    protected $table = 'kategori_produk';

    protected $fillable = ['katpro_name', 'katpro_slug'];
}
