<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Katber extends Model
{
    protected $table = 'kategori_beritas';

    protected $fillable = ['id', 'katber_name', 'katber_slug'];
}
