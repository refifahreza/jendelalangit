<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agenda extends Model
{
    protected $table = 'agendas';

    protected $fillable = ['judul', 'lokasi', 'slug', 'pengajar', 'penyelenggara', 'isi_agenda', 'waktu_awal', 'waktu_akhir'];

    public function getWaktuAwalIndAttribute()
    {
        $date = $this->waktu_awal;
        Carbon::setLocale('es');
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A, %d %B %Y %H:%I');
    }

    public function getWaktuAkhirIndAttribute()
    {
        $date = $this->waktu_akhir;
        Carbon::setLocale('es');
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A, %d %B %Y %H:%I');
    }

    public function getCreatedWatchAttribute($date)
    {
        $date = $this->created_at;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%H:%I');
    }

    public function getCalendarGoogleAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->modify('-8 hour')->formatLocalized('%Y%m%dT%H%I%SZ');
    }

    public function getCalendarGoogleAkhirAttribute($date)
    {
        $date = $this->waktu_akhir;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->modify('-8 hour')->formatLocalized('%Y%m%dT%H%I%SZ');
    }

    public function getCreatedNewAttribute($date)
    {
        $date = $this->created_at;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A, %d %B %Y');
    }

    public function getTanggalTunggalAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%d');
    }

    public function getHariTunggalAwalAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A');
    }

    public function getHariTunggalAkhirAttribute($date)
    {
        $date = $this->waktu_akhir;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A');
    }

    public function getBulanTunggalAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%b');
    }

    public function getJamSatuAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%H:%I');
    }

    public function getJamDuaAttribute($date)
    {
        $date = $this->waktu_akhir;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%H:%I');
    }

    public function getHariAttribute($date)
    {
        $date = $this->waktu_awal;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A');
    }

    public function scopeDataAktif($query)
    {
        return $query->where('aktif', 'Y');
    }

    public function getLimitStrAttribute()
    {
        $isi_agenda = $this->isi_agenda;
        return str_limit($isi_agenda, 150);
    }

    public function getLimitStrDuaAttribute()
    {
        $isi_agenda = $this->isi_agenda;
        return str_limit($isi_agenda, 150);
    }
}
