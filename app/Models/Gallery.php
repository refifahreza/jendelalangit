<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';

    protected $fillable = ['nama_foto', 'foto', 'album_id'];

    public function album() {
        return $this->belongsTo('App\Models\Album', 'album_id');
    }
}
