<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donman extends Model
{
    protected $table = 'donmen';

    protected $fillable = ['kode_donasi', 'nama_donatur', 'pesan', 'jenis_id', 'email', 'rekening_id', 'jumlah', 'status', 'bank_donatur', 'norek_donatur', 'atas_nama', 'cabang', 'bukti_donasi'];

    public function jenis()
    {
        return $this->belongsTo('App\Models\Jendonasi', 'jenis_id');
    }

    public function rekening()
    {
        return $this->belongsTo('App\Models\Rekening', 'rekening_id');
    }
}
