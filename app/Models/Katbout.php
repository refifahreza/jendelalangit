<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Katbout extends Model
{
    protected $table = 'katbouts';

    protected $fillable = ['jenis_tentang', 'slug', 'gambar', 'konten', 'aktif', 'type'];
}
