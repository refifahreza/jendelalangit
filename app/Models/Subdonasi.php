<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subdonasi extends Model
{
    protected $table = 'sub_donasi';

    protected $fillable = ['subdon_name', 'jendon_id'];

    public function jenis() {
        return $this->belongsTo('App\Models\Jendonasi', 'jendon_id');
    }

     public function getNamaZakatAttribute()
    {
        if ($this->zakat) {
            return $this->zakat->nama;
        }
    }
}
