<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;
use Carbon\Carbon;

class Berita extends Model
{
    protected $guarded = [];

    protected $table = 'beritas';
    protected $fillable = ['title', 'title_slug', 'seo_judul', 'kategori_id', 'author_id', 'thumnail', 'deskripsi_gambar', 'content', 'deskripsi_meta', 'kutipan', 'created_at'];

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Models\Katber', 'kategori_id');
    }

    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'berita_tags');
    }

    public function getCreatedWatchAttribute($date)
    {
        $date = $this->created_at;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%H:%I');
    }

    public function getCreatedNewAttribute($date)
    {
        $date = $this->created_at;
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->formatLocalized('%A, %d %B %Y ');
    }

    public function getTagIdAttribute()
    {
        return $this->tag->pluck('id');
    }
}
