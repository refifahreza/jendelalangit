<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donline extends Model
{
    protected $table = 'donlines';

    protected $fillable = ['kode_donasi', 'nama_donatur', 'notelp', 'pesan', 'jenis_id', 'detail_jenis', 'jumlah', 'status', 'snap_token'];

    public function setPending()
    {
        $this->attributes['status'] = 'pending';
        self::save();
    }

    public function setSuccess()
    {
        $this->attributes['status'] = 'success';
        self::save();
    }

    public function setFailed()
    {
        $this->attributes['status'] = 'failed';
        self::save();
    }

    public function setExpired()
    {
        $this->attributes['status'] = 'expired';
        self::save();
    }

    public function jenis() {
        return $this->belongsTo('App\Models\Jendonasi', 'jenis_id');
    }
}
