<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subzakat extends Model
{
    protected $table = 'sub_zakat';

    protected $fillable = ['nama_sub_zakat', 'zakat_id'];

    public function zakat() {
        return $this->belongsTo('App\Models\Zakat', 'zakat_id');
    }
}
