<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use DB;
use App\Models\Katber;
use App\Models\Katbout;
use App\Models\Berita;
use App\Models\Agenda;
use Illuminate\Support\Facades\URL;
use App\User;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // URL::forceScheme('https');
        Schema::defaultStringLength(191);
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Makassar');

        view()->composer('frontend.layouts.navbar2', function ($view) {
            $katbers = Katber::where('aktif', '=', 'Y')->orderBy('katber_name', 'asc')->get();
            $abouts = Katbout::where('aktif', '=', 'Y')->orderBy('jenis_tentang', 'asc')->get();
            $view->with([
                'katbers' => $katbers,
                'abouts' => $abouts,
            ]);
        });

        view()->composer('layouts.sidebar', function ($view) {
            $users = User::where('username', '=', Auth::user()->username)->first();
            $view->with([
                'users' => $users,
            ]);
        });

        view()->composer('frontend.layouts.app', function ($view) {
            $postTerbaru = Berita::orderBy('id', 'DESC')->limit(5)->get();
            $nowDate = Carbon::now()->formatLocalized('%A, %d %B %Y ');
            $view->with([
                'postTerbaru' => $postTerbaru,
                'nowDate' => $nowDate,
            ]);
        });

        view()->composer('frontend.layouts.footer', function ($view) {
            $postTerpopuler = Berita::orderBy('dibaca', 'DESC')->limit(5)->get();
            $agendas = Agenda::where('aktif', 'Y')->orderBy('waktu_awal')->limit(5)->get();
            $view->with([
                'postTerpopuler' => $postTerpopuler,
                'agendas' => $agendas
            ]);
        });

        try {
            Schema::hasTable('settings');
            $web_config = DB::table('settings')->pluck('value', 'key');
            config(['web_config' => $web_config]);
        } catch (\Exception $e) {
        }

        setlocale(LC_ALL, 'id_ID.utf8');
        Carbon::setLocale('id_ID.utf8');
    }
}
